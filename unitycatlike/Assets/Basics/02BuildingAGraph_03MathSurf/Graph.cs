using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using static UnityEngine.Mathf;

public class Graph : MonoBehaviour
{
    public enum GraphType
    {
        kX,
        kXX,
        kXXX,
        kSinX,
        kMultiWave,
        kRipple,
        kHeart,
        kSphere,
        kSphereX,
        kTorus,
        kTorusX,
        kMax,
    }
    public enum TransitionMode
    {
        kManual,
        kRandom,
        kLoop,
        kMax,
    }
    public static class FunctionLibrary
    {
        public delegate Vector3 Function(float u, float v, float t);
        public static Function GetFunction(GraphType graphType)
        {
            switch(graphType)
            {
                case GraphType.kXX:
                return XX;
                case GraphType.kXXX:
                return XXX;
                case GraphType.kSinX:
                return Wave;
                case GraphType.kMultiWave:
                return MultiWave;
                case GraphType.kRipple:
                return Ripple;
                case GraphType.kHeart:
                return Heart;
                case GraphType.kSphere:
                return Sphere;
                case GraphType.kSphereX:
                return SphereX;
                case GraphType.kTorus:
                return Torus;
                case GraphType.kTorusX:
                return TorusX;
            }
            return X;
        }

        public static Vector3 Morph(float u, float v, float t, Function from, Function to, float progress) 
        {
            return Vector3.Lerp(from(u,v,t), to(u,v,t), progress);
        }
        public static Vector3 X(float u, float v, float t)
        {
            Vector3 p;
            p.x = u;
            p.y = 0;
            p.z = v;
            return p;
        }
        public static Vector3 XX(float u, float v, float t)
        {
            Vector3 p = Vector3.zero;
            p.x = u;
            p.y = u*u;
            p.z = v;
            return p;
        }
        public static Vector3 XXX(float u, float v, float t)
        {
            Vector3 p = Vector3.zero;
            p.x = u;
            p.y = u*u*u;
            p.z = v;
            return p;
        }
        public static Vector3 Wave(float u, float v, float t)
        {
            Vector3 p = Vector3.zero;
            p.x = u;
            p.y = Mathf.Sin(Mathf.PI * (u+v+t));
            p.z = v;
            return p;
        }
        public static Vector3 MultiWave(float u, float v, float t)
        {
            Vector3 p = Vector3.zero;
            p.x = u;
            p.z = v;

            float y = Mathf.Sin(Mathf.PI * (u + t));
            y += Mathf.Sin(2f * Mathf.PI * (v + t)) * 0.5f;
            y += Mathf.Sin(Mathf.PI * (u + v + 0.25f * t));

            p.y = y / 2.5f;
            return p;
        }
        public static Vector3 Ripple(float u, float v, float t)
        {
            Vector3 p = Vector3.zero;
            p.x = u;
            p.z = v;

            float d = Sqrt(u*u + v*v);
            float y = Mathf.Sin(Mathf.PI * (4f * d - t));
            p.y = y / (1f + 10f * d);
            return p;
        }
        public static Vector3 Heart(float u, float v, float t)
        {
            Vector3 p = Vector3.zero;
            p.x = u;
            p.y = 0;

            float y1 = 0.6f * Mathf.Sqrt(Mathf.Abs(u)) + Mathf.Sqrt((1-u*u)/2.0f);
            float y2 = 0.6f * Mathf.Sqrt(Mathf.Abs(u)) - Mathf.Sqrt((1-u*u)/2.0f);
            if(v <= 0)
            {
                p.z = y1;
            }
            else
            {
                p.z = y2;
            }
            return p;
        }
        public static Vector3 Sphere(float u, float v, float t)
        {
            float r = Cos(0.5f * PI * v);
            Vector3 p;
            p.x = r * Sin(PI * u);
            p.y = Sin(PI * 0.5f * v);
            p.z = r * Cos(PI * u);
            return p;
        }
        public static Vector3 SphereX(float u, float v, float t)
        {
            float r = 0.9f + 0.1f * Sin(PI * (6f * u + 4f * v + t));
            float s = r * Cos(0.5f * PI * v);
            Vector3 p;
            p.x = s * Sin(PI * u);
            p.y = r * Sin(0.5f * PI * v);
            p.z = s * Cos(PI * u);
            return p;
        }

        public static Vector3 Torus(float u, float v, float t)
        {
            float r1 = 0.75f;
            float r2 = 0.25f;
            float s = r1 + r2 * Cos(PI * v);
            Vector3 p;
            p.x = s * Sin(PI * u);
            p.y = r2 * Sin(PI * v);
            p.z = s * Cos(PI * u);
            return p;
        }
        public static Vector3 TorusX(float u, float v, float t)
        {
            float r1 = 0.7f + 0.1f * Sin(PI* (6f*u + 0.5f * t));
            float r2 = 0.15f + 0.05f * Sin(PI * (8f * u + 4f * v + 2f * t));
            float s = r1 + r2 * Cos(PI * v);
            Vector3 p;
            p.x = s * Sin(PI * u);
            p.y = r2 * Sin(PI * v);
            p.z = s * Cos(PI * u);
            return p;
        }
    }

    [Range(1,3)]
    public float transitionDuration = 1f;
    public GraphType graphType = GraphType.kX;
    public TransitionMode transitionMode = TransitionMode.kLoop;
    [Range(3,6)]
    public float graphDuration = 3f;
    GraphType preGraphType = GraphType.kX;
    [SerializeField]
    Transform pointPrefab;
    [SerializeField, Range(10,100)]
    int resolution = 10;
    List<Transform> points = new List<Transform>();

    void Awake() 
    {
        float step = 2f / resolution;
        Vector3 position = Vector3.zero;
        var scale = Vector3.one * step;
        for(int i=0; i<resolution; i++)
        {
            for(int j=0; j<resolution; j++)
            {
                Transform point = Instantiate(pointPrefab);
                point.localPosition = position;
                point.localScale = scale;

                point.SetParent(transform, false);
                points.Add(point);
            }
        }
    }

    float graphTimer;
    public static void PickGraphType(TransitionMode transitionMode, float graphDuration, ref float graphTimer, ref GraphType graphType)
    {
        if(transitionMode == TransitionMode.kManual)
        {
            return;
        }
        graphTimer += Time.deltaTime;
        if(graphTimer >= graphDuration)
        {
            if(transitionMode == TransitionMode.kLoop)
            {
                graphType = (GraphType)(((int)graphType + 1) % (int)GraphType.kMax);
            }
            else if(transitionMode == TransitionMode.kRandom)
            {
                graphType = (GraphType)Random.Range(0, (int)GraphType.kMax);
            }
            graphTimer = 0;
        }
    }

    bool doTransition = false;
    float transitionTimer = 0f;
    // Update is called once per frame
    void Update()
    {
        PickGraphType(transitionMode, transitionDuration, ref transitionTimer, ref graphType);

        if(preGraphType != graphType && !doTransition)
        {
            transitionTimer = 0f;
            doTransition = true;
        }

        float time = Time.time;
        float step = 2f / resolution;
        FunctionLibrary.Function from = FunctionLibrary.GetFunction(preGraphType);
        FunctionLibrary.Function to = FunctionLibrary.GetFunction(graphType);
        for(int i=0; i<resolution; i++)
        {
            for(int j=0; j<resolution; j++)
            {
                Transform point = points[i*resolution + j];
                float u = (j * step - 1f + step * 0.5f);
                float v = (i * step - 1f + step * 0.5f);
                Vector3 position = point.localPosition;
                if(preGraphType != graphType)
                {
                    position = FunctionLibrary.Morph(u, v, time, from, to, transitionTimer/transitionDuration);
                }
                else
                {
                    position = to(u, v, time);
                }
                point.localPosition = position;
            }
        }

        if(preGraphType != graphType)
        {
            transitionTimer += Time.deltaTime;
            if(transitionTimer>= transitionDuration)
            {
                preGraphType = graphType;
                doTransition = false;
            }
        }
    }
}
