using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[ExecuteAlways]
public class GMyClock : MonoBehaviour
{
    public Transform hourTrans;
    public Transform minuteTrans;
    public Transform secondTrans;
    public bool ticktackMode = true;

    void Update()
    {
        int hour = DateTime.Now.Hour;
        int minute = DateTime.Now.Minute;
        int second = DateTime.Now.Second;
        float hourRotation = (hour % 12 + minute / 60.0f)/12.0f * -360;
        float minuteRotation = (minute % 60)/60.0f * -360;
        float secondRotation = 0;
        if(ticktackMode)
        {
            secondRotation = (second % 60)/60.0f * -360;
        }
        else
        {
            int millisecond = DateTime.Now.Millisecond;
            secondRotation = (second % 60 + millisecond / 1000.0f)/60.0f * -360;
        }

        if(hourTrans != null)
        {
            hourTrans.localEulerAngles = new Vector3(0, hourRotation%360.0f, 0);
        }
        if(minuteTrans!=null)
        {
            minuteTrans.localEulerAngles = new Vector3(0, minuteRotation%360.0f, 0);
        }
        if(secondTrans!=null)
        {
            secondTrans.localEulerAngles = new Vector3(0, secondRotation % 360.0f, 0);
        }
    }
}
