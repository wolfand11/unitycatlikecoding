using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;

using static Unity.Mathematics.math;
using quaternion = Unity.Mathematics.quaternion;
using Random = UnityEngine.Random;

public class FractalProceduralJobOrganic: MonoBehaviour
{
    struct FractalPart{
        public float3 worldPosition;
        public quaternion worldRotation;
        public quaternion rotation;
        public float spinAngle;
        public float maxSagAngle;
        public float spinVelocity;
    }
    [BurstCompile(FloatPrecision.Standard, FloatMode.Fast, CompileSynchronously = true)]
    struct UpdateFractalLevelJob : IJobFor
    {
        public float scale;
        public float deltaTime;
        [ReadOnly]
        public NativeArray<FractalPart> parents;
        public NativeArray<FractalPart> parts;
        [WriteOnly]
        public NativeArray<float3x4> matrices;
        public void Execute(int index)
        {
            FractalPart parent = parents[index/5];
            FractalPart part = parts[index];
            part.spinAngle += part.spinVelocity * deltaTime;

            float3 upAxis = mul(mul(parent.worldRotation, part.rotation), up());
            float3 sagAxis = cross(up(), upAxis);
            float sagMagnitude = length(sagAxis);
            quaternion baseRotation;
            if(sagMagnitude > 0f)
            {
                sagAxis /= sagMagnitude;
                //quaternion sagRotation = quaternion.AxisAngle(sagAxis, PI * 0.25f * sagMagnitude);
                quaternion sagRotation = quaternion.AxisAngle(sagAxis, part.maxSagAngle * sagMagnitude);
                baseRotation = mul(sagRotation, parent.worldRotation);
            }
            else
            {
                baseRotation = parent.worldRotation;
            }

            part.worldRotation = mul(baseRotation, mul(part.rotation, quaternion.RotateY(part.spinAngle)));
            part.worldPosition = parent.worldPosition + mul(part.worldRotation, float3(0f, 1.5f * scale, 0f));
            parts[index] = part;
            float3x3 r = float3x3(part.worldRotation) * scale;
            matrices[index] = float3x4(r.c0, r.c1, r.c2, part.worldPosition);
        }
    }
    [SerializeField]
    Mesh mesh, leafMesh;
    [SerializeField]
    Material material;
    [SerializeField]
    Gradient gradientA, gradientB;
    [SerializeField]
    Color leafColorA, leafColorB;
    [SerializeField, Range(0f, 90f)]
    float maxSagAngleA = 15f, maxSagAngleB = 25f;
    [SerializeField, Range(0f, 90f)]
    float spinVelocityA = 20f, spinVelocityB = 25f;
    [SerializeField, Range(0f, 1f)]
    float reverseSpinChance = 0.25f;

    [SerializeField,Range(2,10)]
    int depth = 3;
    static Quaternion[] rotations = {
        quaternion.identity,
        quaternion.RotateZ(-0.5f*PI), quaternion.RotateZ(0.5f*PI),
        quaternion.RotateX(0.5f*PI), quaternion.RotateX(-0.5f*PI)
    };
    static readonly int matricesId = Shader.PropertyToID("_Matrices");
    static readonly int colorAId = Shader.PropertyToID("_ColorA");
    static readonly int colorBId = Shader.PropertyToID("_ColorB");
    static readonly int sequenceNumbersId = Shader.PropertyToID("_SequenceNumbers");
    static MaterialPropertyBlock propertyBlock;
    NativeArray<FractalPart>[] parts;
    NativeArray<float3x4>[] matrices;
    ComputeBuffer[] matricesBuffers;
    Vector4[] seqenceNumbers;

    FractalPart CreatePart(int childIdx)
    {
        return new FractalPart(){
            rotation = rotations[childIdx],
            maxSagAngle = radians(Random.Range(maxSagAngleA, maxSagAngleB)),
            spinVelocity = (Random.value < reverseSpinChance ? -1f : 1f) * radians(Random.Range(spinVelocityA, spinVelocityB)),
        };
    }

    private void OnEnable() 
    {
        parts = new NativeArray<FractalPart>[depth];
        matrices = new NativeArray<float3x4>[depth];
        matricesBuffers = new ComputeBuffer[depth];
        seqenceNumbers = new Vector4[depth];
        int stride = 12 * 4;
        for(int i=0, length = 1; i<parts.Length; i++, length *= 5)
        {
            parts[i] = new NativeArray<FractalPart>(length, Allocator.Persistent);
            matrices[i] = new NativeArray<float3x4>(length, Allocator.Persistent);
            matricesBuffers[i] = new ComputeBuffer(length, stride);
            seqenceNumbers[i] = new Vector4(Random.value, Random.value, Random.value, Random.value);
        }

        parts[0][0] = CreatePart(0);
        for(int i=1; i<parts.Length; i++)
        {
            var levelParts = parts[i];
            for(int j=0; j<levelParts.Length; j+=5)
            {
                for(int k=0; k<5; k++)
                {
                    levelParts[j+k] = CreatePart(k);
                }
            }
        }

        propertyBlock ??= new MaterialPropertyBlock();
    }

    private void OnDisable() 
    {
        for(int i=0; i<matricesBuffers.Length; i++)
        {
            matricesBuffers[i].Release();
            parts[i].Dispose();
            matrices[i].Dispose();
        }
        parts = null;
        matrices = null;
        matricesBuffers = null;
        seqenceNumbers = null;
    }

    private void OnValidate() {
        if(parts != null && enabled)
        {
            OnDisable();
            OnEnable();
        }
    }

    private void Update() 
    {
        //float spinAngleDelta = 0f;
        //float spinAngleDelta = 0.125f * PI * Time.deltaTime;
        float deltaTime = Time.deltaTime;
        FractalPart rootPart = parts[0][0];
        rootPart.spinAngle += rootPart.spinVelocity * deltaTime;
        rootPart.worldRotation = mul(transform.rotation, mul(rootPart.rotation, quaternion.RotateY(rootPart.spinAngle)));
        rootPart.worldPosition = transform.position;
        parts[0][0] = rootPart;
        float objectScale = transform.lossyScale.x;
        float3x3 r = float3x3(rootPart.worldRotation) * objectScale;
        matrices[0][0] = float3x4(r.c0, r.c1, r.c2, rootPart.worldPosition);

        float scale = objectScale;
        JobHandle jobHandle = default;
        for(int i = 1; i<parts.Length; i++)
        {
            scale *= 0.5f;
            var job = new UpdateFractalLevelJob{
                deltaTime = deltaTime,
                scale = scale,
                parents = parts[i-1],
                parts = parts[i],
                matrices = matrices[i]
            };
            //jobHandle = job.Schedule(parts[i].Length, jobHandle);
            jobHandle = job.ScheduleParallel(parts[i].Length, 5, jobHandle);
        }
        jobHandle.Complete();

        var bounds = new Bounds(rootPart.worldPosition, 3f * objectScale * Vector3.one);
        int leafIndex = matricesBuffers.Length - 1;
        for(int i=0; i<matricesBuffers.Length; i++)
        {
            ComputeBuffer buffer = matricesBuffers[i];
            buffer.SetData(matrices[i]);
            propertyBlock.SetBuffer(matricesId, buffer);
            propertyBlock.SetVector(sequenceNumbersId, seqenceNumbers[i]);
            Mesh instanceMesh;
            Color colorA, colorB;
            if(i == leafIndex)
            {
                colorA = leafColorA;
                colorB = leafColorB;
                instanceMesh = leafMesh;
            }
            else
            {
                float gradientInterpolator = i / (matricesBuffers.Length - 2f);
                colorA = gradientA.Evaluate(gradientInterpolator);
                colorB = gradientB.Evaluate(gradientInterpolator);
                instanceMesh = mesh;
            }
            propertyBlock.SetColor(colorAId, colorA);
            propertyBlock.SetColor(colorBId, colorB);
            Graphics.DrawMeshInstancedProcedural(instanceMesh, 0, material, bounds, buffer.count, propertyBlock);
        }
    }
}