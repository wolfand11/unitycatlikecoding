using UnityEngine;

public class FractalProcedural: MonoBehaviour
{
    struct FractalPart{
        public Vector3 worldPosition;
        public Quaternion worldRotation;
        public Vector3 direction;
        public Quaternion rotation;
        public float spinAngle;
    }
    [SerializeField]
    Mesh mesh;
    [SerializeField]
    Material material;
    [SerializeField,Range(1,10)]
    int depth = 3;
    static Vector3[] directions = {
        Vector3.up, Vector3.right, Vector3.left, Vector3.forward, Vector3.back
    };
    static Quaternion[] rotations = {
        Quaternion.identity,Quaternion.Euler(0f, 0f, -90f),Quaternion.Euler(0f, 0f, 90f),
        Quaternion.Euler(90f, 0f, 0f), Quaternion.Euler(-90f, 0f, 0f)
    };
    static readonly int matricesId = Shader.PropertyToID("_Matrices");
    static MaterialPropertyBlock propertyBlock;
    FractalPart[][] parts;
    Matrix4x4[][] matrices;
    ComputeBuffer[] matricesBuffers;

    FractalPart CreatePart(int childIdx)
    {
        return new FractalPart(){
            direction = directions[childIdx],
            rotation = rotations[childIdx],
        };
    }

    private void OnEnable() 
    {
        parts = new FractalPart[depth][];
        matrices = new Matrix4x4[depth][];
        matricesBuffers = new ComputeBuffer[depth];
        int stride = 16 * 4;
        for(int i=0, length = 1; i<parts.Length; i++, length *= 5)
        {
            parts[i] = new FractalPart[length];
            matrices[i] = new Matrix4x4[length];
            matricesBuffers[i] = new ComputeBuffer(length, stride);
        }

        parts[0][0] = CreatePart(0);
        for(int i=1; i<parts.Length; i++)
        {
            FractalPart[] levelParts = parts[i];
            for(int j=0; j<levelParts.Length; j+=5)
            {
                for(int k=0; k<5; k++)
                {
                    levelParts[j+k] = CreatePart(k);
                }
            }
        }

        propertyBlock ??= new MaterialPropertyBlock();
    }

    private void OnDisable() 
    {
        for(int i=0; i<matricesBuffers.Length; i++)
        {
            matricesBuffers[i].Release();
        }
        parts = null;
        matrices = null;
        matricesBuffers = null;
    }

    private void OnValidate() {
        if(parts != null && enabled)
        {
            OnDisable();
            OnEnable();
        }
    }

    private void Update() 
    {
        float spinAngleDelta = 22.5f * Time.deltaTime;
        FractalPart rootPart = parts[0][0];
        rootPart.spinAngle += spinAngleDelta;
        rootPart.worldRotation = transform.rotation * rootPart.rotation * Quaternion.Euler(0f, rootPart.spinAngle, 0f);
        rootPart.worldPosition = transform.position;
        parts[0][0] = rootPart;
        float objectScale = transform.lossyScale.x;
        matrices[0][0] = Matrix4x4.TRS(rootPart.worldPosition, rootPart.worldRotation, objectScale * Vector3.one);

        float scale = objectScale;
        for(int i = 1; i<parts.Length; i++)
        {
            scale *= 0.5f;
            FractalPart[] parentParts = parts[i-1];
            FractalPart[] levelParts = parts[i];
            Matrix4x4[] levelMatrices = matrices[i];
            for(int j = 0; j<levelParts.Length; j++)
            {
                FractalPart parent = parentParts[j/5];
                FractalPart part = levelParts[j];
                part.spinAngle += spinAngleDelta;
                part.worldRotation = parent.worldRotation * (part.rotation * Quaternion.Euler(0f, part.spinAngle, 0f));
                part.worldPosition = parent.worldPosition + parent.worldRotation * (1.5f * scale * part.direction);
                levelParts[j] = part;
                levelMatrices[j] = Matrix4x4.TRS(part.worldPosition, part.worldRotation, scale * Vector3.one);
            }
        }

        var bounds = new Bounds(rootPart.worldPosition, 3f * objectScale * Vector3.one);
        for(int i=0; i<matricesBuffers.Length; i++)
        {
            ComputeBuffer buffer = matricesBuffers[i];
            buffer.SetData(matrices[i]);
            //material.SetBuffer(matricesId, buffer);
            propertyBlock.SetBuffer(matricesId, buffer);
            Graphics.DrawMeshInstancedProcedural(mesh, 0, material, bounds, buffer.count, propertyBlock);
        }
    }
}