using System.Collections.Generic;
using UnityEngine;

public class FractalFlat : MonoBehaviour
{
    struct FractalPart{
        public Vector3 direction;
        public Quaternion rotation;
        public Transform transform;
    }
    [SerializeField]
    Mesh mesh;
    [SerializeField]
    Material material;
    [SerializeField,Range(1,10)]
    int depth = 3;
    static Vector3[] directions = {
        Vector3.up, Vector3.right, Vector3.left, Vector3.forward, Vector3.back
    };
    FractalPart[][] parts;
    static Quaternion[] rotations = {
        Quaternion.identity,Quaternion.Euler(0f, 0f, -90f),Quaternion.Euler(0f, 0f, 90f),
        Quaternion.Euler(90f, 0f, 0f), Quaternion.Euler(-90f, 0f, 0f)
    };
    FractalPart CreatePart(int levelIndex, int childIdx, float scale)
    {
        var go = new GameObject(string.Format("Fractal Part Lv{0} Idx{1}", levelIndex, childIdx));
        go.transform.localScale = scale * Vector3.one;
        go.transform.SetParent(transform, false);
        var mf = go.AddComponent<MeshFilter>();
        mf.sharedMesh = mesh;
        var mr = go.AddComponent<MeshRenderer>();
        mr.sharedMaterial = material;

        return new FractalPart(){
            direction = directions[childIdx],
            rotation = rotations[childIdx],
            transform = go.transform,
        };
    }

    private void Awake() 
    {
        parts = new FractalPart[depth][];
        for(int i=0, length = 1; i<parts.Length; i++, length *= 5)
        {
            parts[i] = new FractalPart[length];
        }

        float scale = 1f;
        parts[0][0] = CreatePart(0, 0, scale);
        for(int i=1; i<parts.Length; i++)
        {
            scale *= 0.5f;
            FractalPart[] levelParts = parts[i];
            for(int j=0; j<levelParts.Length; j+=5)
            {
                for(int k=0; k<5; k++)
                {
                    levelParts[j+k] = CreatePart(i, k, scale);
                }
            }
        }
    }

    private void Update() 
    {
        Quaternion deltaRotation = Quaternion.Euler(0f, 22.5f * Time.deltaTime, 0f);
        FractalPart rootPart = parts[0][0];
        rootPart.rotation *= deltaRotation;
        rootPart.transform.localRotation = rootPart.rotation;
        parts[0][0] = rootPart;

        for(int i = 1; i<parts.Length; i++)
        {
            FractalPart[] parentParts = parts[i-1];
            FractalPart[] levelParts = parts[i];
            for(int j = 0; j<levelParts.Length; j++)
            {
                Transform parentTransform = parentParts[i/5].transform;
                FractalPart part = levelParts[j];
                part.rotation *= deltaRotation;
                part.transform.localRotation = parentTransform.localRotation * part.rotation;
                part.transform.localPosition = parentTransform.localPosition + 
                    parentTransform.localRotation * 
                    (1.5f * part.transform.localScale.x * part.direction);
                levelParts[j] = part;
            }
        }
    }
}