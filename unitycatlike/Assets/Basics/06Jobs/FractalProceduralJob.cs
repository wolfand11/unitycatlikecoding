using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;

using static Unity.Mathematics.math;
using quaternion = Unity.Mathematics.quaternion;

public class FractalProceduralJob: MonoBehaviour
{
    struct FractalPart{
        public float3 worldPosition;
        public quaternion worldRotation;
        public float3 direction;
        public quaternion rotation;
        public float spinAngle;
    }
    [BurstCompile(FloatPrecision.Standard, FloatMode.Fast, CompileSynchronously = true)]
    struct UpdateFractalLevelJob : IJobFor
    {
        public float spinAngleDelta;
        public float scale;
        [ReadOnly]
        public NativeArray<FractalPart> parents;
        public NativeArray<FractalPart> parts;
        [WriteOnly]
        public NativeArray<float3x4> matrices;
        public void Execute(int index)
        {
            FractalPart parent = parents[index/5];
            FractalPart part = parts[index];
            part.spinAngle += spinAngleDelta;
            part.worldRotation = mul(parent.worldRotation, mul(part.rotation, quaternion.RotateY(part.spinAngle)));
            part.worldPosition = parent.worldPosition + mul(parent.worldRotation, 1.5f * scale * part.direction);
            parts[index] = part;
            float3x3 r = float3x3(part.worldRotation) * scale;
            matrices[index] = float3x4(r.c0, r.c1, r.c2, part.worldPosition);
        }
    }
    [SerializeField]
    Mesh mesh;
    [SerializeField]
    Material material;
    [SerializeField,Range(1,10)]
    int depth = 3;
    static float3[] directions = {
        up(), right(), left(), forward(), back()
    };
    static Quaternion[] rotations = {
        quaternion.identity,
        quaternion.RotateZ(-0.5f*PI), quaternion.RotateZ(0.5f*PI),
        quaternion.RotateX(0.5f*PI), quaternion.RotateX(-0.5f*PI)
    };
    static readonly int matricesId = Shader.PropertyToID("_Matrices");
    static MaterialPropertyBlock propertyBlock;
    NativeArray<FractalPart>[] parts;
    NativeArray<float3x4>[] matrices;
    ComputeBuffer[] matricesBuffers;

    FractalPart CreatePart(int childIdx)
    {
        return new FractalPart(){
            direction = directions[childIdx],
            rotation = rotations[childIdx],
        };
    }

    private void OnEnable() 
    {
        parts = new NativeArray<FractalPart>[depth];
        matrices = new NativeArray<float3x4>[depth];
        matricesBuffers = new ComputeBuffer[depth];
        int stride = 12 * 4;
        for(int i=0, length = 1; i<parts.Length; i++, length *= 5)
        {
            parts[i] = new NativeArray<FractalPart>(length, Allocator.Persistent);
            matrices[i] = new NativeArray<float3x4>(length, Allocator.Persistent);
            matricesBuffers[i] = new ComputeBuffer(length, stride);
        }

        parts[0][0] = CreatePart(0);
        for(int i=1; i<parts.Length; i++)
        {
            var levelParts = parts[i];
            for(int j=0; j<levelParts.Length; j+=5)
            {
                for(int k=0; k<5; k++)
                {
                    levelParts[j+k] = CreatePart(k);
                }
            }
        }

        propertyBlock ??= new MaterialPropertyBlock();
    }

    private void OnDisable() 
    {
        for(int i=0; i<matricesBuffers.Length; i++)
        {
            matricesBuffers[i].Release();
            parts[i].Dispose();
            matrices[i].Dispose();
        }
        parts = null;
        matrices = null;
        matricesBuffers = null;
    }

    private void OnValidate() {
        if(parts != null && enabled)
        {
            OnDisable();
            OnEnable();
        }
    }

    private void Update() 
    {
        float spinAngleDelta = 0.125f * PI * Time.deltaTime;
        FractalPart rootPart = parts[0][0];
        rootPart.spinAngle += spinAngleDelta;
        rootPart.worldRotation = mul(transform.rotation, mul(rootPart.rotation, quaternion.RotateY(rootPart.spinAngle)));
        rootPart.worldPosition = transform.position;
        parts[0][0] = rootPart;
        float objectScale = transform.lossyScale.x;
        float3x3 r = float3x3(rootPart.worldRotation) * objectScale;
        matrices[0][0] = float3x4(r.c0, r.c1, r.c2, rootPart.worldPosition);

        float scale = objectScale;
        JobHandle jobHandle = default;
        for(int i = 1; i<parts.Length; i++)
        {
            scale *= 0.5f;
            var job = new UpdateFractalLevelJob{
                spinAngleDelta = spinAngleDelta,
                scale = scale,
                parents = parts[i-1],
                parts = parts[i],
                matrices = matrices[i]
            };
            //jobHandle = job.Schedule(parts[i].Length, jobHandle);
            jobHandle = job.ScheduleParallel(parts[i].Length, 5, jobHandle);
        }
        jobHandle.Complete();

        var bounds = new Bounds(rootPart.worldPosition, 3f * objectScale * Vector3.one);
        for(int i=0; i<matricesBuffers.Length; i++)
        {
            ComputeBuffer buffer = matricesBuffers[i];
            buffer.SetData(matrices[i]);
            //material.SetBuffer(matricesId, buffer);
            propertyBlock.SetBuffer(matricesId, buffer);
            Graphics.DrawMeshInstancedProcedural(mesh, 0, material, bounds, buffer.count, propertyBlock);
        }
    }
}