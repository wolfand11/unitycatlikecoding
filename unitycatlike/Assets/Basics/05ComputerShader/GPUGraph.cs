using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Graph;

public class GPUGraph : MonoBehaviour
{
    [Range(1,3)]
    public float transitionDuration = 1f;
    public GraphType graphType = GraphType.kX;
    public TransitionMode transitionMode = TransitionMode.kLoop;
    [Range(3,6)]
    public float graphDuration = 3f;
    GraphType preGraphType = GraphType.kX;
    const int maxResolution = 1000;
    [SerializeField, Range(10, maxResolution)]
    int resolution = 10;
    [SerializeField]
    ComputeShader computeShader;
    [SerializeField]
    Mesh mesh;
    [SerializeField]
    Material material;

    ComputeBuffer positionsBuffer;

    static readonly int positionsId = Shader.PropertyToID("_Positions");
    static readonly int resolutionId = Shader.PropertyToID("_Resolution");
    static readonly int stepId = Shader.PropertyToID("_Step");
    static readonly int timeId = Shader.PropertyToID("_Time");

    private void OnEnable() 
    {
        positionsBuffer = new ComputeBuffer(maxResolution * maxResolution, 3*4);
    }

    private void OnDisable() 
    {
        positionsBuffer.Release();
        positionsBuffer = null;
    }

    bool doTransition = false;
    float transitionTimer = 0f;
    void Update()
    {
        PickGraphType(transitionMode, graphDuration, ref transitionTimer, ref graphType);

        if(preGraphType != graphType && !doTransition)
        {
            transitionTimer = 0f;
            doTransition = true;
        }
        UpdateFunctionOnGPU();

        if(preGraphType != graphType)
        {
            transitionTimer += Time.deltaTime;
            if(transitionTimer>= transitionDuration)
            {
                preGraphType = graphType;
                doTransition = false;
            }
        }
    }

    void UpdateFunctionOnGPU()
    {
        float step = 2f / resolution;
        computeShader.SetInt(resolutionId, resolution);
        computeShader.SetFloat(stepId, step);
        computeShader.SetFloat(timeId, Time.time);
        computeShader.SetBuffer((int)graphType, positionsId, positionsBuffer);
        int groups = Mathf.CeilToInt(resolution / 8f);
        computeShader.Dispatch((int)graphType, groups, groups, 1);
        material.SetBuffer(positionsId, positionsBuffer);
        material.SetFloat(stepId, step);

        var bounds = new Bounds(Vector3.zero, Vector3.one * (2f + 2f / resolution));
        Graphics.DrawMeshInstancedProcedural(mesh, 0, material, bounds, resolution*resolution);
    }
}
