using Unity.Collections;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Burst;
using System;
using UnityEngine;

using static Unity.Mathematics.math;

public static partial class Noise
{
    [Serializable]
    public struct Settings
    {
        public int seed;
        [Min(1)]
        public int frequency;
        [Range(1, 6)]
        public int octaves;
        [Range(2, 4)]
        public int lacunarity;
        [Range(0f, 1f)]
        public float persistence;
        public static Settings Default => new Settings
        {
            frequency = 4,
            octaves = 1,
            lacunarity = 2,
            persistence = 0.5f
        };
    }
    public interface INoise
    {
        float4 GetNoise4(float4x3 positions, SmallXXHash4 hash, int frequency);
    }

    public struct LatticeSpan4
    {
        public int4 p0, p1;
        public float4 g0, g1;
        public float4 t;
    }

    public interface ILattice
    {
        LatticeSpan4 GetLatticeSpan4(float4 coordinates, int frequency);
        int4 ValidateSingleStep(int4 points, int freqency);
    }
    public struct LatticeNormal : ILattice
    {
        public LatticeSpan4 GetLatticeSpan4(float4 coordinates, int frequency)
        {
            coordinates *= frequency;
            float4 points = floor(coordinates);
            LatticeSpan4 span;
            span.p0 = (int4)points;
            span.p1 = span.p0 + 1;
            span.g0 = coordinates - span.p0;
            span.g1 = span.g0 - 1f;
            span.t = coordinates - points;
            //span.t = smoothstep(0, 1, span.t);
            span.t = span.t * span.t * span.t * (span.t * (span.t * 6f - 15f) + 10f);
            return span;
        }

        public int4 ValidateSingleStep(int4 points, int freqency)
        {
            return points;
        }
    }

    public struct LatticeTiling : ILattice
    {
        public LatticeSpan4 GetLatticeSpan4(float4 coordinates, int frequency)
        {
            coordinates *= frequency;
            float4 points = floor(coordinates);
            LatticeSpan4 span;
            span.p0 = (int4)points;
            span.p1 = span.p0 + 1;
            span.g0 = coordinates - span.p0;
            span.g1 = span.g0 - 1f;

            //span.p0 %= frequency;
            span.p0 -= (int4)(points / frequency) * frequency;
            span.p0 = select(span.p0, span.p0 + frequency, span.p0<0);
            span.p1 = span.p0 + 1;
            span.p1 = select(span.p1, 0, span.p1==frequency);

            span.t = coordinates - points;
            //span.t = smoothstep(0, 1, span.t);
            span.t = span.t * span.t * span.t * (span.t * (span.t * 6f - 15f) + 10f);
            return span;
        }

        public int4 ValidateSingleStep(int4 points, int freqency)
        {
            return select(select(points, 0, points == freqency), freqency-1, points==-1);
        }
    }

    public delegate JobHandle ScheduleDelegate(
            NativeArray<float3x4> positions, NativeArray<float4> noise, Settings settings, 
            SpaceTRS domainTRS, int resolution, JobHandle dependency
    );

    [BurstCompile(FloatPrecision.Standard, FloatMode.Fast, CompileSynchronously = true)]
    public struct Job<N> : IJobFor where N : struct, INoise
    {
        [ReadOnly]
        public NativeArray<float3x4> positions;
        [WriteOnly]
        public NativeArray<float4> noise;
        [WriteOnly]
        public NativeArray<float3x4> derivative;
        public SmallXXHash4 hash;
        public Settings settings;
        public float3x4 domainTRS;

        public void Execute(int index)
        {
            float4x3 position = domainTRS.TransformVectors(transpose(positions[index]));
            var hash = SmallXXHash4.Seed(settings.seed);
            int frequency = settings.frequency;
            float amplitude = 1f;
            float amplitudeSum = 1f;
            float4 sum = 0f;
            for(int i=0; i<settings.octaves; i++)
            {
                sum += amplitude * default(N).GetNoise4(position, hash + i, frequency);
                amplitudeSum += amplitude;
                frequency *= settings.lacunarity;
                amplitude *= settings.persistence;
            }
            noise[index] = sum / amplitudeSum;
        }

        public static JobHandle ScheduleParallel(NativeArray<float3x4> positions, NativeArray<float4> noise, Settings settings, 
            SpaceTRS domainTRS, int resolution, JobHandle dependency)
        {
            return new Job<N>{
                positions = positions,
                noise = noise,
                settings = settings,
                domainTRS = domainTRS.Matrix,
            }.ScheduleParallel(positions.Length, resolution, dependency);
        }

        public static void Run(NativeArray<float3x4> positions, NativeArray<float4> noise, Settings settings, SpaceTRS domainTRS, int resolution)
        {
            new Job<N>
            {
                positions = positions,
                noise = noise,
                settings = settings,
                domainTRS = domainTRS.Matrix,
            }.Run(positions.Length);
        }
    }
}

