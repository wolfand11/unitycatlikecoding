using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;

using static Unity.Mathematics.math;

public abstract class Visualization : MonoBehaviour
{
    static int configId = Shader.PropertyToID("_Config");
    static int positionsId = Shader.PropertyToID("_Positions");
    static int normalsId = Shader.PropertyToID("_Normals");

    [SerializeField]
    Mesh instanceMesh;
    [SerializeField]
    Material material;
    [SerializeField, Range(1, 512)]
    int resolution = 16;
    [SerializeField, Range(-0.5f, 0.5f)]
    float displacement = 0.1f;

    [SerializeField]
    Shapes.Shape shapeType;
    [SerializeField, Range(0.1f, 10f)]
    float instanceScale = 2f;

    NativeArray<float3x4> positions;
    NativeArray<float3x4> normals;
    ComputeBuffer positionsBuffer;
    ComputeBuffer normalsBuffer;
    MaterialPropertyBlock propertyBlock;
    Bounds bounds;

    bool isDirty;

    protected abstract void EnableVisualization(int dataLength, MaterialPropertyBlock propertyBlock);
    protected abstract void DisableVisualization();
    protected abstract void UpdateVisualization(NativeArray<float3x4> positions, int resolution, JobHandle handle);
    void OnEnable()
    {
        int length = resolution * resolution;
        length = length / 4 + (length & 1);
        positions = new NativeArray<float3x4>(length, Allocator.Persistent);
        normals = new NativeArray<float3x4>(length, Allocator.Persistent);
        positionsBuffer = new ComputeBuffer(length*4, 3*4);
        normalsBuffer = new ComputeBuffer(length*4, 3*4);
        propertyBlock ??= new MaterialPropertyBlock();
        EnableVisualization(length, propertyBlock);
        propertyBlock.SetVector(configId, new Vector4(resolution, instanceScale/resolution, displacement));

        isDirty = true;
    }

    void OnDisable  ()
    {
        positions.Dispose();
        normals.Dispose();
        positionsBuffer.Release();
        normalsBuffer.Release();
        positionsBuffer = null;
        normalsBuffer = null;
        DisableVisualization();
    }

    void OnValidate()
    {
        if(positionsBuffer != null && enabled)
        {
            OnDisable();
            OnEnable();
        }
    }

    void Update() 
    {
        if(isDirty || transform.hasChanged)
        {
            isDirty = false;
            transform.hasChanged = false;

            UpdateVisualization(positions, resolution, Shapes.shapeJobs[(int)shapeType](
                positions, normals, resolution, transform.localToWorldMatrix, default
            ));

            positionsBuffer.SetData(positions.Reinterpret<float3>(3*4*4));
            normalsBuffer.SetData(normals.Reinterpret<float3>(3*4*4));

            propertyBlock.SetBuffer(positionsId, positionsBuffer);
            propertyBlock.SetBuffer(normalsId, normalsBuffer);

            bounds = new Bounds(
                transform.position,
                float3(2f * cmax(abs(transform.lossyScale)) + displacement)
            );
        }
        Graphics.DrawMeshInstancedProcedural(instanceMesh, 0, material, bounds, resolution*resolution, propertyBlock);
    }
}

























