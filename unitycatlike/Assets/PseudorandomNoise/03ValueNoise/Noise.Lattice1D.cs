using Unity.Mathematics;

using static Unity.Mathematics.math;

public static partial class Noise
{
    public struct Lattice1D<L, G> : INoise 
        where L : struct, ILattice where G : struct, IGradient
    {
        public float4 GetNoise4(float4x3 positions, SmallXXHash4 hash, int frequency)
        {
            LatticeSpan4 x = default(L).GetLatticeSpan4(positions.c0, frequency);
            SmallXXHash4 h0 = hash.Eat(x.p0);
            SmallXXHash4 h1 = hash.Eat(x.p1);

            var g = default(G);
            float4 v = lerp(
                g.Evaluate(h0, x.g0), 
                g.Evaluate(h1, x.g1), 
                x.t
            );
            return g.EvaluateAfterInterpolation(v);
        }
    }

    public struct Lattice2D<L, G> : INoise
        where L : struct, ILattice where G : struct, IGradient
    {
        public float4 GetNoise4(float4x3 positions, SmallXXHash4 hash, int frequency)
        {
            LatticeSpan4 x = default(L).GetLatticeSpan4(positions.c0, frequency);
            LatticeSpan4 z = default(L).GetLatticeSpan4(positions.c2, frequency);

            SmallXXHash4 h0 = hash.Eat(x.p0);
            SmallXXHash4 h1 = hash.Eat(x.p1);

            SmallXXHash4 h00 = h0.Eat(z.p0);
            SmallXXHash4 h01 = h0.Eat(z.p1);
            SmallXXHash4 h10 = h1.Eat(z.p0);
            SmallXXHash4 h11 = h1.Eat(z.p1);

            var g = default(G);
            var v = lerp(
                lerp(
                    g.Evaluate(h00, x.g0, z.g0), 
                    g.Evaluate(h01, x.g0, z.g1), 
                    z.t
                ),
                lerp(
                    g.Evaluate(h10, x.g1, z.g0), 
                    g.Evaluate(h11, x.g1, z.g1), 
                    z.t
                ),
                x.t
            );
            return g.EvaluateAfterInterpolation(v);
        }
    }

    public struct Lattice3D<L, G> : INoise
        where L : struct, ILattice where G : struct, IGradient
    {
        public float4 GetNoise4(float4x3 positions, SmallXXHash4 hash, int frequency)
        {
            LatticeSpan4 x = default(L).GetLatticeSpan4(positions.c0, frequency);
            LatticeSpan4 y = default(L).GetLatticeSpan4(positions.c1, frequency);
            LatticeSpan4 z = default(L).GetLatticeSpan4(positions.c2, frequency);

            SmallXXHash4 h0 = hash.Eat(x.p0);
            SmallXXHash4 h1 = hash.Eat(x.p1);

            SmallXXHash4 h00 = h0.Eat(y.p0);
            SmallXXHash4 h01 = h0.Eat(y.p1);
            SmallXXHash4 h10 = h1.Eat(y.p0);
            SmallXXHash4 h11 = h1.Eat(y.p1);

            SmallXXHash4 h000 = h00.Eat(z.p0);
            SmallXXHash4 h001 = h00.Eat(z.p1);
            SmallXXHash4 h010 = h01.Eat(z.p0);
            SmallXXHash4 h011 = h01.Eat(z.p1);
            SmallXXHash4 h100 = h10.Eat(z.p0);
            SmallXXHash4 h101 = h10.Eat(z.p1);
            SmallXXHash4 h110 = h11.Eat(z.p0);
            SmallXXHash4 h111 = h11.Eat(z.p1);

            var g = default(G);
            float4 yz0Result = lerp(
                lerp(
                    g.Evaluate(h000, x.g0, y.g0, z.g0),
                    g.Evaluate(h001, x.g0, y.g0, z.g1),
                    z.t
                ),
                lerp(
                    g.Evaluate(h010, x.g0, y.g1, z.g0),
                    g.Evaluate(h011, x.g0, y.g1, z.g1),
                    z.t
                ),
                y.t
            );
            float4 yz1Result = lerp(
                lerp(
                    g.Evaluate(h100, x.g1, y.g0, z.g0),
                    g.Evaluate(h101, x.g1, y.g0, z.g1),
                    z.t
                ),
                lerp(
                    g.Evaluate(h110, x.g1, y.g1, z.g0),
                    g.Evaluate(h111, x.g1, y.g1, z.g1),
                    z.t
                ),
                y.t
            );
            var v = lerp(yz0Result, yz1Result, x.t);
            return g.EvaluateAfterInterpolation(v);
        }
    }
}