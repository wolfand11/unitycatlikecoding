#ifndef STANDARD_SURFACE_URP_CLOTHWIND
#define STANDARD_SURFACE_URP_CLOTHWIND

//#define XD_REQUIRES_SCREEN_UV
//#define _NORMALMAP

#define XD_CUSTOM_MAT_PROP \
half4 _Color; \
sampler2D _MainTex; \
float4 _MainTex_ST; \

#include "Assets/Rendering/CustomSRP/CustomOfficialURP/StandardSurfaceURP_LitInput.hlsl"
#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
#include "Assets/Rendering/CustomSRP/CustomOfficialURP/StandardSurfaceURP_ForwardInputOutput.hlsl"

void surface_vert (inout Surface_Attributes input) 
{
}

void surface_surf (Surface_Varyings input, inout SurfaceData o) 
{
	o = (SurfaceData)0;
	// Albedo comes from a texture tinted by color
	float4 c = tex2D (_MainTex, input.uv) * _Color * input.vertColor;
	o.albedo = c.rgb;
	//o.albedo = input.vertColor;
	o.metallic = 0.5;
	o.smoothness = 0.5;
	//o.normalTS = half3(0, 1, 0);
	o.normalTS = SampleNormal(input.uv, TEXTURE2D_ARGS(_BumpMap, sampler_BumpMap), _BumpScale);
	//o.occlusion = SampleOcclusion(input.uv);
	//o.emission = SampleEmission(input.uv, _EmissionColor.rgb, TEXTURE2D_ARGS(_EmissionMap, sampler_EmissionMap));
	o.alpha = c.a;
}

#define SURFACE_vert_func surface_vert
#define SURFACE_surf_func surface_surf

#endif // STANDARD_SURFACE_URP_CLOTHWIND
