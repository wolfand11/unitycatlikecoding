using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unity4Noise
{
    [RequireComponent(typeof(ParticleSystem))]
    public class SurfaceFlow : MonoBehaviour
    {
        public SurfaceCreator surface;
        ParticleSystem particleSystem;
        ParticleSystem.Particle[] particles;
        public float flowStrength = 0.1f;


        private void LateUpdate()
        {
            if(particleSystem == null)
            {
                particleSystem = GetComponent<ParticleSystem>();
            }
            if(particles == null || particles.Length < particleSystem.main.maxParticles)
            {
                particles = new ParticleSystem.Particle[particleSystem.main.maxParticles];
            }

            int particleCount = particleSystem.GetParticles(particles);
            PositionParticles();
            particleSystem.SetParticles(particles, particleCount);
        }

        void PositionParticles()
        {
            Quaternion q = Quaternion.Euler(surface.rotation);
            Quaternion qInv = Quaternion.Inverse(q);

            NoiseMethod method = Noise.methods[(int)surface.noiseType][surface.dimensions - 1];
            float amplitude = surface.damping ? surface.strength / surface.noiseSettings.frequency : surface.strength;
            for(int i=0; i<particles.Length; i++)
            {
                Vector3 position = particles[i].position;
                Vector3 point = q * new Vector3(position.x, position.z) + surface.offset;
                NoiseSample noiseSample = Noise.Sum(method, point, surface.noiseSettings.frequency, 
                    surface.noiseSettings.octaves, surface.noiseSettings.lacunarity, surface.noiseSettings.persistence);
                noiseSample = surface.noiseType == NoiseMethodType.Value ? (noiseSample - 0.5f) : (noiseSample * 0.5f);
                noiseSample *= amplitude;
                noiseSample.derivative = qInv * noiseSample.derivative;
                //position += noiseSample.derivative * Time.deltaTime * flowStrength;
                Vector3 curl = new Vector3(noiseSample.derivative.y, 0f, -noiseSample.derivative.x);
                position += curl * Time.deltaTime * flowStrength;
                position.y = noiseSample.value + particleSystem.main.startSize.Evaluate(0);
                particles[i].position = position;
                if(position.x<-0.5f || position.x>0.5f || position.z<-0.5f || position.z>0.5f)
                {
                    particles[i].remainingLifetime = 0f;
                }
            }
        }
    }
}
