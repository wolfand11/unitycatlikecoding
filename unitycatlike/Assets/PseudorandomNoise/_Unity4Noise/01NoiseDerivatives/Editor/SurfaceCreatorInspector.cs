using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Unity4Noise
{
    [CustomEditor(typeof(SurfaceCreator))]
    public class SurfaceCreatorInspector : Editor
    {
        private SurfaceCreator creator;

        private void OnEnable()
        {
            creator = target as SurfaceCreator;
            Undo.undoRedoPerformed += RefreshCreator;
        }

        private void OnDisable()
        {
            Undo.undoRedoPerformed -= RefreshCreator;
        }

        void RefreshCreator()
        {
            if (Application.isPlaying)
            {
                creator.Refresh();
            }
        }

        public override void OnInspectorGUI()
        {
            EditorGUI.BeginChangeCheck();
            DrawDefaultInspector();
            if (EditorGUI.EndChangeCheck())
            {
                RefreshCreator();
            }
        }
    }
}
