using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Collections;
using Unity.Mathematics;

namespace Unity4Noise
{

    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
    public class SurfaceCreator : MonoBehaviour
    {
        Mesh mesh;
        Vector3[] vertices;
        Vector3[] normals;
        Color[] colors;
        Vector2[] uv;
        int[] trianges;

        [Range(1, 200)]
        public int resolution = 10;
        int currentResolution;

        [Range(0f, 1f)]
        public float strength = 1f;
        public bool damping;

        public Noise.Settings noiseSettings = Noise.Settings.Default;
        public Vector3 offset;
        public Vector3 rotation;
        
        [Range(1, 3)]
        public int dimensions = 3;
        public bool tiling;
        public NoiseMethodType noiseType;
        public Gradient coloring;
        public bool coloringForStrength;
        public bool analyticalDerivatives;
        public bool showNormals;


        private void OnEnable()
        {
            if (mesh == null)
            {
                mesh = new Mesh();
                mesh.name = "Surface Mesh";
                GetComponent<MeshFilter>().mesh = mesh;
            }

            Refresh();
        }

        private void OnDisable()
        {
        }

        public void Refresh()
        {
            if (currentResolution != resolution)
            {
                int length = (resolution + 1) * (resolution + 1);
                length = length / 4 + (length & 1);

                CreateGrid();
            }

            Quaternion q = Quaternion.Euler(rotation);
            Quaternion qInv = Quaternion.Inverse(q);
            Vector3 point00 = q * new Vector3(-0.5f,-0.5f) + offset;
            Vector3 point10 = q * new Vector3( 0.5f,-0.5f) + offset;
            Vector3 point01 = q * new Vector3(-0.5f, 0.5f) + offset;
            Vector3 point11 = q * new Vector3( 0.5f, 0.5f) + offset;

            NoiseMethod method = Noise.methods[(int)noiseType][dimensions - 1];
            float stepSize = 1f / resolution;
            float amplitude = damping ? strength / noiseSettings.frequency : strength;
            for (int v = 0, y = 0; y <= resolution; y++)
            {
                Vector3 point0 = Vector3.Lerp(point00, point01, y * stepSize);
                Vector3 point1 = Vector3.Lerp(point10, point11, y * stepSize);
                for (int x = 0; x <= resolution; x++, v++)
                {
                    Vector3 point = Vector3.Lerp(point0, point1, x * stepSize);
                    NoiseSample noiseSample = Noise.Sum(method, point, noiseSettings.frequency, noiseSettings.octaves, noiseSettings.lacunarity, noiseSettings.persistence);
                    noiseSample = noiseType==NoiseMethodType.Value ? (noiseSample - 0.5f) : (noiseSample * 0.5f);

                    if (coloringForStrength)
                    {
                        colors[v] = coloring.Evaluate(noiseSample.value + 0.5f);
                        noiseSample *= amplitude;
                    }
                    else
                    {
                        noiseSample *= amplitude;
                        colors[v] = coloring.Evaluate(noiseSample.value + 0.5f);
                    }

                    noiseSample.derivative = qInv * noiseSample.derivative;
                    if(analyticalDerivatives)
                    {
                        //normals[v] = Vector3.Cross(
                        //    new Vector3(0f, noiseSample.derivative.y, 1f),
                        //    new Vector3(1f, noiseSample.derivative.x, 0f)
                        //).normalized;
                        normals[v] = new Vector3(-noiseSample.derivative.x, 1f, -noiseSample.derivative.y).normalized;
                    }
                    vertices[v].y = noiseSample.value;
                }
            }

            mesh.vertices = vertices;
            mesh.colors = colors;
            mesh.uv = uv;
            mesh.triangles = trianges;
            if(!analyticalDerivatives)
            {
                CalculateNormals();
            }
            mesh.normals = normals;

            //mesh.RecalculateNormals();
            //normals = mesh.normals;
        }

        void CalculateNormals()
        {
            for (int v = 0, z = 0; z <= resolution; z++)
            {
                for (int x = 0; x <= resolution; x++, v++)
                {
                    normals[v] = Vector3.Cross(
                        new Vector3(0f, GetZDerivative(x, z), 1f),
                        new Vector3(1f, GetXDerivative(x, z), 0f)
                    ).normalized;
                }
            }
        }

        float GetXDerivative(int x, int z)
        {
            int rowOffset = z * (resolution + 1);
            float left, right, distance;
            if (x > 0)
            {
                left = vertices[rowOffset + x - 1].y;
                if (x < resolution)
                {
                    right = vertices[rowOffset + x + 1].y;
                    distance = 2f / resolution;
                }
                else
                {
                    right = vertices[rowOffset + x].y;
                    distance = 1f / resolution;
                }
            }
            else
            {
                left = vertices[rowOffset + x].y;
                right = vertices[rowOffset + x + 1].y;
                distance = 1f / resolution;
            }
            return (right - left) / distance;
        }

        float GetZDerivative(int x, int z)
        {
            int rowLength = resolution + 1;
            float back, forward, distance;
            if (z > 0)
            {
                back = vertices[(z - 1) * rowLength + x].y;
                if (z < resolution)
                {
                    forward = vertices[(z + 1) * rowLength + x].y;
                    distance = 2f / resolution;
                }
                else
                {
                    forward = vertices[z * rowLength + x].y;
                    distance = 1f / resolution;
                }
            }
            else
            {
                back = vertices[z * rowLength + x].y;
                forward = vertices[(z + 1) * rowLength + x].y;
                distance = 1f / resolution;
            }
            return (forward - back) / distance;
        }

        void CreateGrid()
        {
            currentResolution = resolution;
            mesh.Clear();

            vertices = new Vector3[(resolution + 1) * (resolution + 1)];
            normals = new Vector3[vertices.Length];
            uv = new Vector2[vertices.Length];
            colors = new Color[vertices.Length];
            float stepSize = 1f / resolution;
            float3x4 tmpPosGroup = 0;
            for (int v = 0, z = 0; z <= resolution; z++)
            {
                for (int x = 0; x <= resolution; x++, v++)
                {
                    float posX = x * stepSize - 0.5f;
                    float posY = 0f;
                    float posZ = z * stepSize - 0.5f;

                    vertices[v] = new Vector3(posX, posY, posZ);
                    normals[v] = Vector3.up;
                    colors[v] = Color.black;
                    uv[v] = new Vector2(x * stepSize, z * stepSize);
                }
            }

            trianges = new int[resolution * resolution * 6];
            for (int t = 0, v = 0, y = 0; y < resolution; y++, v++)
            {
                for (int x = 0; x < resolution; x++, v++, t += 6)
                {
                    trianges[t] = v;
                    trianges[t + 1] = v + resolution + 1;
                    trianges[t + 2] = v + 1;
                    trianges[t + 3] = v + 1;
                    trianges[t + 4] = v + resolution + 1;
                    trianges[t + 5] = v + resolution + 2;
                }
            }
        }

        private void OnDrawGizmosSelected()
        {
            if (showNormals && vertices != null)
            {
                float scale = 1f / resolution;
                Gizmos.color = Color.yellow;
                for (int v = 0; v < vertices.Length; v++)
                {
                    Gizmos.DrawRay(vertices[v], normals[v] * scale);
                }
            }
        }
    }
}
