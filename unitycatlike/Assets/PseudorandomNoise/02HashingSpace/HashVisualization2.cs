using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;

using static Unity.Mathematics.math;

public class HashVisualization2 : MonoBehaviour
{
    [BurstCompile(FloatPrecision.Standard, FloatMode.Fast, CompileSynchronously = true)]
    struct HashJob : IJobFor
    {
        [ReadOnly]
        public NativeArray<float3x4> positions;
        [WriteOnly]
        public NativeArray<uint4> hashes;
        public SmallXXHash4 hash;
        public float3x4 domainTRS;

        float4x3 TransformPositions(float3x4 trs, float4x3 p) => float4x3(
            trs.c0.x * p.c0 + trs.c1.x * p.c1 + trs.c2.x * p.c2 + trs.c3.x,
            trs.c0.y * p.c0 + trs.c1.y * p.c1 + trs.c2.y * p.c2 + trs.c3.y,
            trs.c0.z * p.c0 + trs.c1.z * p.c1 + trs.c2.z * p.c2 + trs.c3.z
        );

        public void Execute(int index)
        {
            float4x3 p = transpose(positions[index]);
            p = TransformPositions(domainTRS, p);

            int4 u = (int4)floor(p.c0);
            int4 v = (int4)floor(p.c1);
            int4 w = (int4)floor(p.c2);

            hashes[index] = hash.Eat(u).Eat(v).Eat(w);
        }
    }

    static int hashesId = Shader.PropertyToID("_Hashes");
    static int configId = Shader.PropertyToID("_Config");
    static int positionsId = Shader.PropertyToID("_Positions");
    static int normalsId = Shader.PropertyToID("_Normals");

    [SerializeField]
    Mesh instanceMesh;
    [SerializeField]
    Material material;
    [SerializeField, Range(1, 512)]
    int resolution = 16;
    [SerializeField]
    int seed;
    [SerializeField, Range(-0.5f, 0.5f)]
    float displacement = 0.1f;

    [SerializeField]
    SpaceTRS domain = new SpaceTRS{
        scale = 8f
    };

    [SerializeField]
    Shapes.Shape shapeType;
    [SerializeField, Range(0.1f, 10f)]
    float instanceScale = 2f;

    NativeArray<uint4> hashes;
    NativeArray<float3x4> positions;
    NativeArray<float3x4> normals;
    ComputeBuffer hashesBuffer;
    ComputeBuffer positionsBuffer;
    ComputeBuffer normalsBuffer;
    MaterialPropertyBlock propertyBlock;
    Bounds bounds;

    bool isDirty;
    void OnEnable()
    {
        int length = resolution * resolution;
        length = length / 4 + (length & 1);
        hashes = new NativeArray<uint4>(length, Allocator.Persistent);
        positions = new NativeArray<float3x4>(length, Allocator.Persistent);
        normals = new NativeArray<float3x4>(length, Allocator.Persistent);
        hashesBuffer = new ComputeBuffer(length*4, 4);
        positionsBuffer = new ComputeBuffer(length*4, 3*4);
        normalsBuffer = new ComputeBuffer(length*4, 3*4);
        propertyBlock ??= new MaterialPropertyBlock();
        propertyBlock.SetVector(configId, new Vector4(resolution, instanceScale/resolution, displacement));

        isDirty = true;
    }

    void OnDisable  ()
    {
        hashes.Dispose();
        positions.Dispose();
        normals.Dispose();
        hashesBuffer.Release();
        positionsBuffer.Release();
        normalsBuffer.Release();
        hashesBuffer = null;
        positionsBuffer = null;
        normalsBuffer = null;
    }

    void OnValidate()
    {
        if(hashesBuffer != null && enabled)
        {
            OnDisable();
            OnEnable();
        }
    }

    void Update() 
    {
        if(isDirty || transform.hasChanged)
        {
            isDirty = false;
            transform.hasChanged = false;

            JobHandle handle = Shapes.shapeJobs[(int)shapeType](positions, normals, resolution, transform.localToWorldMatrix, default);
            new HashJob{
                hashes = hashes,
                positions = positions,
                hash = SmallXXHashX.Seed(seed),
                domainTRS = domain.Matrix
            }.ScheduleParallel(hashes.Length, resolution, handle).Complete();

            hashesBuffer.SetData(hashes.Reinterpret<uint>(4*4));
            positionsBuffer.SetData(positions.Reinterpret<float3>(3*4*4));
            normalsBuffer.SetData(normals.Reinterpret<float3>(3*4*4));

            propertyBlock.SetBuffer(hashesId, hashesBuffer);
            propertyBlock.SetBuffer(positionsId, positionsBuffer);
            propertyBlock.SetBuffer(normalsId, normalsBuffer);

            bounds = new Bounds(
                transform.position,
                float3(2f * cmax(abs(transform.lossyScale)) + displacement)
            );
        }
        Graphics.DrawMeshInstancedProcedural(instanceMesh, 0, material, bounds, resolution*resolution, propertyBlock);
    }
}

























