Shader "Custom/Flow/DirectionalFlow2"
{
    Properties
    {
        _Color("Color", Color) = (1,1,1,1)
        [NoScaleOffset]_MainTex ("Derivative (AG) Height(B)", 2D) = "black" {}
        [NoScaleOffset]_FlowMap ("Flow (RG, A noise)", 2D) = "black" {}
        [Toggle(_NORMALMAP)] _UseNormalMap("Use NormalMap", Float) = 0
        _Tiling("Tiling", Float) = 1 
		_FlowStrength("Flow Strength", Range(0, 2)) = 1 
        _FlowOffset("Flow Offset", Float) = 0
        _HeightScale("Height Scale, Constant", Float) = 0.25
        _HeightScaleModulated("Height Scale, Modulated", Float) = 0.75
        _TimeSpeed("Time Speed", Range(0, 2)) = 1
        _Smoothness("Smoothness", Range(0, 1)) = 0.5
        _Metallic("Metallic", Range(0, 1)) = 0.0

		// Blending state
        [HideInInspector] _Mode ("__mode", Float) = 0.0
        [HideInInspector] _SrcBlend ("__src", Float) = 1.0
        [HideInInspector] _DstBlend ("__dst", Float) = 0.0
        [HideInInspector] _ZWrite ("__zw", Float) = 1.0

		[Enum(UnityEngine.Rendering.CullMode)] _Cull("Cull Mode", Float) = 2
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
			Name "ForwardLit"
            Tags { "LightMode" = "UniversalForward"}

            Blend[_SrcBlend][_DstBlend]
            ZWrite[_ZWrite]
            Cull[_Cull]

            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag

			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma shader_feature_local _NORMALMAP

            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Input.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/UnityInput.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/SpaceTransforms.hlsl"

            struct appdata
            {
                float4 positionOS   : POSITION;
                float2 uv           : TEXCOORD0;
                float3 normalOS     : NORMAL;
				float4 tangentOS    : TANGENT;
            };

            struct v2f
            {
                float2 uv                       : TEXCOORD0;
                float4 positionCS               : SV_POSITION;
                float3 positionWS               : TEXCOORD1;
                float3 normalWS                 : TEXCOORD2;
				float4 tangentWS                : TEXCOORD3;    // xyz: tangent, w: sign
				float3 viewDirWS                : TEXCOORD4;
            };

            CBUFFER_START(UnityPerMaterial)
				float4 _MainTex_ST;
				half4 _Color;
                half _Tiling;
                half _TimeSpeed;
                half _FlowStrength;
                half _FlowOffset;
                half _HeightScale;
                half _HeightScaleModulated;
                half _Smoothness;
				half _Metallic;
                half4 _CustomTime;
			CBUFFER_END

            TEXTURE2D(_MainTex); SAMPLER(sampler_MainTex);
            TEXTURE2D(_FlowMap); SAMPLER(sampler_FlowMap);

            v2f vert (appdata v)
            {
                v2f o;
                o.positionWS = TransformObjectToWorld(v.positionOS);
				o.positionCS = TransformWorldToHClip(o.positionWS);
                o.normalWS = TransformObjectToWorldNormal(v.normalOS);
                o.tangentWS = float4(TransformObjectToWorldDir(v.tangentOS.xyz), v.tangentOS.w);
                o.viewDirWS = _WorldSpaceCameraPos - o.positionWS;

                o.uv = v.uv;
                return o;
            }

            void InitializeInputData(v2f input, half3 normalTS, out InputData inputData)
			{
				inputData = (InputData)0;

			#if defined(REQUIRES_WORLD_SPACE_POS_INTERPOLATOR)
				inputData.positionWS = input.positionWS;
			#endif

				half3 viewDirWS = SafeNormalize(input.viewDirWS);
			#if defined(_NORMALMAP) || defined(_DETAIL)
				float sgn = input.tangentWS.w;      // should be either +1 or -1
				float3 bitangent = sgn * cross(input.normalWS.xyz, input.tangentWS.xyz);
				inputData.normalWS = TransformTangentToWorld(normalTS, half3x3(input.tangentWS.xyz, bitangent.xyz, input.normalWS.xyz));
			#else
				inputData.normalWS = input.normalWS;
			#endif

				inputData.normalWS = NormalizeNormalPerPixel(inputData.normalWS);
				inputData.viewDirectionWS = viewDirWS;

			#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
				inputData.shadowCoord = input.shadowCoord;
			#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
				inputData.shadowCoord = TransformWorldToShadowCoord(inputData.positionWS);
			#else
				inputData.shadowCoord = float4(0, 0, 0, 0);
			#endif

				//inputData.fogCoord = input.fogFactorAndVertexLight.x;
				//inputData.vertexLighting = input.fogFactorAndVertexLight.yzw;
				//inputData.bakedGI = SAMPLE_GI(input.lightmapUV, input.vertexSH, inputData.normalWS);
				//inputData.normalizedScreenSpaceUV = GetNormalizedScreenSpaceUV(input.positionCS);
				//inputData.shadowMask = SAMPLE_SHADOWMASK(input.lightmapUV);
			}

            float3 UnpackDerivativeHeight(float4 textureData)
            {
                float3 dh = textureData.agb;
                dh.xy = dh.xy * 2 - 1;
                return dh;
            }

            float2 DirectionalFlowUV(float2 uv, float2 flowDir, float tiling, float time, out float2x2 rotation)
            {
                float2 dir = normalize(flowDir);
                //uv -= time * flowDir;
                rotation = float2x2(dir.y, -dir.x, dir.x, dir.y);
                uv = mul(rotation, uv);
                return uv * tiling;
            }

            half4 frag(v2f i) : SV_Target
            {
                float time = _CustomTime.y * _TimeSpeed;
                //float2 uvFlow = DirectionalFlowUV(i.uv, float2(1, 1), _Tiling, time);
				float2x2 derivRotation;
                float2 uvFlow = DirectionalFlowUV(i.uv, float2(sin(time), cos(time)), _Tiling, time, derivRotation);

                float3 dh = UnpackDerivativeHeight(SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, uvFlow));
                dh.xy = mul(derivRotation, dh.xy);
                half4 albedo = dh.z * dh.z * _Color;
                //half4 albedo = half4(dh.rg, 0 , 0);
                float3 normalTS = normalize(float3(-dh.xy, 1));

                InputData inputData;
                InitializeInputData(i, normalTS, inputData);
                half4 col = UniversalFragmentPBR(inputData, albedo, _Metallic, 0, _Smoothness, 0, 0, 1);
                return col;
            }
            ENDHLSL
        }
    }
}
