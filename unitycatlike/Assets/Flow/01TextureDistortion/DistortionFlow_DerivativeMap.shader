Shader "Custom/Flow/DistortionFlow_DerivativeMap"
{
    Properties
    {
        _Color("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo", 2D) = "white" {}
        [NoScaleOffset]_FlowMap ("Flow (RG, A noise)", 2D) = "black" {}
        [Toggle(_NORMALMAP)] _UseNormalMap("Use NormalMap", Float) = 0
		[NoScaleOffset]_DerivHeightMap("Derivative (AG) Height(B)", 2D) = "black" {}
        _UJump("U jump per phase", Range(-0.25, 0.25)) = 0.25
        _VJump("V jump per phase", Range(-0.25, 0.25)) = 0.25
        _Tiling("Tiling", Float) = 1 
		_FlowStrength("Flow Strength", Range(0, 2)) = 1 
        _FlowOffset("Flow Offset", Float) = 0
        _HeightScale("Height Scale, Constant", Float) = 0.25
        _HeightScaleModulated("Height Scale, Modulated", Float) = 0.75
        _TimeSpeed("Time Speed", Range(0, 2)) = 1
        _Smoothness("Smoothness", Range(0, 1)) = 0.5
        _Metallic("Metallic", Range(0, 1)) = 0.0

		// Blending state
        [HideInInspector] _Mode ("__mode", Float) = 0.0
        [HideInInspector] _SrcBlend ("__src", Float) = 1.0
        [HideInInspector] _DstBlend ("__dst", Float) = 0.0
        [HideInInspector] _ZWrite ("__zw", Float) = 1.0

		[Enum(UnityEngine.Rendering.CullMode)] _Cull("Cull Mode", Float) = 2
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
			Name "ForwardLit"
            Tags { "LightMode" = "UniversalForward"}

            Blend[_SrcBlend][_DstBlend]
            ZWrite[_ZWrite]
            Cull[_Cull]

            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag

			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma shader_feature_local _NORMALMAP

            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Input.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/UnityInput.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/SpaceTransforms.hlsl"

            struct appdata
            {
                float4 positionOS   : POSITION;
                float2 uv           : TEXCOORD0;
                float3 normalOS     : NORMAL;
				float4 tangentOS    : TANGENT;
            };

            struct v2f
            {
                float2 uv                       : TEXCOORD0;
                float4 positionCS               : SV_POSITION;
                float3 positionWS               : TEXCOORD1;
                float3 normalWS                 : TEXCOORD2;
				float4 tangentWS                : TEXCOORD3;    // xyz: tangent, w: sign
				float3 viewDirWS                : TEXCOORD4;
            };

            CBUFFER_START(UnityPerMaterial)
				float4 _MainTex_ST;
				half4 _Color;
                half _UJump;
                half _VJump;
                half _Tiling;
                half _TimeSpeed;
                half _FlowStrength;
                half _FlowOffset;
                half _HeightScale;
                half _HeightScaleModulated;
                half _Smoothness;
				half _Metallic;
                half4 _CustomTime;
			CBUFFER_END

            TEXTURE2D(_MainTex); SAMPLER(sampler_MainTex);
            TEXTURE2D(_FlowMap); SAMPLER(sampler_FlowMap);
            TEXTURE2D(_DerivHeightMap); SAMPLER(sampler_DerivHeightMap);

            v2f vert (appdata v)
            {
                v2f o;
                o.positionWS = TransformObjectToWorld(v.positionOS);
				o.positionCS = TransformWorldToHClip(o.positionWS);
                o.normalWS = TransformObjectToWorldNormal(v.normalOS);
                o.tangentWS = float4(TransformObjectToWorldDir(v.tangentOS.xyz), v.tangentOS.w);
                o.viewDirWS = _WorldSpaceCameraPos - o.positionWS;

                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            void InitializeInputData(v2f input, half3 normalTS, out InputData inputData)
			{
				inputData = (InputData)0;

			#if defined(REQUIRES_WORLD_SPACE_POS_INTERPOLATOR)
				inputData.positionWS = input.positionWS;
			#endif

				half3 viewDirWS = SafeNormalize(input.viewDirWS);
			#if defined(_NORMALMAP) || defined(_DETAIL)
				float sgn = input.tangentWS.w;      // should be either +1 or -1
				float3 bitangent = sgn * cross(input.normalWS.xyz, input.tangentWS.xyz);
				inputData.normalWS = TransformTangentToWorld(normalTS, half3x3(input.tangentWS.xyz, bitangent.xyz, input.normalWS.xyz));
			#else
				inputData.normalWS = input.normalWS;
			#endif

				inputData.normalWS = NormalizeNormalPerPixel(inputData.normalWS);
				inputData.viewDirectionWS = viewDirWS;

			#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
				inputData.shadowCoord = input.shadowCoord;
			#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
				inputData.shadowCoord = TransformWorldToShadowCoord(inputData.positionWS);
			#else
				inputData.shadowCoord = float4(0, 0, 0, 0);
			#endif

				//inputData.fogCoord = input.fogFactorAndVertexLight.x;
				//inputData.vertexLighting = input.fogFactorAndVertexLight.yzw;
				//inputData.bakedGI = SAMPLE_GI(input.lightmapUV, input.vertexSH, inputData.normalWS);
				//inputData.normalizedScreenSpaceUV = GetNormalizedScreenSpaceUV(input.positionCS);
				//inputData.shadowMask = SAMPLE_SHADOWMASK(input.lightmapUV);
			}

            float3 UnpackDerivativeHeight(float4 textureData)
            {
                float3 dh = textureData.agb;
                dh.xy = dh.xy * 2 - 1;
                return dh;
            }

            float2 FlowUV(float2 uv, float2 flowDir, float time)
            {
                float progress = frac(time);
                return uv + flowDir * progress;
            }

			float3 FlowUVW(float2 uv, float2 flowDir, float2 jump, float flowOffset, float tiling, float time, bool flowB)
            {
                float phaseOffset = flowB ? 0.5 : 0;
                float progress = frac(time + phaseOffset);
                float3 uvw;
                uvw.xy = uv - flowDir * (progress + flowOffset);
                uvw.xy *= tiling;
                uvw.xy += phaseOffset;
                uvw.xy += (time - progress) * jump;
                uvw.z = 1 - abs(1 - 2 * progress);
                return uvw;
            }

            half4 frag(v2f i) : SV_Target
            {
                //float2 flowDir = half2(0, 1);
                float3 flowDirSpeed = SAMPLE_TEXTURE2D(_FlowMap, sampler_FlowMap, i.uv);
                float2 flowDir = flowDirSpeed.xy * 2 - 1;
                flowDir = flowDir * _FlowStrength;
                float flowSpeed = flowDirSpeed.z * _FlowStrength;

                float timeOffset = SAMPLE_TEXTURE2D(_FlowMap, sampler_FlowMap, i.uv).a;
                //float timeOffset = 0;

                float time = (_CustomTime.y + timeOffset) * _TimeSpeed;
                float2 jump = float2(_UJump, _VJump);
                float3 uvwA = FlowUVW(i.uv, flowDir, jump, _FlowOffset, _Tiling, time, false);
                float3 uvwB = FlowUVW(i.uv, flowDir, jump, _FlowOffset, _Tiling, time, true);

                float finalHeightScale = flowSpeed * _HeightScaleModulated + _HeightScale;
                float3 dhA = UnpackDerivativeHeight(SAMPLE_TEXTURE2D(_DerivHeightMap, sampler_DerivHeightMap, uvwA.xy)) * (uvwA.z * finalHeightScale);
                float3 dhB = UnpackDerivativeHeight(SAMPLE_TEXTURE2D(_DerivHeightMap, sampler_DerivHeightMap, uvwB.xy)) * (uvwB.z * finalHeightScale);
                //half3 normalTS = half3(0, 0, 1);
                half3 normalTS = normalize(float3(-(dhA.xy + dhB.xy), 1));
                //return half4((normalTS + 1) / 2, 1);

                half4 albedoA = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, uvwA.xy) * uvwA.z;
                half4 albedoB = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, uvwB.xy) * uvwB.z;
                half4 albedo = (albedoA + albedoB) * _Color;
                //half4 albedo = pow(dhA.z + dhB.z, 2);


                InputData inputData;
                InitializeInputData(i, normalTS, inputData);
                //return half4((inputData.normalWS + 1) / 2, 1);
                half4 col = UniversalFragmentPBR(inputData, albedo, _Metallic, 0, _Smoothness, 0, 0, 1);
                return col;
            }
            ENDHLSL
        }
    }
}
