﻿Shader "Unlit/Mirror"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_Roughness("Roughness", Range(0, 1)) = 0.5
    }

    SubShader
    {
        Tags { "RenderType"="Transparent" }
        LOD 100

        Pass
        {
			Tags 
			{ 
				"LightMode"="ForwardBase" 
			}
			Blend SrcAlpha OneMinusSrcAlpha
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
			#include "HLSLSupport.cginc"
			#include "UnityStandardConfig.cginc"
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
				float3 worldNormal : TEXCOORD1;
				float3 worldPos : TEXCOORD5;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float _Roughness;

            v2f vert (appdata v)
            {
                v2f o;
				UNITY_INITIALIZE_OUTPUT(v2f, o);
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
	        	o.worldNormal = mul(v.normal, (float3x3)unity_WorldToObject);
	        	o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                return o;
            }

			half3 BoxProjection(half3 dir, half3 pos, half4 cubemapPos, half3 boxMin, half3 boxMax)
			{
				if (cubemapPos.w > 0)
				{
					half3 factors = ((dir > 0 ? boxMax : boxMin) - pos) / dir;
					half realFactor = min(min(factors.x, factors.y), factors.z);
					return dir * realFactor + (pos - cubemapPos);
				}
				return dir;
			}

            fixed4 frag (v2f i) : SV_Target
            {
				half3 viewDir = normalize(_WorldSpaceCameraPos.xyz - i.worldPos);
				i.worldNormal = normalize(i.worldNormal);
				half3 reflectDir = reflect(-viewDir, i.worldNormal);
				half3 reflectDir1 = BoxProjection(reflectDir, i.worldPos,
					unity_SpecCube0_ProbePosition,
					unity_SpecCube0_BoxMin,
					unity_SpecCube0_BoxMax);
				half4 envColor = UNITY_SAMPLE_TEXCUBE_LOD(unity_SpecCube0, reflectDir1, _Roughness*UNITY_SPECCUBE_LOD_STEPS);
				envColor.rgb = DecodeHDR(envColor, unity_SpecCube0_HDR);
                return fixed4(envColor.rgb, saturate(dot(envColor.rgb,1)));
            }
            ENDCG
        }
    }
}
