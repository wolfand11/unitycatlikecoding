﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCropTex : MonoBehaviour
{
    public bool showSrcTex = false;
    public bool doTest = false;
    public Texture2D srcTexture;
    public Rect cropRect;

    SpriteRenderer sr;
    private void Start()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if(showSrcTex)
        {
            ShowTex(srcTexture);
            showSrcTex = false;
        }
        if(doTest)
        {
            var newTex = GenTexture.CropTexture2D(srcTexture, cropRect);
            ShowTex(newTex);
            doTest = false;
        }
    }

    void ShowTex(Texture2D tex)
    {
        if(tex!= null)
        {
            sr.sprite = Sprite.Create(
                tex, 
                new Rect(0,0, tex.width, tex.height),
                new Vector2(0.5f, 0.5f)
            );
        }
    }
}
