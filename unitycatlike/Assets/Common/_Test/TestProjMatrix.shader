﻿Shader "Unlit/TestProjMatrix"
{
    Properties
    {
		_Color("Color", Color) = (1,1,1,1)
		[Enum(UnityEngine.Rendering.CullMode)] _Cull ("Cull Mode", Float) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
			Cull [_Cull]
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			#include "UnityCG.cginc"


            struct appdata
            {
                float4 vertex : POSITION;
				float3 normal : NORMAL;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
				float3 wNormal : TEXCOORD1;
				float4 wPos : TEXCOORD2;
				float4 vPos : TEXCOORD3;
            };

            float4 _Color;

            v2f vert (appdata v)
            {
                v2f o;
                o.uv = v.uv;
				o.wNormal = UnityObjectToWorldNormal(v.normal);
				o.wPos = mul(unity_ObjectToWorld, v.vertex);
				o.vPos = mul(UNITY_MATRIX_V, o.wPos);
                o.vertex = mul(UNITY_MATRIX_P, o.vPos);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				float ndl = dot(WorldSpaceLightDir(i.wPos), normalize(i.wNormal));
				float3 col = saturate(_Color.rgb + i.vPos.xyz + i.wPos.xyz);
                return float4(col,1) * ndl + 0.1;
            }
            ENDCG
        }
    }
}
