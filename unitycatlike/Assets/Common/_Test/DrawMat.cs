﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawMat : MonoBehaviour
{
    public Material mat;
    public bool isGeneratePng = false;
    void Start()
    {
        isGeneratePng = false;
    }

    void Update()
    {
        if(isGeneratePng && mat!=null)
        {
            isGeneratePng = false;
            Texture2D tex = mat.mainTexture as Texture2D;
            if(tex!=null)
            {
                DrawPng(tex.width, tex.height, Application.dataPath + "/../test.png", mat);
            }
        }
    }

      public static void DrawPng(int pngWidth, int pngHeight, string imgPath, Material mat=null)
      {
          if (pngWidth < 1 || pngHeight < 1)
          {
              Debug.LogWarning(string.Format("png size error! w={0} h={1}", pngWidth, pngHeight));
              return;
          }
          if( string.IsNullOrEmpty(imgPath))
          {
              Debug.LogError("imgName is nullOrEmpty");
              return;
          }

          Rect rect = new Rect(0, 0, pngWidth, pngHeight);
          RenderTexture rt = new RenderTexture(pngWidth, pngHeight, 0);
          Graphics.Blit(mat.mainTexture, rt, mat);

          RenderTexture.active = rt;
          Texture2D tex = new Texture2D((int)rt.width, (int)rt.height, TextureFormat.RGB24, false);
          tex.ReadPixels(rect, 0, 0);
          tex.Apply();
          RenderTexture.active = null;

          System.IO.File.WriteAllBytes(imgPath, tex.EncodeToPNG());
          GameObject.Destroy(rt);
      }
}
