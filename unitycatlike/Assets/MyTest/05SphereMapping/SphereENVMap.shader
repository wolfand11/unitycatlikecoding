﻿Shader "Custom/Test/SphereENVMap"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_CubeTex("Cube Texture", Cube) = "gray" {}
        _Roughness("Roughness", Range(0,1)) = 0.5
		[KeywordEnum(None, Cube, WNormal, WReflect, VImageNormal, WParaboloid, WParaboloidRefect, VParaboloid)] _EnvUV("Env UV Mode", Float) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
			Cull Off
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			#pragma multi_compile _ _ENVUV_WNORMAL _ENVUV_WREFLECT _ENVUV_VIMAGENORMAL _ENVUV_VPARABOLOID _ENVUV_WPARABOLOID _ENVUV_WPARABOLOIDREFECT _ENVUV_CUBE

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float4 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
				float3 wNormal : TEXCOORD1;
				float3 wPos : TEXCOORD2;
#if SHADER_STAGE_FRAGMENT
				half cullFace : VFACE;
#endif
            };

            sampler2D _MainTex;
			UNITY_DECLARE_TEXCUBE(_CubeTex);
            float4 _MainTex_ST;
			float _Roughness;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.wPos = mul(unity_ObjectToWorld, v.vertex);
				o.wNormal = mul(v.normal, (float3x3)unity_WorldToObject);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				half frontFace = 1;
#if SHADER_STAGE_FRAGMENT
				frontFace = i.cullFace > 0 ? 1 : -1;
#endif

				float3 viewDir = normalize(_WorldSpaceCameraPos.xyz - i.wPos);
				i.wNormal = normalize(i.wNormal) * frontFace;
				float3 reflectDir = i.wNormal * dot(viewDir, i.wNormal) * 2 - viewDir;
				float2 uv = i.wNormal.xy*0.5 + 0.5;
#if defined(_ENVUV_WREFLECT)
				uv = reflectDir.xy*0.5 + 0.5;
#elif defined(_ENVUV_VIMAGENORMAL)
				float3 imgViewDir = float3(0, 0, 1);
				float3 imgReflectDir = mul(reflectDir, (float3x3)unity_MatrixInvV);
				float3 imgNormal = imgReflectDir + imgViewDir;
				uv = imgNormal.xy / length(imgNormal) * 0.5 + 0.5;
#elif defined(_ENVUV_WPARABOLOID)
				uv = i.wNormal.xy / (abs(i.wNormal.z) + 1) * 0.5 + 0.5;
#elif defined(_ENVUV_WPARABOLOIDREFECT)
				uv = reflectDir.xy / (abs(reflectDir.z) + 1) * 0.5 + 0.5;
#elif defined(_ENVUV_VPARABOLOID)
				float3 imgViewDir = float3(0, 0, 1);
				float3 imgReflectDir = mul(reflectDir, (float3x3)unity_MatrixInvV);
				float3 imgNormal = imgReflectDir + imgViewDir;
				uv = imgNormal.xy / (imgNormal.z + 1) * 0.5 + 0.5;
#endif
				half percepturalRoughness = _Roughness * (1.7 - 0.7*_Roughness);
				half mip = percepturalRoughness * 6;

#if defined(_ENVUV_CUBE)
				fixed4 col = UNITY_SAMPLE_TEXCUBE_LOD(_CubeTex, reflectDir, mip);
#else
				fixed4 col = tex2Dlod(_MainTex, float4(uv, 0, mip));
#endif
				col.rgb = DecodeHDR(col, unity_SpecCube0_HDR);
#if defined(UNITY_USE_NATIVE_HDR)
				col = 1;
#else
				col = 0;
#endif
                return lerp(0.5, col, frontFace==1);
            }
            ENDCG
        }
    }
}
