﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShaderLODTest : MonoBehaviour
{
    public enum ShaderLOD
    {
        VertexLit_100 = 100,
        DecalReflectiveVertexLit_150 = 150,
        Diffuse_200 = 200,
        DiffuseDetailReflectiveBumpedUnlitRBVertexLit_250 = 250,
        BumpedAndSpecular_300 = 300,
        BumpedSpecular_400 = 400,
        Parallax_500 = 500,
        ParallaxSpecular_600 = 600
    }

    Shader shader;
    public ShaderLOD shaderLOD = ShaderLOD.ParallaxSpecular_600;
    public bool changeGlobalShaderLOD = false;
    public bool resetGlobalShaderLOD = false;
    public bool resetShaderLOD = false;
    int originGlobalShaderMaxLOD = (int)ShaderLOD.ParallaxSpecular_600;


    void Start()
    {
        var renderer = GetComponent<Renderer>();
        if (renderer == null) return;
        var mat = renderer.sharedMaterial;
        if (mat == null) return;
        shader = mat.shader;
        Debug.Log("shader.maximumLOD = " + shader.maximumLOD);
        Debug.Log("originGlobalShaderMaxLOD = " + Shader.globalMaximumLOD);
        originGlobalShaderMaxLOD = Shader.globalMaximumLOD;
    }

    void Update()
    {
        if(shader != null)
        {
            if(changeGlobalShaderLOD)
            {
                Shader.globalMaximumLOD = (int)shaderLOD;
            }
            else
            {
                shader.maximumLOD = (int)shaderLOD;
            }
        }

        if(resetGlobalShaderLOD)
        {
            resetGlobalShaderLOD = false;
            Shader.globalMaximumLOD = originGlobalShaderMaxLOD;
        }

        if(resetShaderLOD)
        {
            resetShaderLOD = false;
            // 该 shader 恢复受 Shader.globalMaximumLOD 影响
            shader.maximumLOD = -1;
        }
    }
}
