﻿Shader "Unlit/ShaderLODTest"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
	CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "AutoLight.cginc"
		struct appdata
		{
			float4 vertex : POSITION;
			float3 normal : NORMAL;
			float2 uv : TEXCOORD0;
		};

		struct v2f
		{
			float4 vertex : SV_POSITION;
			float4 worldPos : TEXCOORD1;
			float3 worldNormal : TEXCOORD2;
		};

		v2f vert (appdata v)
		{
			v2f o;
			o.vertex = UnityObjectToClipPos(v.vertex);
			o.worldPos = mul(unity_ObjectToWorld, v.vertex);
			o.worldNormal = mul(v.normal, (float3x3)unity_WorldToObject);
			return o;
		}

		UnityLight CreateLight(v2f i)
		{
			UnityLight light;
		#if defined(POINT) || defined(SPOT) || defined(POINT_COOKIE)
			half3 lightVect = _WorldSpaceLightPos0.xyz - i.worldPos;
		#else
			half3 lightVect = _WorldSpaceLightPos0.xyz;
		#endif
			light.dir = normalize(lightVect);
			light.color = _LightColor0;
			light.ndotl = DotClamped(i.worldNormal, light.dir);
			return light;
		}
	ENDCG

    SubShader
	{
		Tags { "RenderType" = "Opaque" }
		LOD 400

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 col = fixed4(1,0,0,1);
				UnityLight light = CreateLight(i);
				col.rgb = col.rgb * light.color * light.ndotl;
				return col;
			}
			ENDCG
		}
	}

	SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 300

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            fixed4 frag (v2f i) : SV_Target
            {
				fixed4 col = fixed4(0,1,0,1);
				UnityLight light = CreateLight(i);
				col.rgb = col.rgb * light.color * light.ndotl;
                return col;
            }
            ENDCG
        }
    }
}
