﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class BoundingBoxAlgorithm : MonoBehaviour
{
    public enum TestMode
    {
        Intersect,
        IntersectX
    }

    public Vector3 minPos = Vector3.one;
    public Vector3 maxPos = Vector3.one * 4;
    public Vector3 rayOrigin;
    public Vector3 rayDir = Vector3.up;
    public float rayMax = 10;

    public TestMode testMode = TestMode.Intersect;
    public bool doTest = false;
    void Update()
    {
        if(doTest)
        {
            if(testMode==TestMode.Intersect)
            {
                DoIntersect();
            }
            else if(testMode==TestMode.IntersectX)
            {
                DoIntersectX();
            }

            doTest = false;
        }
    }

    void DoIntersect()
    {
        float tmp = 0;
        float t0 = 0;
        float t1 = rayMax;
        for(int i=0; i<3; i++)
        {
            float invRayDir = 1 / rayDir[i];
            float tNear = (minPos[i] - rayOrigin[i]) * invRayDir;
            float tFar = (maxPos[i] - rayOrigin[i]) * invRayDir;
            if(tNear>tFar)
            {
                tmp = tNear;
                tNear = tFar;
                tFar = tNear;
            }
            tFar *= 1 + 2 * Gamma(3);
            t0 = tNear > t0 ? tNear : t0;
            t1 = tFar < t1 ? tFar : t1;
            if (t0 > t1)
            {
                Debug.Log(string.Format("False ({0}) t0({1}) > t1({2})", i, t0, t1));
            }
        }
        Debug.Log(string.Format("True t0({0}) t1({1})", t0, t1));
    }

    void DoIntersectX()
    {
        var bounds = new Vector3[] { minPos, maxPos };
        var invDir = Vector3.one;
        invDir.x /= rayDir.x;
        invDir.y /= rayDir.y;
        invDir.z /= rayDir.z;
        var dirIsNeg = new int[]{ rayDir.x<0 ? 1: 0, rayDir.y<0 ? 1: 0, rayDir.z<0 ? 1: 0};
        float tMin = (bounds[dirIsNeg[0]].x - rayOrigin.x) * invDir.x;
        float tMax = (bounds[1-dirIsNeg[0]].x - rayOrigin.x) * invDir.x;
        float tyMin = (bounds[dirIsNeg[1]].y - rayOrigin.y) * invDir.y;
        float tyMax = (bounds[1-dirIsNeg[1]].y - rayOrigin.y) * invDir.y;
        tMax *= 1 + 2 * Gamma(3);
        tyMax *= 1 + 2 * Gamma(3);
        if (tMin > tyMax || tyMin > tMax)
        {
            Debug.Log(string.Format("False  tMin({0}) > tyMax({3}) || tyMin({2}) > tMax({1})", tMin, tMax, tyMin, tyMax));
        }
        float tzMin = (bounds[dirIsNeg[2]].z - rayOrigin.z) * invDir.z;
        float tzMax = (bounds[1-dirIsNeg[2]].z - rayOrigin.z) * invDir.z;
        tzMax *= 1 + 2 * Gamma(3);
        if (tMin > tzMax || tzMin > tMax)
        {
            Debug.Log(string.Format("False  tMin({0}) > tzMax({3}) || tzMin({2}) > tMax({1})", tMin, tMax, tzMin, tzMax));
        }
        if (tzMin > tMin) tMin = tzMin;
        if (tzMax < tMax) tMax = tzMax;
        Debug.Log(string.Format("{0} tMin({1}) tMax({2})", (tMin<rayMax && tMax>0), tMin, tMax));
    }

    float Gamma(int n)
    {
        return (n * float.Epsilon) / (1 - n * float.Epsilon);
    }
}
