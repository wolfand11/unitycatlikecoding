using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class TestResolution : MonoBehaviour
{
    public Text txt_Info;
    public InputField if_Size;
    public Button btn_ChangeSize;
    public Button btn_Update;

    int width
    {
        get
        {
            int idx = Screen.resolutions.Length - 1;
            // Portrait
            //return Screen.resolutions[idx].width;
            // Landscape
            return Screen.resolutions[idx].height; 
        }
    }

    int height
    {
        get
        {
            int idx = Screen.resolutions.Length - 1;
            // Portrait
            //return Screen.resolutions[idx].height;
            // Landscape
            return Screen.resolutions[idx].width;
        }
    }


    private void Awake()
    {
#if UNITY_EDITOR
        txt_Info.text = string.Format("mw{0}, mh{1}, cw{2}, ch{3}", 
            width, height,
            Screen.currentResolution.width, Screen.currentResolution.height);
#else
        txt_Info.text = string.Format("mw{0}, mh{1}, cw{2}, ch{3}", 
            width, height,
            Screen.currentResolution.width, Screen.currentResolution.height);
#endif
        btn_ChangeSize.onClick.AddListener(OnClickChangeSize);
        btn_Update.onClick.AddListener(OnClickUpdateInfo);
    }

    int newHeight
    {
        get
        {
            if(string.IsNullOrEmpty(if_Size.text))
            {
                return 480;
            }
            return int.Parse(if_Size.text);
        }
    }

    float ratio
    {
        get
        {
            return (float)newHeight / height;
        }
    }

    void OnClickChangeSize()
    {
        int newWidth = (int)(width * ratio);
        if (newHeight > 0 && height >= newHeight)
        {
            Screen.SetResolution(newWidth, newHeight, FullScreenMode.FullScreenWindow);
            OnClickUpdateInfo();
        }
        else
        {
            txt_Info.text = string.Format("ERROR: w{0} h{1} newW{2} newH{3}", width, height, newWidth, newHeight);
        }
    }

    void OnClickUpdateInfo()
    {
        txt_Info.text = string.Format("mw{0}, mh{1}, cw{2}, ch{3}, r{4:0.00}", 
            width, height, 
            Screen.currentResolution.width, Screen.currentResolution.height,
            ratio);
    }
}
