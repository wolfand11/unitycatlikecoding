using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct LightControlSetting
{
    public Color color;
    public float intensity;

    public static LightControlSetting Default()
    {
        return new LightControlSetting()
        {
            color = Color.white,
            intensity = 1f            
        };
    }
}