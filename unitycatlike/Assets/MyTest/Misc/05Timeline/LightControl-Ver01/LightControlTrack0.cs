using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;

[TrackClipType(typeof(LightControlAsset0))]   // 指定track接受的clip type
[TrackBindingType(typeof(Light))]            // 指定 track 绑定的对象，其可以为一个GameObject，或一个Component，或一个Asset
public class LightControlTrack0 : TrackAsset {}