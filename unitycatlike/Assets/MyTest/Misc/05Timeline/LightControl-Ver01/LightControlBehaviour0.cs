using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class LightControlBehaviour0 : PlayableBehaviour
{
    //public Light light = null;    // 不需要指定light component
    public LightControlSetting setting = LightControlSetting.Default();

    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        Light light = playerData as Light;

        if (light != null)
        {
            light.color = setting.color;
            light.intensity = setting.intensity;
        }
    }
}