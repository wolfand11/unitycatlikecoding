using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class LightControlAsset0 : PlayableAsset
{
    //public ExposedReference<Light> light;  // 不需要指定 Light Component
    public LightControlSetting setting = LightControlSetting.Default();

    public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<LightControlBehaviour0>.Create(graph);

        var lightControlBehaviour = playable.GetBehaviour();
        //lightControlBehaviour.light = light.Resolve(graph.GetResolver());
        lightControlBehaviour.setting = setting;

        return playable;
    }
}