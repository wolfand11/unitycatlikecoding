using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class LightControlAsset2 : PlayableAsset
{
    public LightControlBehaviour2 template;

    public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<LightControlBehaviour2>.Create(graph, template);
        return playable;
    }
}