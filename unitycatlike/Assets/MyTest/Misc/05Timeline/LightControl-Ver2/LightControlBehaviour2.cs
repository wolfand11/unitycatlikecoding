using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

// TIPS: 使用 PlayableBehaviour template 需要添加 [System.Serializable]
[System.Serializable]
public class LightControlBehaviour2 : PlayableBehaviour
{
    public LightControlSetting setting = LightControlSetting.Default();
}