using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class LightControlMixerBehaviour1 : PlayableBehaviour
{
    // NOTE: This function is called at runtime and edit time.  Keep that in mind when setting the values of properties.
    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        Light trackBinding = playerData as Light;
        LightControlSetting finalSetting = LightControlSetting.Default();

        if (!trackBinding)
            return;

        int inputCount = playable.GetInputCount (); //get the number of all clips on this track

        for (int i = 0; i < inputCount; i++)
        {
            float inputWeight = playable.GetInputWeight(i);
            ScriptPlayable<LightControlBehaviour1> inputPlayable = (ScriptPlayable<LightControlBehaviour1>)playable.GetInput(i);
            LightControlBehaviour1 input = inputPlayable.GetBehaviour();

            // Use the above variables to process each frame of this playable.
            finalSetting.intensity += input.setting.intensity * inputWeight;
            finalSetting.color += input.setting.color * inputWeight;
        }

        //assign the result to the bound object
        trackBinding.intensity = finalSetting.intensity;
        trackBinding.color = finalSetting.color;
    }
}