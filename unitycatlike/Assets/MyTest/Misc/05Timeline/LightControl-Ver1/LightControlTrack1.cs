using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[TrackClipType(typeof(LightControlAsset1))] // 指定track接受的clip type
[TrackBindingType(typeof(Light))] // 指定 track 绑定的对象，其可以为一个GameObject，或一个Component，或一个Asset
public class LightControlTrack1 : TrackAsset
{
     public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount) 
     {
         return ScriptPlayable<LightControlMixerBehaviour1>.Create(graph, inputCount);
     }   
}