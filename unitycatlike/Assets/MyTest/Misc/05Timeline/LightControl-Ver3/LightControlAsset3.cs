using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

[System.Serializable]
public class LightControlAsset3 : PlayableAsset
{
    public LightControlBehaviour3 template;

    public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<LightControlBehaviour3>.Create(graph, template);
        return playable;
    }
}