using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct LightControlSetting3
{
    public Color color;
    public float intensity;
    public ExposedReference<Light> light;
    public static LightControlSetting3 Default()
    {
        return new LightControlSetting3()
        {
            color = Color.white,
            intensity = 1f,
            light = new ExposedReference<Light>(),
        };
    }
}