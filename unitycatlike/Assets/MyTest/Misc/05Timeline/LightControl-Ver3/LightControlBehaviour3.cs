using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

// TIPS: 使用 PlayableBehaviour template 需要添加 [System.Serializable]
[System.Serializable]
public class LightControlBehaviour3 : PlayableBehaviour
{
    public LightControlSetting3 setting = LightControlSetting3.Default();
    
    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        var graph = playable.GetGraph();
        var light = setting.light.Resolve(graph.GetResolver());
        if (light != null)
        {
            light.color = setting.color;
            light.intensity = setting.intensity;
        }
    }
}