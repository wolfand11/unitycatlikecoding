using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
   
[Serializable]
public class VolumeClipBehaviour : PlayableBehaviour
{
    public LensDistortionParams paramsLensDistortion = new LensDistortionParams();
    public VignetteParams paramsVignette = new VignetteParams();
    public ChromaticAberrationParams paramsChromaticAberration = new ChromaticAberrationParams();

    private Volume _volume;
    
    public override void OnPlayableCreate(Playable playable)
    {
        var graph = playable.GetGraph();
        //var light = setting.light.Resolve(graph.GetResolver());
        //var volume = graph.GetResolver().GetManager<VolumeManager>();        
    }

    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        _volume = playerData as Volume;
        if (_volume == null) return;

        var progress = (float)(playable.GetTime() / playable.GetDuration());

        LensDistortion lensDistortion;
        if (!_volume.sharedProfile.TryGet<LensDistortion>(out lensDistortion))
        {
            lensDistortion = _volume.sharedProfile.Add<LensDistortion>();
        }

        lensDistortion.active = paramsLensDistortion.Enabled;
        lensDistortion.intensity.Override(paramsLensDistortion.Intensity.Evaluate(progress));
        lensDistortion.xMultiplier.Override(paramsLensDistortion.XMultiplier.Evaluate(progress));
        lensDistortion.yMultiplier.Override(paramsLensDistortion.YMultiplier.Evaluate(progress));
        lensDistortion.scale.Override(paramsLensDistortion.Scale.Evaluate(progress));

        Vignette vignette;
        if (!_volume.sharedProfile.TryGet<Vignette>(out vignette))
        {
            vignette = _volume.sharedProfile.Add<Vignette>();
        }
        vignette.active = paramsVignette.Enabled;
        vignette.color.Override(paramsVignette.Color.Evaluate(progress));
        vignette.center.Override(new Vector2(paramsVignette.CenterX.Evaluate(progress),  paramsVignette.CenterY.Evaluate(progress)));
        vignette.intensity.Override(paramsVignette.Intensity.Evaluate(progress));
        //vignette.intensity.value = paramsVignette.Intensity.Evaluate(progress);
        vignette.smoothness.Override(paramsVignette.Smoothness.Evaluate(progress));

        ChromaticAberration chromaticAberration;
        if (!_volume.sharedProfile.TryGet<ChromaticAberration>(out chromaticAberration))
        {
            chromaticAberration = _volume.sharedProfile.Add<ChromaticAberration>();
        }
        chromaticAberration.active = paramsChromaticAberration.Enabled;
        
        //chromaticAberration.intensity.Override(paramsChromaticAberration.Intensity);
        chromaticAberration.intensity.Override(paramsChromaticAberration.Intensity.Evaluate(progress));
    }
}
