using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

[System.Serializable]
public class VolumeClipAsset : PlayableAsset
{
    public VolumeClipBehaviour template;
    
    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<VolumeClipBehaviour>.Create(graph, template);
        return playable;
    }
}
