using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Timeline;

[TrackClipType(typeof(VolumeClipAsset))]
[TrackBindingType(typeof(Volume))]
public class VolumeTrackAsset : TrackAsset { }
