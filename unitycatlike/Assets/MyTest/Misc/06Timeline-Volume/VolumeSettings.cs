using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class LensDistortionParams
{
    public bool Enabled;
    public AnimationCurve Intensity;
    public AnimationCurve XMultiplier;
    public AnimationCurve YMultiplier;
    public AnimationCurve Scale;

    public static LensDistortionParams Default()
    {
        var param = new LensDistortionParams()
        {
            Enabled = false,
            Intensity = new AnimationCurve(),
            XMultiplier = new AnimationCurve(),
            YMultiplier = new AnimationCurve(),
            Scale = new AnimationCurve()
        };
        return param;
    }
}

[Serializable]
public class VignetteParams
{
    public bool Enabled;
    public Gradient Color;
    public AnimationCurve CenterX;
    public AnimationCurve CenterY;
    public AnimationCurve Intensity;
    public AnimationCurve Smoothness;

    public static VignetteParams Default()
    {
        var param = new VignetteParams()
        {
            Enabled = false,
            Color = new Gradient(),
            CenterX = AnimationCurve.Constant(0, 1, 0.5f),
            CenterY = AnimationCurve.Constant(0, 1, 0.5f),
            Intensity = AnimationCurve.Constant(0, 1, 1.0f),
            Smoothness = AnimationCurve.Constant(0, 1, 0.0f),
        };
        return param;
    }
}

[Serializable]
public struct ChromaticAberrationParams
{
    public bool Enabled;
    //public float Intensity;    
    public AnimationCurve Intensity;

    public static ChromaticAberrationParams Default()
    {
        var param = new ChromaticAberrationParams()
        {
            Enabled = false,
            //Intensity = 0.0f
            Intensity = AnimationCurve.Constant(0, 1, 1.0f),
        };
        return param;
    }
}