﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer), typeof(MeshFilter))]
public class DrawTriangle : MonoBehaviour
{
    public Material mat;
    MeshFilter mf;
    MeshRenderer mr;
    void Start()
    {
        mf = GetComponent<MeshFilter>();
        if (mf == null) return;
        mr = GetComponent<MeshRenderer>();
        if (mr == null) return;

        Mesh mesh = new Mesh();
        Vector3[] vertices = new Vector3[3];
        vertices[0] = new Vector3( 0,  1, 0);
        vertices[1] = new Vector3( 1, -1, 0);
        vertices[2] = new Vector3(-1, -1, 0);
        
        Vector2[] uvs = new Vector2[3];
        uvs[0] = new Vector2(0, 1);
        uvs[1] = new Vector2(1, 0);
        uvs[2] = new Vector2(0, 0);

        int[] tris = new int[3];
        tris[0] = 0;
        tris[1] = 1;
        tris[2] = 2;

        mesh.vertices = vertices;
        mesh.triangles = tris;
        mesh.uv = uvs;
        mf.mesh = mesh;
        mr.sharedMaterial = mat;
    }
}
