﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class PreviewShaderInputMatrix : MonoBehaviour
{
    public Vector3 forward;
    public Matrix4x4 obj2WorldMat;
    public Matrix4x4 world2ObjMat;
    public Matrix4x4 viewMat;
    public Matrix4x4 projMat;
    public Matrix4x4 invertProjMat;
    public List<string> childWPos;
    public List<string> childViewPos;
    public List<string> childProjPos;
    public List<string> childNDCPos;
    public List<string> childScreenPos;

    public bool update = false;
    Camera currentCamera
    {
        get
        {
            var cam = GetComponent<Camera>();
            if(cam == null)
            {
                return Camera.current;
            }
            else
            {
                return cam;
            }
        }
    }

    void Update()
    {
        if(update)
        {
            forward = transform.forward;
            obj2WorldMat = transform.localToWorldMatrix;
            world2ObjMat = transform.worldToLocalMatrix;
            viewMat = currentCamera.worldToCameraMatrix;
            projMat = currentCamera.projectionMatrix;
            invertProjMat = currentCamera.projectionMatrix.inverse;
            childWPos.Clear();
            childViewPos.Clear();
            childProjPos.Clear();
            childNDCPos.Clear();
            childScreenPos.Clear();

            var childs = transform.GetComponentsInChildren<Transform>();
            foreach(var child in childs)
            {
                var childInfo = string.Format("{0} {1:F2} {2:F2} {3:F2}", child.name, child.position.x, child.position.y, child.position.z);
                childWPos.Add(childInfo);

                Vector4 tmpPos = child.position;
                tmpPos.w = 1;
                var viewPos = viewMat * tmpPos;
                var childViewInfo = string.Format("{0} {1:F2} {2:F2} {3:F2} {4:F2}", child.name, viewPos.x, viewPos.y, viewPos.z, viewPos.w);
                childViewPos.Add(childViewInfo);

                var projPos = projMat * viewPos;
                var childProjInfo = string.Format("{0} {1:F2} {2:F2} {3:F2} {4:F2}", child.name, projPos.x, projPos.y, projPos.z, projPos.w);
                childProjPos.Add(childProjInfo);

                var childNDCInfo = string.Format("{0} {1:F2} {2:F2} {3:F2}", child.name, projPos.x/projPos.w, projPos.y/projPos.w, projPos.z/projPos.w);
                childNDCPos.Add(childNDCInfo);

                var screenPos = currentCamera.WorldToScreenPoint(child.position);
                var childScreenInfo = string.Format("{0} {1:F2} {2:F2}", child.name, screenPos.x, screenPos.y);
                childScreenPos.Add(childScreenInfo);
            }

            update = false;
        }
    }
}
