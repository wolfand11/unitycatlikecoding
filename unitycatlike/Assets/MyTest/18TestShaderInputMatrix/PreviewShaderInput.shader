﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Unlit/PreviewShaderInput"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_LightDir ("Light Dir", Vector) = (0,0,1)
		_Shininess("Shininess", Float) = 10
		[Enum(UnityEngine.Rendering.CullMode)]_Cull("Cull Mode", Float) = 1
		[Enum(Off, 0, On, 1)]_ZWrite("Z Write", Float) = 1
		[Enum(UnityEngine.Rendering.CompareFunction)]_ZTest("Z Test", Float) = 1
		[Toggle(ENABLE_NOL)] EnableNoL("Enable NoL", Float) = 0
		[Toggle(ENABLE_VFACE)] EnableVFace("Enable VFace", Float) = 0
		[Toggle(ENABLE_MYLIGHT)] EnableMyLight("Enable My Light", Float) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
			ZWrite [_ZWrite]
			ZTest [_ZTest]
			Cull [_Cull]
            CGPROGRAM
			#pragma target 3.0
            #pragma vertex vert
            #pragma fragment frag
			#pragma multi_compile _ ENABLE_NOL
			#pragma multi_compile _ ENABLE_VFACE
			#pragma multi_compile _ ENABLE_MYLIGHT

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
				float4 wPos : TEXCOORD1;
				float4 vPos : TEXCOORD2;
				float4 pPos : TEXCOORD3;
				float3 wNormal : TEXCOORD4;
#if SHADER_STAGE_FRAGMENT
				fixed face : VFACE;
#endif
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float3 _LightDir;
			float _Shininess;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);

				o.wPos = mul(unity_ObjectToWorld, v.vertex);
				o.vPos = mul(UNITY_MATRIX_V, o.wPos);
				o.pPos = mul(UNITY_MATRIX_P, o.vPos);
				o.wNormal = mul(v.normal, (float3x3)unity_WorldToObject);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
				float3 viewDir = normalize(_WorldSpaceCameraPos.xyz - i.wPos);
#if defined(ENABLE_MYLIGHT)
				float3 lightDir = _LightDir.xyz;
#else
				float3 lightDir = _WorldSpaceLightPos0.xyz;
#endif
				float NoL = dot(lightDir, i.wNormal);
				float3 H = normalize((viewDir + lightDir));
				float HoN = dot(H, normalize(i.wNormal));
				float specFactor = pow(HoN, _Shininess);

#if SHADER_STAGE_FRAGMENT && ENABLE_VFACE
				if (i.face > 0)
				{
					col *= float4(1, 1, 0, 1);
				}
				else
				{
					col *= float4(0, 0, 1, 1);
				}
#endif
#if defined(ENABLE_NOL)
				col = col * NoL + col * specFactor;
#endif
				col.a = (i.wPos + i.vPos + i.pPos).x;
                return col;
            }
            ENDCG
        }
    }
}
