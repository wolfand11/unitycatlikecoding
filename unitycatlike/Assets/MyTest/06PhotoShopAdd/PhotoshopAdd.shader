﻿Shader "Custom/MyTest/PhotoshopAdd"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _MainTexB ("Texture", 2D) = "white" {}
		[KeywordEnum(None, Opacity, HardLight, HardLightEx)] _AddMode("Add Mode", Float) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
			Blend SrcAlpha OneMinusSrcAlpha
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			#pragma multi_compile _  _ADDMODE_OPACITY _ADDMODE_HARDLIGHT _ADDMODE_HARDLIGHTEX
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            sampler2D _MainTexB;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				// https://en.wikipedia.org/wiki/Blend_modes

                fixed4 col = tex2D(_MainTex, i.uv);
                fixed4 colB = tex2D(_MainTexB, i.uv);
				fixed4 result = 0;
				#if defined(_ADDMODE_HARDLIGHT)
					col = pow(col, 1.0 / 2.2);
					colB = pow(colB, 1.0/2.2);
					result = colB < 0.5 ? col * colB * 2 : 1 - (1 - col)*(1 - colB)*2;
					result = pow(result, 2.2);
					//result = lerp(col*(1-colB.a), result, colB.a);
					//result = lerp(col, result, colB.a);
				#elif defined(_ADDMODE_HARDLIGHTEX)
					result.r = colB.r < 0.5 ? col.r * colB.r * 2 : 1 - (1 - col.r)*(1 - colB.r)*2;
					result.g = colB.g < 0.5 ? col.g * colB.g * 2 : 1 - (1 - col.g)*(1 - colB.g)*2;
					result.b = colB.b < 0.5 ? col.b * colB.b * 2 : 1 - (1 - col.b)*(1 - colB.b)*2;
					result.a = 1;
				#elif defined(_ADDMODE_OPACITY)
					result = colB*colB.a+(1-colB.a)*col;
				#else
					result = col;
				#endif
                return result;
            }
            ENDCG
        }
    }
}
