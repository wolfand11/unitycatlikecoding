﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class TestShearing : MonoBehaviour
{
#if UNITY_EDITOR
    Vector3[] vertexArr = new Vector3[8]
    {
        Vector3.zero,
        new Vector3(1,0,0),
        new Vector3(1,0,1),
        new Vector3(0,0,1),
        new Vector3(0,1,0),
        new Vector3(1,1,0),
        Vector3.one,
        new Vector3(0,1,1),
    };
    Vector3[] tmpVertexArr = new Vector3[8];

    public float s = 1.5f;
    public float t = 1.5f;
    public Color color = Color.red;
    public KeepAxis keepAxis = KeepAxis.Z;
    public bool doShearing = true;

    public enum KeepAxis
    {
        Invalid,X,Y,Z
    }

    Matrix4x4 matrix = Matrix4x4.identity;


    void Update()
    {
        if(doShearing)
        {
            if(KeepAxis.Invalid!=keepAxis)
            {
                UpdateMatrix();
                for(var i=0; i<8; i++)
                {
                    tmpVertexArr[i] = matrix.MultiplyPoint(vertexArr[i]);
                }
            }
            doShearing = false;
        }
    }

    void UpdateMatrix()
    {
        if(keepAxis == KeepAxis.X)
        {
            matrix = new Matrix4x4(
                new Vector4(1, s, t, 0),
                new Vector4(0, 1, 0, 0),
                new Vector4(0, 0, 1, 0),
                new Vector4(0, 0, 0, 1)
                );
        }
        else if(keepAxis == KeepAxis.Y)
        {
            matrix = new Matrix4x4(
                new Vector4(1, 0, 0, 0),
                new Vector4(s, 1, t, 0),
                new Vector4(0, 0, 1, 0),
                new Vector4(0, 0, 0, 1)
                );

        }
        else
        {
            matrix = new Matrix4x4(
                new Vector4(1, 0, 0, 0),
                new Vector4(0, 1, 0, 0),
                new Vector4(s, t, 1, 0),
                new Vector4(0, 0, 0, 1)
                );

        }
    }

    void OnDrawGizmos()
    {
        Color gizmosOldColor = Gizmos.color;

        Gizmos.color = color;
        Gizmos.DrawLine(tmpVertexArr[0], tmpVertexArr[1]);
        Gizmos.DrawLine(tmpVertexArr[1], tmpVertexArr[2]);
        Gizmos.DrawLine(tmpVertexArr[2], tmpVertexArr[3]);
        Gizmos.DrawLine(tmpVertexArr[3], tmpVertexArr[0]);

        Gizmos.DrawLine(tmpVertexArr[4], tmpVertexArr[5]);
        Gizmos.DrawLine(tmpVertexArr[5], tmpVertexArr[6]);
        Gizmos.DrawLine(tmpVertexArr[6], tmpVertexArr[7]);
        Gizmos.DrawLine(tmpVertexArr[7], tmpVertexArr[4]);

        Gizmos.DrawLine(tmpVertexArr[0], tmpVertexArr[4]);
        Gizmos.DrawLine(tmpVertexArr[1], tmpVertexArr[5]);
        Gizmos.DrawLine(tmpVertexArr[2], tmpVertexArr[6]);
        Gizmos.DrawLine(tmpVertexArr[3], tmpVertexArr[7]);
            
        Gizmos.color = gizmosOldColor;
    }
#endif
}
