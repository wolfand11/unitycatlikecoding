﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteInEditMode]
public class GBilinearFilteringTest : MonoBehaviour
{
    public int srcImageSize = 10;
    public int desImageSize = 512;
    public bool doTest = false;
    public bool reGenSrcImage = false;
#if UNITY_EDITOR
    void Update()
    {
        if(doTest)
        {
            TestBilinearInterpolation();

            reGenSrcImage = false;
            doTest = false;
        }
    }

    void TestBilinearInterpolation()
    {
        string srcImgPath = "Assets/MyTest/04BilinearFiltering/srcImage.png";
        string desImgPath = "Assets/MyTest/04BilinearFiltering/desImage.png";

        var importer = AssetImporter.GetAtPath(srcImgPath) as TextureImporter;
        Texture2D srcImage = null;
        Color[] srcImageColorArr = null;
        if (importer == null || reGenSrcImage)
        {
            srcImage = new Texture2D(srcImageSize, srcImageSize);
            srcImageColorArr = new Color[srcImageSize * srcImageSize];
            for (var i = 0; i < srcImageSize * srcImageSize; i++)
            {
                srcImageColorArr[i] = Random.ColorHSV();
            }
            srcImage.SetPixels(srcImageColorArr);
            System.IO.File.WriteAllBytes(srcImgPath, srcImage.EncodeToPNG());
        }
        else
        {
            importer.isReadable = true;
            importer.mipmapEnabled = false;
            importer.textureType = TextureImporterType.Sprite;
            importer.filterMode = FilterMode.Point;
            importer.SaveAndReimport();

            srcImage = AssetDatabase.LoadAssetAtPath<Texture2D>(srcImgPath);
            srcImageColorArr = srcImage.GetPixels();
        }

        float maxUV = (srcImageSize - 1.0f) / srcImageSize;
        Texture2D desImage = new Texture2D(desImageSize, desImageSize);
        Color[] desImageColorArr = new Color[desImageSize*desImageSize];
        for(var i=0; i<desImageSize; i++)
        {
            for(var j=0; j<desImageSize; j++)
            {
                var desImageColorIdx = i * desImageSize + j;
                float tu = (float)j / desImageSize * maxUV;
                float tv = (float)i / desImageSize * maxUV;

                int tx = (int)(tu * srcImageSize);
                int ty = (int)(tv * srcImageSize);
                int txPlus1 = tx + 1;
                txPlus1 = Mathf.Min(txPlus1, srcImageSize-1);
                int tyPlus1 = ty + 1;
                tyPlus1 = Mathf.Min(tyPlus1, srcImageSize-1);
                var c00Idx = ty * srcImageSize + tx;
                var c10Idx = ty * srcImageSize + txPlus1;
                var c01Idx = tyPlus1 * srcImageSize + tx;
                var c11Idx = tyPlus1 * srcImageSize + txPlus1;
                
                Color c00 = srcImageColorArr[c00Idx];
                Color c10 = srcImageColorArr[c10Idx];
                Color c01 = srcImageColorArr[c01Idx];
                Color c11 = srcImageColorArr[c11Idx];

                float dx = srcImageSize * tu - tx;
                float dy = srcImageSize * tv - ty;

                desImageColorArr[desImageColorIdx] = BilinearFilter(c00, c10, c01, c11, dx, dy);
            }
        }
        desImage.SetPixels(desImageColorArr);
        System.IO.File.WriteAllBytes(desImgPath, desImage.EncodeToPNG());
    }

    Color BilinearFilter(Color c00, Color c10, Color c01, Color c11, float tx, float ty)
    {
#if false
        Color a = c00 * (1 - tx) + c10 * tx;
        Color b = c10 * (1 - tx) + c11 * tx;
        return (a * (1 - ty) + b * ty);
#else
        return 
            (1 - tx) * (1 - ty) * c00 +
            tx * (1 - ty) * c10 +
            (1 - tx) * ty * c01 +
            tx * ty * c11;
#endif
    }


#endif
}
