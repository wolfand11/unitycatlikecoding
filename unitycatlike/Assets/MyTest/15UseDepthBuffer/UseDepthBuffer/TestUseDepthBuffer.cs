﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

[RequireComponent(typeof(Camera))]
[ImageEffectOpaque]
[ExecuteInEditMode]
public class TestUseDepthBuffer : MonoBehaviour
{
    Shader bufferCopyShader;
    Shader useBufferShader;
    Material _mat;
    Material _useBufferMat;
    Camera _camera;
    RenderTexture _rt;
    CommandBuffer _cb;
    void OnEnable()
    {
        Init();
    }

    void Init()
    {
        if (bufferCopyShader == null)
        {
            bufferCopyShader = Shader.Find("Test/TestUseDepthBuffer_BlitColorAndDepth");
        }
        if (bufferCopyShader == null) return;
        if(useBufferShader == null)
        {
            useBufferShader = Shader.Find("Test/TestUseDepthBuffer");
        }
        if (useBufferShader == null) return;

        if(_mat==null)
        {
            _mat = new Material(bufferCopyShader);
        }
        if (_mat == null) return;
        if(_useBufferMat==null)
        {
            _useBufferMat = new Material(useBufferShader);
        }
        if (_useBufferMat == null) return;

        _camera = GetComponent<Camera>();
        if (_camera == null) return;
        _camera.forceIntoRenderTexture = true;

        if(_rt == null)
        {
            _rt = RenderTexture.GetTemporary(_camera.pixelWidth, _camera.pixelHeight, 24, RenderTextureFormat.BGRA32, RenderTextureReadWrite.Linear);
        }
        if(_rt==null) return;

        if(_cb == null)
        {
            _cb = new CommandBuffer();
            _cb.name = "Test Use Depth Buffer";
            _cb.BeginSample("Blit RT to FrameBuffer");
            _cb.Blit(BuiltinRenderTextureType.CameraTarget, _rt.colorBuffer, _mat, 0);
            _cb.Blit(BuiltinRenderTextureType.Depth, _rt.depth, _mat, 1);
            _cb.EndSample("Blit RT to FrameBuffer");

            _cb.BeginSample("Use RT");
            _cb.SetGlobalTexture("_ColorTex", _rt.colorBuffer);
            _cb.SetGlobalTexture("_DepthTex", _rt.depth);
            _cb.DrawProcedural(Matrix4x4.identity, _useBufferMat, 0, MeshTopology.Triangles, 3);
            _cb.EndSample("Use RT");
            //_camera.AddCommandBuffer(CameraEvent.AfterForwardOpaque, _cb);
            //_camera.AddCommandBuffer(CameraEvent.AfterSkybox, _cb);
            _camera.AddCommandBuffer(CameraEvent.AfterEverything, _cb);
        }
    }

    void OnDisable()
    {
        if(_rt!=null)
        {
            RenderTexture.ReleaseTemporary(_rt);
        }
        if(_cb!=null)
        {
            //_camera.RemoveCommandBuffer(CameraEvent.AfterForwardOpaque, _cb);
            //_camera.RemoveCommandBuffer(CameraEvent.AfterSkybox, _cb);
            _camera.RemoveCommandBuffer(CameraEvent.AfterEverything, _cb);
        }
        _rt = null;
    }

    void OnPreRender()
    {
    }

    void OnPostRender()
    {
    }
}
