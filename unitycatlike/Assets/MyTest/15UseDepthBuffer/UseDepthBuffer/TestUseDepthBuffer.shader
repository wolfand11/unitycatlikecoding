﻿Shader "Test/TestUseDepthBuffer"
{
    Properties
    {
        _ColorTex ("Texture", 2D) = "white" {}
		_DepthTex("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _ColorTex;
            sampler2D _DepthTex;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_ColorTex, i.uv);
				float depth = SAMPLE_DEPTH_TEXTURE(_DepthTex, i.uv);
				col += depth;
                return col;
            }
            ENDCG
        }
    }
}
