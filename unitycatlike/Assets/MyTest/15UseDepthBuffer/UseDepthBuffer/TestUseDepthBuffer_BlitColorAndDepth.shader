﻿Shader "Test/TestUseDepthBuffer_BlitColorAndDepth"
{
	CGINCLUDE
		struct appdata
		{
			float4 vertex : POSITION;
			float2 uv : TEXCOORD0;
		};

		struct v2f
		{
			float2 uv : TEXCOORD0;
			float4 vertex : SV_POSITION;
		};

		sampler2D _MainTex;

		v2f vert (appdata v)
		{
			v2f o;
			o.vertex = float4(v.vertex.xy, 0, 1);
			o.uv = (v.vertex + 1) * 0.5;
			#if UNITY_UV_STARTS_AT_TOP
				o.uv= o.uv * float2(1.0, -1.0) + float2(0.0, 1.0);
			#endif
			return o;
		}
	ENDCG
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
			ZTest Off
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);
                return col;
            }
            ENDCG
        }
		Pass
		{
			ZTest Off
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            void frag (v2f i, out float depth:SV_DEPTH)
            {
				depth = SAMPLE_DEPTH_TEXTURE(_MainTex, i.uv);
            }
            ENDCG

		}
    }
}
