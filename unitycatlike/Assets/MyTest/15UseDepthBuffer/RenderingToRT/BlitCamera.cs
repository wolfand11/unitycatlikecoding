﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

[RequireComponent(typeof(Camera))]
[ExecuteInEditMode]
public class BlitCamera : MonoBehaviour
{
    Camera _camera;
    RenderTexture _rtColorBuffer;
    RenderTexture _rtDepthBuffer;
    RenderTexture _rtColorBuffer2;
    RenderTexture _rtDepthBuffer2;
    GameObject _blitCamGObj;
    Camera _blitCam;
    CommandBuffer _cbBlitTo2;
    CommandBuffer _cb;
    Shader _blitShader;
    Material _blitMat;
    Material _blitTo2Mat;
    void Awake()
    {
        Debug.Log("BlitCamera Awake!");
    }

    void OpenPipeline()
    {
        _camera = GetComponent<Camera>();
        _camera.targetTexture = null;
        _camera.forceIntoRenderTexture = false;
        _camera.clearFlags = CameraClearFlags.Nothing;
        _camera.RemoveAllCommandBuffers();

        _rtColorBuffer = RenderTexture.GetTemporary(_camera.pixelWidth, _camera.pixelHeight, 0, RenderTextureFormat.Default, RenderTextureReadWrite.Linear);
        _rtColorBuffer.name = "Custom Cam ColorBuffer";
        _rtDepthBuffer = RenderTexture.GetTemporary(_camera.pixelWidth, _camera.pixelHeight, 24, RenderTextureFormat.Depth);
        _rtDepthBuffer.name = "Custom Cam DepthBuffer";
        _rtColorBuffer2 = RenderTexture.GetTemporary(_camera.pixelWidth, _camera.pixelHeight, 0, RenderTextureFormat.Default, RenderTextureReadWrite.Linear);
        _rtColorBuffer2.name = "Custom Cam ColorBuffer 2";
        _rtDepthBuffer2 = RenderTexture.GetTemporary(_camera.pixelWidth, _camera.pixelHeight, 24, RenderTextureFormat.Depth);
        _rtDepthBuffer2.name = "Custom Cam DepthBuffer 2";

        var tmpBlitCamTrans = transform.Find("blit cam");
        if(tmpBlitCamTrans==null)
        {
            _blitCamGObj = new GameObject("blit cam");
        }
        else
        {
            _blitCamGObj = tmpBlitCamTrans.gameObject;
        }
        _blitCamGObj.transform.SetParent(transform, false);
        _blitCamGObj.hideFlags = HideFlags.DontSave;
        _blitCam = _blitCamGObj.AddComponent<Camera>();
        // set blit cam property
        _blitCam.CopyFrom(_camera);
        _blitCam.clearFlags = CameraClearFlags.Skybox;
        _blitCam.depthTextureMode = DepthTextureMode.None;
        _blitCam.SetTargetBuffers(_rtColorBuffer.colorBuffer, _rtDepthBuffer.depthBuffer);
        _blitCam.cullingMask = ~0;
        _blitCam.enabled = false;

        _blitShader = Shader.Find("Test/BlitToCam_BlitColorAndDepth");
        //// build blitTo2 cmd buffer
        //_cbBlitTo2 = new CommandBuffer();
        //_cbBlitTo2.name = "BlitTo2RTCMD";
        //_cbBlitTo2.BeginSample("Blit To 2RT");
        //_blitTo2Mat = new Material(_blitShader);
        //_cbBlitTo2.SetGlobalTexture("_ColorTex", _rtColorBuffer);
        //_cbBlitTo2.SetGlobalTexture("_DepthTex", _rtDepthBuffer);
        //_cbBlitTo2.SetRenderTarget(
        //    _rtColorBuffer2, RenderBufferLoadAction.DontCare, RenderBufferStoreAction.Store, 
        //    _rtDepthBuffer2, RenderBufferLoadAction.DontCare, RenderBufferStoreAction.Store
        //);
        //_cbBlitTo2.DrawProcedural(Matrix4x4.identity, _blitTo2Mat, 0, MeshTopology.Triangles, 3);
        //_cbBlitTo2.EndSample("Blit To 2RT");
        //_blitCam.AddCommandBuffer(CameraEvent.AfterEverything, _cbBlitTo2);


        // build cmd buffer
        _cb = new CommandBuffer();
        _cb.name = "BlitToScreenCMD";
        _cb.BeginSample("Blit To Screen");
        _blitMat = new Material(_blitShader);
        //_cb.SetGlobalTexture("_ColorTex", _rtColorBuffer2);
        //_cb.SetGlobalTexture("_DepthTex", _rtDepthBuffer2);
        _cb.SetGlobalTexture("_ColorTex", _rtColorBuffer.colorBuffer);
        _cb.SetGlobalTexture("_DepthTex", _rtDepthBuffer.depthBuffer);
        _cb.DrawProcedural(Matrix4x4.identity, _blitMat, 0, MeshTopology.Triangles, 3);
        _cb.EndSample("Blit To Screen");

        _camera.AddCommandBuffer(CameraEvent.AfterEverything, _cb);
        //_camera.AddCommandBuffer(CameraEvent.AfterForwardOpaque, _cb);
    }

    void ClosePipeline()
    {
        if(_cbBlitTo2!=null)
        {
            _cbBlitTo2.Clear();
            if(_blitCam!=null)
            {
                _blitCam.RemoveCommandBuffer(CameraEvent.AfterEverything, _cbBlitTo2);
            }
        }

        if(_cb!=null)
        {
            _cb.Clear();
            if(_camera!=null)
            {
                _camera.RemoveCommandBuffer(CameraEvent.AfterEverything, _cb);
                //_camera.RemoveCommandBuffer(CameraEvent.AfterForwardOpaque, _cb);
            }
        }
        
        if(_rtColorBuffer!=null)
        {
            RenderTexture.ReleaseTemporary(_rtColorBuffer);
        }
        if(_rtDepthBuffer!=null)
        {
            RenderTexture.ReleaseTemporary(_rtDepthBuffer);
        }

        if(_blitCamGObj!=null)
        {
            GameObject.DestroyImmediate(_blitCamGObj);
        }

        _blitCamGObj = null;
        _blitCam = null;
        _rtColorBuffer = null;
        _rtDepthBuffer = null;
        _cb = null;
    }

    void OnEnable()
    {
        Debug.Log("BlitCamera OnEnable!");
        OpenPipeline();
    }

    void OnDisable()
    {
        Debug.Log("BlitCamera OnDisable!");
        ClosePipeline();
    }

    void OnDestroy()
    {
        Debug.Log("BlitCamera Destroy!");
    }

    void OnPreRender()
    {
        if(_blitCamGObj!=null)
        {
            _blitCamGObj.transform.position = transform.position;
            _blitCamGObj.transform.rotation = transform.rotation;
        }

        if(_blitCam!=null)
        {
            _blitCam.Render();
        }
    }

    void OnPostRender()
    {
    }
}
