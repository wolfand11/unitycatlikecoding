﻿Shader "Test/BlitToCam_BlitColorAndDepth"
{
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
			ZTest Off
			ZWrite On
			Cull Off
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
			struct appdata
			{
				uint vertexID : SV_VertexID;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _ColorTex;
			sampler2D _DepthTex;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = float4(
					v.vertexID <= 1 ? -1.0 : 3.0,
					v.vertexID == 1 ? 3.0 : -1.0,
					0.0,
					1.0
				);
				o.uv = float2(
					v.vertexID <= 1 ? 0.0 : 2.0,
					v.vertexID == 1 ? 2.0 : 0.0
				);

				if (_ProjectionParams.x < 0.0)
				{
					o.uv.y = 1.0 - o.uv.y;
				}
				return o;
			}

			/*
			fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_ColorTex, i.uv);
                return col;
            }
			*/

            fixed4 frag (v2f i, out float depth:SV_DEPTH) : SV_Target
            {
                fixed4 col = tex2D(_ColorTex, i.uv);
				depth = SAMPLE_DEPTH_TEXTURE(_DepthTex, i.uv);
                return col;
            }

			/*
			void frag (v2f i, out float depth:SV_DEPTH)
            {
				depth = SAMPLE_DEPTH_TEXTURE(_DepthTex, i.uv);
            }
			*/
            ENDCG
        }
    }
}
