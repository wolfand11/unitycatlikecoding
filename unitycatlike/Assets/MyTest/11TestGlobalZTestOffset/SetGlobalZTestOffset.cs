﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class SetGlobalZTestOffset : MonoBehaviour
{
    public CompareFunction compareFunction;
    public float offsetFactor = 0;
    public float offsetUnits = 0;
    public bool doSet = false;


    void Update()
    {
        if(doSet)
        {
            Shader.SetGlobalInt("MZTest", (int)compareFunction);
            Shader.SetGlobalFloat("MOffsetFactor", offsetFactor);
            Shader.SetGlobalFloat("MOffsetUnits", offsetUnits);

            doSet = false;
        }
    }
}
