﻿Shader "Unlit/TestUIImageMat"
{
    Properties
    {
		_MainTex("Main Texture", 2D) = "white" {}
		_StartColor("Start Color", Color) = (0,0,0,0)
		_EndColor("End Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };
			Texture2D _MainTex;
			float4 _StartColor;
			float4 _EndColor;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				fixed4 color = 1;
				fixed3 hsv = RgbToHsv(_EndColor);
				hsv.y = i.uv.x;
				hsv.z = pow(i.uv.y,2.2);
				color.rgb = HsvToRgb(hsv);
				return color;
            }
            ENDCG
        }
    }
}
