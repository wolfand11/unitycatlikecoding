﻿Shader "Unlit/TestShaderMatrix"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
	    [Enum(NotDefineM,0,DefineM,1,DefineMWithVect,2)] DefineMatrix("DefineMatrix", Float) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
				float3 wNormal : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float DefineMatrix;

            v2f vert (appdata v)
            {
                v2f o;
				float4 translatedVertex = v.vertex + 0.5;
				if (DefineMatrix != 0)
				{
					float4x4 translateM = float4x4(
						1, 0, 0, 0,
						0, 1, 0, 0,
						0, 0, 1, 0,
						0, 0, 0, 1
					);
					if (DefineMatrix == 1)
					{
						// unity shader中向量为列向量
						// 矩阵每一列表示变换后新坐标系的基坐标轴
						// xAxis = （2,0,0)
						// yAxis = （1,1,0)
						// zAxis = （0,0,2)
						// 直接初始化矩阵
						translateM = float4x4(
							2, 0, 0, 0,
							1, 1, 0, 0,
							0, 0, 2, 0,
							0, 0, 0, 1
						);
					}
					else if (DefineMatrix == 2)
					{
						// 使用向量初始化矩阵
						float4 row0 = float4(2, 0, 0, 0);
						float4 row1 = float4(1, 1, 0, 0);
						float4 row2 = float4(0, 0, 2, 0);
						float4 row3 = float4(0, 0, 0, 1);
						translateM = float4x4(row0,row1,row2,row3);
					}
					translatedVertex = mul(translateM, translatedVertex);
				}
				o.wNormal = mul(v.normal, (float3x3)unity_WorldToObject);
                o.vertex = UnityObjectToClipPos(translatedVertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = 1;
				float3x3 colM = float3x3(
					1, 1, 0,
					0, 1, 0,
					0, 0, 1
				);
				// colM[0]取出的数据是矩阵第0行的数据
				// colM[2]取出的数据是矩阵第2行的数据
				col.rgb = colM[0];
				float3 lightDir = _WorldSpaceLightPos0.xyz;
                return col*dot(lightDir, i.wNormal);
				//return col;
            }
            ENDCG
        }
    }
}
