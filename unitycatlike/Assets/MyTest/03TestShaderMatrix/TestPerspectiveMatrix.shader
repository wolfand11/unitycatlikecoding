﻿Shader "Unlit/TestPerspectiveMatrix"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        [Enum(NotDefineM,0,DefineM,1,DefineMWithVect,2)] DefineMatrix("DefineMatrix", Float) = 0
    }
    SubShader
    {
        Tags
        {
            "RenderType"="Opaque"
        }

        Pass
        {
            Name "ForwardLit"
            Tags
            {
                "LightMode" = "UniversalForward"
            }

            Blend One Zero
            ZWrite On
            Cull Off

            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Input.hlsl"
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/SpaceTransforms.hlsl"

            struct appdata
            {
                float4 positionOS : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 positionCS : SV_POSITION;
                float3 positionWS : TEXCOORD1;
            };

            CBUFFER_START(UnityPerMaterial)
                float4 _MainTex_ST;
            CBUFFER_END

            TEXTURE2D(_MainTex);
            SAMPLER(sampler_MainTex);
            float4x4 _WorldToLocalMat;
            float4x4 _WorldToCamMat;
            float4x4 _CustomProjMat;
            float4x4 _UnityCamProjMat;
            float4x4 _GPUProjMatTrue;
            float4x4 _GPUProjMatFalse;

            v2f vert(appdata v)
            {
                v2f o;
                o.positionWS = TransformObjectToWorld(v.positionOS);
                o.positionCS = TransformWorldToHClip(o.positionWS);

                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            half4 frag(v2f i) : SV_Target
            {
                float4x4 tmpMat = mul(_WorldToLocalMat, _WorldToCamMat);
                tmpMat = mul(tmpMat, _CustomProjMat);
                tmpMat = mul(tmpMat, _UnityCamProjMat);
                tmpMat = mul(tmpMat, _GPUProjMatTrue);
                tmpMat = mul(tmpMat, _GPUProjMatFalse);
                tmpMat = mul(tmpMat, UNITY_MATRIX_P);
                tmpMat = mul(tmpMat, unity_CameraProjection);
                tmpMat = mul(tmpMat, UNITY_MATRIX_V);
                half4 col = mul(tmpMat, i.uv.xyxy);
                col.r = 1;
                return col;
            }
            ENDHLSL
        }
    }
}