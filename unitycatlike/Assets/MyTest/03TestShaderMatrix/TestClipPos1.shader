Shader "Custom/TestClipPos1"
{
    Properties
    {
        _Color("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo", 2D) = "white" {}
        _NormalTex ("Normal", 2D) = "bump" {}
        _Smoothness("Smoothness", Range(0, 1)) = 0.5
        _Metallic("Metallic", Range(0, 1)) = 0.0

    	[KeywordEnum(NONE, WToOne, WToOneEx)] _ClipPosMode ("ClipPosMode", Float) = 0
    	
		// Blending state
        [HideInInspector] _Mode ("__mode", Float) = 0.0
        [HideInInspector] _SrcBlend ("__src", Float) = 1.0
        [HideInInspector] _DstBlend ("__dst", Float) = 0.0
        [HideInInspector] _ZWrite ("__zw", Float) = 1.0

		[Enum(UnityEngine.Rendering.CullMode)] _Cull("Cull Mode", Float) = 2
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
			Name "ForwardLit"
            Tags { "LightMode" = "UniversalForward"}

            Blend[_SrcBlend][_DstBlend]
            ZWrite[_ZWrite]
            Cull[_Cull]

            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile _ _CLIPPOSMODE_WTOONE _CLIPPOSMODE_WTOONEEX

            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Input.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/UnityInput.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/SpaceTransforms.hlsl"

            struct appdata
            {
                float4 positionOS   : POSITION;
                float2 uv           : TEXCOORD0;
                float3 normalOS     : NORMAL;
				float4 tangentOS    : TANGENT;
            };

            struct v2f
            {
                float2 uv                       : TEXCOORD0;
                float4 positionCS               : SV_POSITION;
                float3 positionWS               : TEXCOORD1;
                float3 normalWS                 : TEXCOORD2;
				float4 tangentWS                : TEXCOORD3;    // xyz: tangent, w: sign
				float3 viewDirWS                : TEXCOORD4;
            };

            CBUFFER_START(UnityPerMaterial)
				float4 _MainTex_ST;
				half4 _Color;
                half _Smoothness;
				half _Metallic;
				half _ClipWTo1;
			CBUFFER_END

            TEXTURE2D(_MainTex); SAMPLER(sampler_MainTex);
            TEXTURE2D(_NormalTex);

            v2f vert (appdata v)
            {
                v2f o;
                o.positionWS = TransformObjectToWorld(v.positionOS);
				o.positionCS = TransformWorldToHClip(o.positionWS);
                o.normalWS = TransformObjectToWorldNormal(v.normalOS);
                o.tangentWS = float4(TransformObjectToWorldDir(v.tangentOS.xyz), v.tangentOS.w);
                o.viewDirWS = _WorldSpaceCameraPos - o.positionWS;

                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
            	#if _CLIPPOSMODE_WTOONE
					o.positionCS.w = 1;
            	#elif _CLIPPOSMODE_WTOONEEX
					o.positionCS /= o.positionCS.w;
					o.positionCS.w = 1;
            	#endif
                return o;
            }

            void InitializeInputData(v2f input, half3 normalTS, out InputData inputData)
			{
				inputData = (InputData)0;

			#if defined(REQUIRES_WORLD_SPACE_POS_INTERPOLATOR)
				inputData.positionWS = input.positionWS;
			#endif

				half3 viewDirWS = SafeNormalize(input.viewDirWS);
			#if defined(_NORMALMAP) || defined(_DETAIL)
				float sgn = input.tangentWS.w;      // should be either +1 or -1
				float3 bitangent = sgn * cross(input.normalWS.xyz, input.tangentWS.xyz);
				inputData.normalWS = TransformTangentToWorld(normalTS, half3x3(input.tangentWS.xyz, bitangent.xyz, input.normalWS.xyz));
			#else
				inputData.normalWS = input.normalWS;
			#endif

				inputData.normalWS = NormalizeNormalPerPixel(inputData.normalWS);
				inputData.viewDirectionWS = viewDirWS;

			#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
				inputData.shadowCoord = input.shadowCoord;
			#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
				inputData.shadowCoord = TransformWorldToShadowCoord(inputData.positionWS);
			#else
				inputData.shadowCoord = float4(0, 0, 0, 0);
			#endif

				//inputData.fogCoord = input.fogFactorAndVertexLight.x;
				//inputData.vertexLighting = input.fogFactorAndVertexLight.yzw;
				//inputData.bakedGI = SAMPLE_GI(input.lightmapUV, input.vertexSH, inputData.normalWS);
				//inputData.normalizedScreenSpaceUV = GetNormalizedScreenSpaceUV(input.positionCS);
				//inputData.shadowMask = SAMPLE_SHADOWMASK(input.lightmapUV);
			}

            half4 frag (v2f i) : SV_Target
            {
                half4 albedo = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.uv);
                half3 normalTS = SAMPLE_TEXTURE2D(_NormalTex, sampler_MainTex, i.uv);
                InputData inputData;
                InitializeInputData(i, normalTS, inputData);
                half4 col = UniversalFragmentPBR(inputData, albedo, _Metallic, 0, _Smoothness, 0, 0, 1);
                return col;
            }
            ENDHLSL
        }
    }
}
