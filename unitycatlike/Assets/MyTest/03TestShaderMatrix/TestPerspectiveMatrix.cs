using System;
using System.Collections;
using System.Collections.Generic;
using Leyoutech.Utility;
using UnityEngine;

[ExecuteAlways]
public class TestPerspectiveMatrix : MonoBehaviour
{
    public Material mat;

    // Start is called before the first frame update
    void Start()
    {
        var mr = GetComponent<MeshRenderer>();
        if (mr != null)
        {
            mat = mr.sharedMaterial;
        }
    }

    private void Update()
    {
        if (mat != null)
        {
            var cam = Camera.main;
            if (cam != null)
            {
                mat.SetMatrix("_WorldToLocalMat", cam.transform.worldToLocalMatrix);
                mat.SetMatrix("_WorldToCamMat", cam.worldToCameraMatrix);
                var customProjMat = new Matrix4x4();
                CalcGLProjMat(ref customProjMat, cam);
                mat.SetMatrix("_CustomProjMat", customProjMat);                
                mat.SetMatrix("_GPUProjMatTrue", GL.GetGPUProjectionMatrix(customProjMat, true));            
                mat.SetMatrix("_GPUProjMatFalse", GL.GetGPUProjectionMatrix(customProjMat, false));                
                mat.SetMatrix("_UnityCamProjMat", cam.projectionMatrix);
            }
        }
    }

    // 
    void CalcMyProjMat(ref Matrix4x4 mat, Camera cam)
    {
        if (cam == null) return;
        
        float n = cam.nearClipPlane;
        float f = cam.farClipPlane;
        // Fov Axis is Vertical
        float fovV = Mathf.Deg2Rad * cam.fieldOfView;
        float zoomy = 1f / Mathf.Tan(fovV / 2f);
        // cam.aspect = width / height
        float zoomx = zoomy / cam.aspect; 
        mat.m00 = zoomx;
        mat.m11 = zoomy;
        mat.m22 = (f + n) / (f - n);
        mat.m23 = 1f;
        mat.m32 = 2 * f * n / (n - f);
        /*
        zoomx  0      0            0 
        0      zoomy  0            0
        0      0      (f+n)/(f-n)  1
        0      0      2nf/(n-f)    0
        **/
    }

    void MyProjMat2GLProjMat(ref Matrix4x4 mat)
    {
        var flipZMat = Matrix4x4.identity;
        flipZMat.m22 = -1;
        
        mat = flipZMat * mat;
        mat = mat.transpose;
    }

    void CalcGLProjMat(ref Matrix4x4 mat, Camera cam)
    {
        CalcMyProjMat(ref mat, cam);
        MyProjMat2GLProjMat(ref mat);
    }
}