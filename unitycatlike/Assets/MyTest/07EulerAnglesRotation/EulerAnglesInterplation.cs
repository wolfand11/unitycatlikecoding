﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EulerAnglesInterplation : MonoBehaviour
{
    public Vector3 startEulerAngle;
    public Vector3 endEulerAngle;
    public float duration = 3f;


    float curTime = 0;
    Vector3 tmp;
    void Update()
    {
        duration = Mathf.Abs(duration);
        if(duration<Mathf.Epsilon)
        {
            duration = 3f;
        }

        curTime += Time.deltaTime;
        float t = curTime / duration;
        tmp.x = Mathf.Lerp(startEulerAngle.x, endEulerAngle.x, t);
        tmp.y = Mathf.Lerp(startEulerAngle.y, endEulerAngle.y, t);
        tmp.z = Mathf.Lerp(startEulerAngle.z, endEulerAngle.z, t);
        transform.localEulerAngles = tmp;

        if(curTime>duration)
        {
            tmp = startEulerAngle;
            startEulerAngle = endEulerAngle;
            endEulerAngle = tmp;
            curTime = 0;
        }
    }
}
