﻿Shader "Custom/TestTextureInIf"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "red" {}
        _MainTex2 ("Texture2", 2D) = "gray" {}
		_Condition("Condition", Float) = 1.0
		_Condition2("Condition2", Float) = 1.0
		_Color1("Color1", Color) = (0,1,0,0)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
			Tags
            {
                "LightMode" = "ForwardBase"
            }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			#pragma multi_compile _ SHADOWS_SCREEN

            #include "UnityCG.cginc"
			#include "AutoLight.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 pos: SV_POSITION;
				float3 worldPos : TEXCOORD1;
				#if defined(UNITY_NO_SCREENSPACE_SHADOWS)
					UNITY_SHADOW_COORDS(2)
				#else
					SHADOW_COORDS(2)
				#endif
            };

            sampler2D _MainTex;
            sampler2D _MainTex2;
            float4 _MainTex_ST;
			float _Condition;
			float _Condition2;
			float4 _Color1;
			

            v2f vert (appdata v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);
				#if defined(UNITY_NO_SCREENSPACE_SHADOWS)
					TRANSFER_SHADOW(o);
				#else
					UNITY_TRANSFER_SHADOW(o, 0);
				#endif
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col1 = tex2D(_MainTex, i.uv) * 0.2;
				if (_Condition > 0.5)
				{
					col1 += tex2D(_MainTex2, i.uv+_Condition);
				}

				fixed4 col2 = tex2D(_MainTex2, i.uv) * 0.2;
				UNITY_LIGHT_ATTENUATION(attenuation, i, i.worldPos);
				if (_Condition2 > 0.5)
				{
					//UNITY_LIGHT_ATTENUATION(attenuation, i, i.worldPos);
					col2 *= attenuation;
					col2 *= _Color1;
					col1 *= _Color1;
				}
                return col1*col2;
            }
            ENDCG
        }
    }
}
