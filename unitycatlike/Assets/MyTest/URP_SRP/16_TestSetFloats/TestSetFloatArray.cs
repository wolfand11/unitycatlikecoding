using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSetFloatArray : MonoBehaviour
{
    public float[] floatArray = new []{0.0f,0.2f,0.4f,0.6f,0.8f,1.0f};
    public bool update = true;

    public TestSetFloatArray()
    {
        floatArray = new []{0.0f,0.2f,0.4f,0.6f,0.8f,1.0f};
    }

    private void Awake()
    {
        UploadFloatArray();
    }

    void Update()
    {
        if (update)
        {
            update = false;
            UploadFloatArray();
        }
    }

    void UploadFloatArray()
    {
        Shader.SetGlobalFloatArray("_FloatArray", floatArray);
    }
}
