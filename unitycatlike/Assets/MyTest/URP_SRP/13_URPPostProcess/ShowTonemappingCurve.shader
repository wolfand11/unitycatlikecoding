Shader "Test/URP/ShowTonemappingCurve"
{
    SubShader
    {
        Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline"}
        LOD 100

        Pass
        {
            Name "Blit"
            ZTest Always
            ZWrite Off
            Cull Off
            Blend One Zero

            HLSLPROGRAM
            #pragma vertex Vert
            #pragma fragment Fragment
            #pragma multi_compile_local _ _TONEMAP_ACES _TONEMAP_NEUTRAL

            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/ACES.hlsl"
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"

            TEXTURE2D_X(_CurveRT);
            half4 _CurveRT_TexelSize;
            float xScale;
            float yScale;
            half4 curveColor;
            half4 xAxisMarks;
            half4 yAxisMarks;
            half4 xAxisMarksColor;
            half4 yAxisMarksColor;
            half curveWidth;
            half markWidth;

            struct Attributes
            {
                float4 positionOS : POSITION;
                float2 uv         : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct Varyings
            {
                float4 positionCS : SV_POSITION;
                float2 uv         : TEXCOORD0;
                UNITY_VERTEX_OUTPUT_STEREO
            };

            Varyings Vert(Attributes input)
            {
				Varyings output;
                UNITY_SETUP_INSTANCE_ID(input);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

                output.positionCS = float4(input.positionOS.xy, 0, 1);
                output.uv = (input.positionOS.xy + 1) * 0.5;
                #if UNITY_UV_STARTS_AT_TOP
                output.uv = output.uv * half2(1, -1) + half2(0, 1);
                #endif
                return output;
            }

            float3 Tonemap(float3 colorLinear)
            {
                #if _TONEMAP_NEUTRAL
                {
                    colorLinear = NeutralTonemap(colorLinear);
                }
                #elif _TONEMAP_ACES
                {
                    // Note: input is actually ACEScg (AP1 w/ linear encoding)
                    float3 aces = ACEScg_to_ACES(colorLinear);
                    colorLinear = AcesTonemap(aces);
                }
                #endif
    
                return colorLinear;
            }

            float3 DrawPoint(float curPos, float pointPos, float axisScale, half4 pointColor, float pointSize)
            {
                //_CurveRT_TexelSize: 1 / width, 1 / height, width, height
                float pointMin = pointPos - abs(_CurveRT_TexelSize.y) * pointSize * axisScale;
                float pointMax = pointPos + abs(_CurveRT_TexelSize.y) * pointSize * axisScale;
                if(curPos>= pointMin && curPos<pointMax)
                {
                    return pointColor;
                }
                return 0;
            }

            half4 Fragment(Varyings input) : SV_Target
            {
                UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);
                float3 colorLinear = input.uv.x * xScale;
                //return half4(input.uv,0,1);
                float3 colorAfterTonemap = Tonemap(colorLinear);
                //return half4(colorAfterTonemap, 1);
                float4 color = 0;
                color.rgb += DrawPoint(input.uv.y*yScale, colorAfterTonemap.r, yScale, curveColor, curveWidth);
                // draw xAxis Marks
                color.rgb += DrawPoint(input.uv.x*xScale, xAxisMarks.x, xScale, xAxisMarksColor, markWidth);
                color.rgb += DrawPoint(input.uv.x*xScale, xAxisMarks.y, xScale, xAxisMarksColor, markWidth);
                color.rgb += DrawPoint(input.uv.x*xScale, xAxisMarks.z, xScale, xAxisMarksColor, markWidth);
                color.rgb += DrawPoint(input.uv.x*xScale, xAxisMarks.w, xScale, xAxisMarksColor, markWidth);
                // draw yAxis Marks
                color.rgb += DrawPoint(input.uv.y*yScale, yAxisMarks.x, yScale, yAxisMarksColor, markWidth);
                color.rgb += DrawPoint(input.uv.y*yScale, yAxisMarks.y, yScale, yAxisMarksColor, markWidth);
                color.rgb += DrawPoint(input.uv.y*yScale, yAxisMarks.z, yScale, yAxisMarksColor, markWidth);
                color.rgb += DrawPoint(input.uv.y*yScale, yAxisMarks.w, yScale, yAxisMarksColor, markWidth);
                return color;
            }
            ENDHLSL
        }
    }
}
