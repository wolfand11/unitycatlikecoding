using Leyoutech.Utility;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class ShowTonemappingCurve : ScriptableRendererFeature
{
    public TonemappingMode tonemappingMode = TonemappingMode.ACES;
    public RenderTexture curveRT;
    public Material material;
    public float xScale = 10;
    public float yScale = 10;
    public Color curveColor = Color.red;
    public Vector4 xAxisMarks = -Vector4.one;
    public Vector4 yAxisMarks = -Vector4.one;
    public Color xAxisMarksColor = Color.green;
    public Color yAxisMarksColor = Color.yellow;
    public float curveWidth = 1;
    public float markWidth = 1;

    class CustomRenderPass : ScriptableRenderPass
    {
        ShowTonemappingCurve settings;

        public CustomRenderPass(ShowTonemappingCurve settings)
        {
            this.settings = settings;
            base.profilingSampler = new ProfilingSampler(nameof(ShowTonemappingCurve));
        }

        // This method is called before executing the render pass.
        // It can be used to configure render targets and their clear state. Also to create temporary render target textures.
        // When empty this render pass will render to the active camera render target.
        // You should never call CommandBuffer.SetRenderTarget. Instead call <c>ConfigureTarget</c> and <c>ConfigureClear</c>.
        // The render pipeline will ensure target setup and clearing happens in an performance manner.
        public override void Configure(CommandBuffer cmd, RenderTextureDescriptor cameraTextureDescriptor)
        {
            settings.material.DisableKeyword("_TONEMAP_ACES");
            settings.material.DisableKeyword("_TONEMAP_NEUTRAL");
            if (settings.tonemappingMode == TonemappingMode.ACES)
            {
                settings.material.EnableKeyword("_TONEMAP_ACES");
            }
            else if (settings.tonemappingMode == TonemappingMode.Neutral)
            {
                settings.material.EnableKeyword("_TONEMAP_NEUTRAL");
            }
            settings.material.SetFloat("xScale", settings.xScale);
            settings.material.SetFloat("yScale", settings.yScale);
            settings.material.SetColor("curveColor", settings.curveColor);
            settings.material.SetTexture("_CurveRT", settings.curveRT);
            settings.material.SetVector("xAxisMarks", settings.xAxisMarks);
            settings.material.SetVector("yAxisMarks", settings.yAxisMarks);
            settings.material.SetColor("xAxisMarksColor", settings.xAxisMarksColor);
            settings.material.SetColor("yAxisMarksColor", settings.yAxisMarksColor);
            settings.material.SetFloat("curveWidth", settings.curveWidth);
            settings.material.SetFloat("markWidth", settings.markWidth);
            
            
            ConfigureTarget(settings.curveRT);
            ConfigureClear(ClearFlag.All, Color.clear);
        }

        // Here you can implement the rendering logic.
        // Use <c>ScriptableRenderContext</c> to issue drawing commands or execute command buffers
        // https://docs.unity3d.com/ScriptReference/Rendering.ScriptableRenderContext.html
        // You don't have to call ScriptableRenderContext.submit, the render pipeline will call it at specific points in the pipeline.
        public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
        {
            CommandBuffer cmd = CommandBufferPool.Get();
            using (new ProfilingScope(cmd, base.profilingSampler))
            {
                cmd.DrawMesh(RenderingUtils.fullscreenMesh, Matrix4x4.identity, settings.material, 0, 0);
            }

            context.ExecuteCommandBuffer(cmd);
            CommandBufferPool.Release(cmd);
        }

        /// Cleanup any allocated resources that were created during the execution of this render pass.
        public override void FrameCleanup(CommandBuffer cmd)
        {
        }
    }

    CustomRenderPass m_ScriptablePass;

    public override void Create()
    {
        Debug.Log("Create");

        if(material==null || curveRT==null) return;
        
        m_ScriptablePass = new CustomRenderPass(this);
        m_ScriptablePass.renderPassEvent = RenderPassEvent.BeforeRenderingOpaques;
    }

    // Here you can inject one or multiple render passes in the renderer.
    // This method is called when setting up the renderer once per-camera.
    public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
    {
        if(m_ScriptablePass!=null) renderer.EnqueuePass(m_ScriptablePass);
    }
}