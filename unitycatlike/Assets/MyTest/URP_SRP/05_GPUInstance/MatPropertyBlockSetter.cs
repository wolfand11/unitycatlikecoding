using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatPropertyBlockSetter : MonoBehaviour
{
    public bool randomColor = true;
    public bool needUpdate = true;
    public Color color = Color.white;
    public string colorPropName = "_BaseColor";
    int colorShaderID = Shader.PropertyToID("_BaseColor");

    Renderer renderer;
    MaterialPropertyBlock mpb;

    private void OnEnable()
    {
        if (renderer == null)
        {
            renderer = GetComponent<Renderer>();
        }
        if (mpb == null)
        {
            mpb = new MaterialPropertyBlock();
        }
    }

    void Update()
    {
        if (!needUpdate) return;
        needUpdate = false;

        colorShaderID = Shader.PropertyToID(colorPropName);

        if (renderer == null) return;
        if (mpb == null) return;

        if (randomColor)
        {
            color = UnityEngine.Random.ColorHSV();
        }
        renderer.GetPropertyBlock(mpb);
        mpb.SetColor(colorShaderID, color);
        renderer.SetPropertyBlock(mpb);
    }
}