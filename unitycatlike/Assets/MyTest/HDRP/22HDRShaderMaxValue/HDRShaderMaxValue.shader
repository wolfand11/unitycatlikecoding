Shader "Unlit/HDRShaderMaxValue"
{
    Properties
    {
        _MainColor("Color", Color) = (1,1,1)
        _Intensity("Intensity", Float) = 3
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
            };

            float4 _MainColor;
            float _Intensity;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                return o;
            }

            float4 frag(v2f i) : SV_Target
            {
                float4 color = _MainColor * _Intensity;
                color.g -= 1.0f;
                color.b -= 2.0f;
                return color;
            }
            ENDCG
        }
    }
}
