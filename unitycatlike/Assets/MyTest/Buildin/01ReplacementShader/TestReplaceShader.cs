﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestReplaceShader : MonoBehaviour
{
    public enum ReplaceTagName
    {
        kEmpty,
        kRenderType,
        kEffectTag,
    }

    public bool logEnabled = false;
    public bool drawInOnGUI = true;
    public ReplaceTagName eReplaceTagName;
    Camera _camera;
    Shader replacedShader;
    string replaceTag
    {
        get
        {
            switch(eReplaceTagName)
            {
                case ReplaceTagName.kEmpty:return "";
                case ReplaceTagName.kRenderType:return "RenderType";
                case ReplaceTagName.kEffectTag:return "EffectTag";
            }
            return "";
        }
    }
    private void Awake()
    {
        _camera = GetComponent<Camera>();
        replacedShader = Shader.Find("Unlit/TestReplaceShaderDes");
    }

    bool is_use = false;
    bool isReplaceTagEmpty = false;
    void OnGUI()
    {
        if (is_use && drawInOnGUI)
        {
            //使用高光shader：Specular来渲染Camera
            _camera.RenderWithShader(replacedShader, replaceTag);
        }

        isReplaceTagEmpty = GUI.Toggle(new Rect(10.0f, 10.0f, 300.0f, 45.0f), isReplaceTagEmpty, "Tag Is Empty");

        if (GUI.Button(new Rect(10.0f, 60.0f, 300.0f, 45.0f), "使用RenderWithShader"))
        {
            //RenderWithShader每调用一次只渲染一帧，所以不可将其直接放到这儿
            //_camera.RenderWithShader(replacedShader, "RenderType");
            is_use = true;
        }

        if (GUI.Button(new Rect(10.0f, 110.0f, 300.0f, 45.0f), "使用SetReplacementShader"))
        {
            //SetReplacementShader方法用来替换已有shader，调用一次即可
            _camera.SetReplacementShader(replacedShader, replaceTag);
            is_use = false;
        }
        if (GUI.Button(new Rect(10.0f, 160.0f, 300.0f, 45.0f), "ResetReplacementShader"))
        {
			//重置摄像机的shader渲染模式
            _camera.ResetReplacementShader();
            is_use = false;
        }
    }

    int OnRender_Counter = 0;
    void Update()
    {
        OnRender_Counter = 0;
    }

    //void OnPreCull()
    void OnPreRender()
    //void OnPostRender()
    {
        OnRender_Counter++;
        if (is_use && !drawInOnGUI && OnRender_Counter<5)
        {
            _camera.RenderWithShader(replacedShader, replaceTag);
        }       
        LogFunc("OnRender = " + OnRender_Counter);
    }

    void LogFunc(string funcMark)
    {
        if(logEnabled)
            Debug.Log("FrameNO = " + Time.frameCount + " " + funcMark);
    }
}
