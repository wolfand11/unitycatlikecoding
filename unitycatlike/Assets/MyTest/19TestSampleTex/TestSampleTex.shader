﻿Shader "Unlit/TestSampleTex"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		[KeywordEnum(ALL, R, G, B, A)] _ShowChannel("Show Channel", Float) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

			#pragma shader_feature _ _SHOWCHANNEL_R _SHOWCHANNEL_G _SHOWCHANNEL_B _SHOWCHANNEL_A
            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);
#if defined(_SHOWCHANNEL_R)
				return col.r;
#elif defined(_SHOWCHANNEL_G)
				return col.g;
#elif defined(_SHOWCHANNEL_B)
				return col.b;
#elif defined(_SHOWCHANNEL_A)
				return col.a;
#endif
                return col;
            }
            ENDCG
        }
    }
}
