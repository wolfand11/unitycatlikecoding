#ifndef MY_BUMPINESS
// Upgrade NOTE: excluded shader from DX11; has structs without semantics (struct v2f members tangentToWorld)
#pragma exclude_renderers d3d11
#define MY_BUMPINESS

#include "AutoLight.cginc"
#include "UnityPBSLighting.cginc"

struct appdata
{
	float4 vertex : POSITION;
	float2 uv : TEXCOORD0;
	float3 normal : NORMAL;
	float4 tangent: TANGENT;
};

struct v2f
{
	float2 uv : TEXCOORD0;
	float4 vertex : SV_POSITION;
	float3 normal : TEXCOORD1;
	float3 tangentToWorld[3] : TEXCOORD2;
	float3 worldPos : TEXCOORD5;
#ifdef VERTEXLIGHT_ON
	float3 vertexLightColor : TEXCOORD6;
#endif
};

sampler2D _MainTex;
float4 _MainTex_ST;
sampler2D _HeightMap;
sampler2D _NormalMap;
float4 _HeightMap_TexelSize;
half _BumpScale;
fixed _Metallic;
fixed _Smoothness;
sampler2D _DetailMap;
sampler2D _DetailNormal;

void ComputeVertexLightColor(inout v2f i)
{
#ifdef VERTEXLIGHT_ON
	i.vertexLightColor = Shade4PointLights(
		unity_4LightPosX0, unity_4LightPosY0, unity_4LightPosZ0,
		unity_LightColor[0].rgb, unity_LightColor[1].rgb,
		unity_LightColor[2].rgb, unity_LightColor[3].rgb,
		unity_4LightAtten0, i.worldPos, i.normal
	);
#endif
}

float3x3 MCreateTangentToWorldPerVertex(float3 normal, float3 tangent, half tangentSign)
{
	half sign = tangentSign * unity_WorldTransformParams.w;
	float3 binormal = cross(normal, tangent) * sign;
	return float3x3(tangent, binormal, normal);
}

v2f vert (appdata v)
{
	v2f o;
	o.vertex = UnityObjectToClipPos(v.vertex);
	o.uv = TRANSFORM_TEX(v.uv, _MainTex);
	o.normal = mul(v.normal, (float3x3)unity_WorldToObject);
	o.worldPos = mul(unity_ObjectToWorld, v.vertex);
	float3 tangentWorld = UnityObjectToWorldDir(v.tangent.xyz);
	float3x3 tangentToWorld = MCreateTangentToWorldPerVertex(o.normal, tangentWorld, v.tangent.w);
	o.tangentToWorld[0] = tangentToWorld[0];
	o.tangentToWorld[1] = tangentToWorld[1];
	o.tangentToWorld[2] = tangentToWorld[2];
	ComputeVertexLightColor(o);
	return o;
}

UnityLight CreateLight(v2f i)
{
	UnityLight light;
#if defined(POINT) || defined(SPOT) || defined(POINT_COOKIE)
	half3 lightVector = _WorldSpaceLightPos0.xyz - i.worldPos;
#else
	half3 lightVector = _WorldSpaceLightPos0.xyz;
#endif
	light.dir = normalize(lightVector);
	UNITY_LIGHT_ATTENUATION(attenuation, 0, i.worldPos);
	light.color = _LightColor0.rgb * attenuation;
	light.ndotl = DotClamped(i.normal, light.dir);
	return light;
}

UnityIndirect CreateIndirectLight(v2f i)
{
	UnityIndirect indirectLight;
	indirectLight.diffuse = 0;
	indirectLight.specular = 0;
#ifdef VERTEXLIGHT_ON
	indirectLight.diffuse = i.vertexLightColor;
#endif

#ifdef FORWARD_BASE_PASS
	indirectLight.diffuse += ShadeSH9(float4(i.normal, 1));
#endif

	return indirectLight;
}

//#define __USE_CONST_NORMAL
//#define __USE_HEIGHTMAP
//#define __USE_HEIGHTMAP_CALC_NORMAL_1
//#define __USE_HEIGHTMAP_CALC_NORMAL_2
#define __USE_NORMAL_MAP
float3 GetNormal(v2f i)
{
	float3 normal = float3(0, 1, 0);
#if defined(__USE_CONST_NORMAL)
	// do nothing
#elif defined(__USE_HEIGHTMAP)
	normal.y = tex2D(_HeightMap, i.uv).x;
#elif defined(__USE_HEIGHTMAP_CALC_NORMAL_1)
	float2 deltaU = float2(_HeightMap_TexelSize.x, 0);
	float h1 = tex2D(_HeightMap, i.uv);
	float h2 = tex2D(_HeightMap, i.uv+deltaU);
	normal = float3(deltaU.x, h2 - h1, 0);

	
	//#define __USE_TANGENT_AS_NORMAL 
	#define __FIX_HEIGHT_NORMAL
	#if defined(__USE_TANGENT_AS_NORMAL)
		#if defined(__FIX_HEIGHT_NORMAL)
			normal.y = 1;
		#endif
	#else
		normal = normal.yxz;
		#if defined(__FIX_HEIGHT_NORMAL)
			normal.y = 1;
		#endif
	#endif
#elif defined(__USE_HEIGHTMAP_CALC_NORMAL_2)
	float2 du = float2(_HeightMap_TexelSize.x * 0.5, 0);
	float u1 = tex2D(_HeightMap, i.uv - du);
	float u2 = tex2D(_HeightMap, i.uv + du);
	float3 tu = float3(1, u2 - u1, 0);

	float2 dv = float2(0, _HeightMap_TexelSize.y * 0.5);
	float v1 = tex2D(_HeightMap, i.uv - dv);
	float v2 = tex2D(_HeightMap, i.uv + dv);
	float3 tv = float3(0, v2 - v1, 1);

	normal = cross(tv, tu);
#elif defined(__USE_NORMAL_MAP)
	normal.xy = tex2D(_NormalMap, i.uv).wy * 2 - 1;
	normal.xy *= _BumpScale;
	normal.z = sqrt(1 - saturate(dot(normal.xy, normal.xy)));
	normal = normal.xzy;
#endif
#if defined(DETAILNORMAL_ON)
	float3 detailNormal;
	detailNormal.xy = tex2D(_DetailNormal, i.uv).wy * 2 - 1;
	detailNormal.xy *= _BumpScale;
	detailNormal.z = sqrt(1 - saturate(dot(detailNormal.xy, detailNormal.xy)));
	detailNormal = detailNormal.xzy;

	normal.x = normal.x + detailNormal.x;
	normal.y = normal.y * detailNormal.y;
	normal.z = normal.z + detailNormal.z;
#endif
	normal = normalize(normal);

#define __CALC_WORLDSPACE_NORMAL
#if defined(__CALC_WORLDSPACE_NORMAL)
	float3 wTangent = i.tangentToWorld[0];
	float3 wBinormal = i.tangentToWorld[1];
	float3 wNormal = i.tangentToWorld[2];
	normal = normalize(wTangent*normal.x + wBinormal*normal.y + wNormal*normal.z);
#endif
	return normal;
}

fixed4 frag (v2f i) : SV_Target
{
	fixed4 col;
	col.w = 1;
	fixed3 mainTexColor = tex2D(_MainTex, i.uv);

	half3 normal = GetNormal(i);
#ifdef ONLY_SH
	col.rgb = ShadeSH9(float4(i.normal, 1));
#else
	half3 viewDir = normalize(_WorldSpaceCameraPos - i.worldPos);
	fixed oneMinusReflectivity = unity_ColorSpaceDielectricSpec.a - _Metallic * unity_ColorSpaceDielectricSpec.a;

	fixed3 albedo = mainTexColor * oneMinusReflectivity;
	fixed3 specularColor = lerp(unity_ColorSpaceDielectricSpec.rgb, mainTexColor, _Metallic);

	UnityLight light = CreateLight(i);
	UnityIndirect indirectLight = CreateIndirectLight(i);
	col.xyz = UNITY_BRDF_PBS(albedo, specularColor, oneMinusReflectivity, _Smoothness, normal, viewDir, light, indirectLight);
#endif
	return col;
}

#endif