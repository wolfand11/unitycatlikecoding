﻿Shader "Custom/Bumpiness"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		[NoScaleOffset]_HeightMap("Heights", 2D) = "gray" {}
		[NoScaleOffset]_NormalMap("NormalMap", 2D) = "bump" {}
		_BumpScale("BumpScale", Float) = 1
        _Metallic("Metallic", Range(0, 1)) = 0.5
        _Smoothness("Smoothness", Range(0, 1)) = 0.5

		_DetailMap("Detail Map", 2D) = "white" {}
		[Toggle]DetailMap("Detail Map?", Float) = 0
		_DetailNormal("Detail Normal", 2D) = "bump" {}
		[Toggle]DetailNormal("Detail Normal?", Float) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            Tags
            {
                "LightMode" = "ForwardBase"
            }
            CGPROGRAM
            #pragma multi_compile _ VERTEXLIGHT_ON
            #pragma multi_compile _ ONLY_SH
			#pragma shader_feature _ DETAILMAP_ON
			#pragma shader_feature _ DETAILNORMAL_ON
            #define FORWARD_BASE_PASS
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 3.0
            #include "MyBumpiness.cginc"
            ENDCG
        }

        Pass
        {
            Tags
            {
                "LightMode" = "ForwardAdd"
            }
            Blend One One
            ZWrite Off
            CGPROGRAM
            #pragma multi_compile DIRECTIONAL POINT SPOT DIRECTIONAL_COOKIE POINT_COOKIE
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 3.0
            #include "MyBumpiness.cginc"
            ENDCG
        }
    }
}
