#ifndef MY_LIGHTING
#define MY_LIGHTING
#include "AutoLight.cginc"
#include "UnityPBSLighting.cginc"

struct appdata
{
	float4 vertex : POSITION;
	float2 uv : TEXCOORD0;
	float3 normal : NORMAL;
};

struct v2f
{
	float2 uv : TEXCOORD0;
	float4 vertex : SV_POSITION;
	float3 normal : TEXCOORD1;
	float3 worldPos : TEXCOORD2;
#ifdef VERTEXLIGHT_ON
	float3 vertexLightColor : TEXCOORD3;
#endif
};

sampler2D _MainTex;
float4 _MainTex_ST;
fixed _Metallic;
fixed _Smoothness;

void ComputeVertexLightColor(inout v2f i)
{
#ifdef VERTEXLIGHT_ON
	i.vertexLightColor = Shade4PointLights(
		unity_4LightPosX0, unity_4LightPosY0, unity_4LightPosZ0,
		unity_LightColor[0].rgb, unity_LightColor[1].rgb,
		unity_LightColor[2].rgb, unity_LightColor[3].rgb,
		unity_4LightAtten0, i.worldPos, i.normal
	);
#endif
}

v2f vert (appdata v)
{
	v2f o;
	o.vertex = UnityObjectToClipPos(v.vertex);
	o.uv = TRANSFORM_TEX(v.uv, _MainTex);
	o.normal = mul(v.normal, (float3x3)unity_WorldToObject);
	o.worldPos = mul(unity_ObjectToWorld, v.vertex);
	ComputeVertexLightColor(o);
	return o;
}

UnityLight CreateLight(v2f i)
{
	UnityLight light;
#if defined(POINT) || defined(SPOT) || defined(POINT_COOKIE)
	half3 lightVector = _WorldSpaceLightPos0.xyz - i.worldPos;
#else
	half3 lightVector = _WorldSpaceLightPos0.xyz;
#endif
	light.dir = normalize(lightVector);
	UNITY_LIGHT_ATTENUATION(attenuation, 0, i.worldPos);
	light.color = _LightColor0.rgb * attenuation;
	light.ndotl = DotClamped(i.normal, light.dir);
	return light;
}

UnityIndirect CreateIndirectLight(v2f i)
{
	UnityIndirect indirectLight;
	indirectLight.diffuse = 0;
	indirectLight.specular = 0;
#ifdef VERTEXLIGHT_ON
	indirectLight.diffuse = i.vertexLightColor;
#endif

#ifdef FORWARD_BASE_PASS
	indirectLight.diffuse += ShadeSH9(float4(i.normal, 1));
#endif

	return indirectLight;
}

fixed4 frag (v2f i) : SV_Target
{
	fixed4 col;
	col.w = 1;
	half3 normal = normalize(i.normal);
#ifdef ONLY_SH
	col.rgb = ShadeSH9(float4(i.normal, 1));
#else
	half3 viewDir = normalize(_WorldSpaceCameraPos - i.worldPos);
	fixed oneMinusReflectivity = unity_ColorSpaceDielectricSpec.a - _Metallic * unity_ColorSpaceDielectricSpec.a;
	fixed3 mainTexColor = tex2D(_MainTex, i.uv);
	fixed3 albedo = mainTexColor * oneMinusReflectivity;
	fixed3 specularColor = lerp(unity_ColorSpaceDielectricSpec.rgb, mainTexColor, _Metallic);

	UnityLight light = CreateLight(i);
	UnityIndirect indirectLight = CreateIndirectLight(i);
	col.xyz = UNITY_BRDF_PBS(albedo, specularColor, oneMinusReflectivity, _Smoothness, normal, viewDir, light, indirectLight);
#endif
	return col;
}

#endif