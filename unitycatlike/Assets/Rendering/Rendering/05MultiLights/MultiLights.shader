﻿Shader "Custom/MultiLights"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Metallic("Metallic", Range(0, 1)) = 0.5
        _Smoothness("Smoothness", Range(0, 1)) = 0.5

    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            Tags
            {
                "LightMode" = "ForwardBase"
            }
            CGPROGRAM
            #pragma multi_compile _ VERTEXLIGHT_ON
            #pragma multi_compile _ ONLY_SH
            #define FORWARD_BASE_PASS
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 3.0
            #include "MyLighting.cginc"
            ENDCG
        }

        Pass
        {
            Tags
            {
                "LightMode" = "ForwardAdd"
            }
            Blend One One
            ZWrite Off
            CGPROGRAM
            #pragma multi_compile DIRECTIONAL POINT SPOT DIRECTIONAL_COOKIE POINT_COOKIE
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 3.0
            #include "MyLighting.cginc"
            ENDCG
        }
    }
}
