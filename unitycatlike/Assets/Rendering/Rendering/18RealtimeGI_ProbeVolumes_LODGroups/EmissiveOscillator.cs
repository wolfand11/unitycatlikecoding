﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmissiveOscillator : MonoBehaviour
{
    Renderer r;
    Material mat;
    // Start is called before the first frame update
    void Start()
    {
        r = GetComponent<Renderer>();
        mat = r.material;
    }

    // Update is called once per frame
    void Update()
    {
        Color c = Color.Lerp(Color.white, Color.black, Mathf.Sin(Time.time * Mathf.PI) * 0.5f + 0.5f);
        mat.SetColor("_EmissionColor", c);

        // 更新实时GI

        // 下面代码会触发使用MetaPass渲染物体，将EmissionColor传递给Enlighten
        //r.UpdateGIMaterials();

        // 下面代码不会触发MetaPass渲染物体，只是直接将EmissionColor传递给Enlighten
        DynamicGI.SetEmissive(r, c);
    }
}
