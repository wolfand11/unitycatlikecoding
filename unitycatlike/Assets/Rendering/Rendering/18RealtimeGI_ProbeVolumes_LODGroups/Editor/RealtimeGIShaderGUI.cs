﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEditor;

public class RealtimeGIShaderGUI : ShaderGUI 
{
	public enum RenderingMode {
		Opaque, Cutout, Fade, Transparent
	}

	public struct RenderingSettings {
		public RenderQueue queue;
		public string renderType;
		public BlendMode srcBlend, dstBlend;
		public bool zWrite;

		public static RenderingSettings[] modes = {
			new RenderingSettings() {
				queue = RenderQueue.Geometry,
				renderType = "",
				srcBlend = BlendMode.One,
				dstBlend = BlendMode.Zero,
				zWrite = true,
			},
			new RenderingSettings() {
				queue = RenderQueue.AlphaTest,
				renderType = "TransparentCutout",
				srcBlend = BlendMode.One,
				dstBlend = BlendMode.Zero,
				zWrite = true,

			},
			new RenderingSettings() {
				queue = RenderQueue.Transparent,
				renderType = "Transparent",
				srcBlend = BlendMode.SrcAlpha,
				dstBlend = BlendMode.OneMinusSrcAlpha,
				zWrite = false,

			},
			new RenderingSettings() {
				queue = RenderQueue.Transparent,
				renderType = "Transparent",
				srcBlend = BlendMode.One,
				dstBlend = BlendMode.OneMinusSrcAlpha,
				zWrite = false,
			}
		};
	}

	Material target;
	MaterialEditor editor;
	MaterialProperty[] properties;

	static string[] customPropertyArr = new string[]
	{
		"_Emissive",
		"_EmissionColor",
		"_EmissiveMap",
	};
	
	public override void OnGUI (MaterialEditor editor, MaterialProperty[] properties)
	{
		this.target = editor.target as Material;
		this.editor = editor;
		this.properties = properties;

		editor.SetDefaultGUIWidths();
		DoRenderingMode();

		for(var i=0; i<properties.Length; i++)
		{
			var property = properties[i];
			if (IsCustomProperty(property))
			{
				continue;
			}

			if((property.flags & (MaterialProperty.PropFlags.HideInInspector | MaterialProperty.PropFlags.PerRendererData)) == MaterialProperty.PropFlags.None)
			{
				float propertyHeight = editor.GetPropertyHeight(property, property.displayName);
				Rect controlRect = EditorGUILayout.GetControlRect(true, propertyHeight, EditorStyles.layerMaskField, new GUILayoutOption[0]);
				editor.ShaderProperty(controlRect, property, property.displayName);
			}
		}
		DoEmissive();
		EditorGUILayout.Space();
		EditorGUILayout.Space();
	}

	static GUIContent staticLabel = new GUIContent();
	static GUIContent MakeLabel (string text, string tooltip = null) {
		staticLabel.text = text;
		staticLabel.tooltip = tooltip;
		return staticLabel;
	}

	bool IsCustomProperty(MaterialProperty property)
	{
		foreach(var p in customPropertyArr)
		{
			if (p == property.name) return true;
		}
		return false;
	}

	MaterialProperty FindProperty (string name) {
		return FindProperty(name, properties);
	}

	bool shouldShowAlphaCutoff = false;
	void DoRenderingMode () {
		RenderingMode mode = RenderingMode.Opaque;
		shouldShowAlphaCutoff = false;
		if (IsKeywordEnabled("_RENDERING_CUTOUT")) {
			mode = RenderingMode.Cutout;
			shouldShowAlphaCutoff = true;
		}
		else if (IsKeywordEnabled("_RENDERING_FADE"))
		{
			mode = RenderingMode.Fade;
		}
		else if(IsKeywordEnabled("_RENDERING_TRANSPARENT"))
		{
			mode = RenderingMode.Transparent;
		}
		
		EditorGUI.BeginChangeCheck();
		mode = (RenderingMode)EditorGUILayout.EnumPopup(
			MakeLabel("Rendering Mode"), mode
		);
		if (EditorGUI.EndChangeCheck())
		{
			RecordAction("Rendering Mode");
			SetKeyword("_RENDERING_CUTOUT", mode == RenderingMode.Cutout);
			SetKeyword("_RENDERING_FADE", mode == RenderingMode.Fade);
			SetKeyword("_RENDERING_TRANSPARENT", mode == RenderingMode.Transparent);

			RenderingSettings settings = RenderingSettings.modes[(int)mode];
			foreach(Material m in editor.targets)
			{
				m.renderQueue = (int)settings.queue;
				m.SetOverrideTag("RenderType", settings.renderType);
				m.SetInt("_SrcBlend", (int)settings.srcBlend);
				m.SetInt("_DstBlend", (int)settings.dstBlend);
				m.SetInt("_ZWrite", settings.zWrite ? 1 : 0);
			}
		}
	}

	void RecordAction (string label)
	{
		editor.RegisterPropertyChangeUndo(label);
	}

	bool IsKeywordEnabled(string keyword)
	{
		return target.IsKeywordEnabled(keyword);
	}

	void SetKeyword (string keyword, bool state)
	{
		if (state) {
			foreach (Material m in editor.targets) {
				m.EnableKeyword(keyword);
			}
		}
		else {
			foreach (Material m in editor.targets) {
				m.DisableKeyword(keyword);
			}
		}
	}

	void DoEmissive()
	{
		bool enableEmissive = target.IsKeywordEnabled("_EMISSION");
		EditorGUI.BeginChangeCheck();
		enableEmissive = EditorGUILayout.Toggle(MakeLabel("EnableEmissive"), enableEmissive);
		if (EditorGUI.EndChangeCheck())
		{
			foreach (Material m in editor.targets)
			{
				if(enableEmissive)
				{
					m.EnableKeyword("_EMISSION");
				}
				else
				{
					m.DisableKeyword("_EMISSION");
				}
			}
		}
		if(enableEmissive)
		{
			foreach (Material m in editor.targets)
			{
				m.globalIlluminationFlags &= ~MaterialGlobalIlluminationFlags.EmissiveIsBlack;
			}

			Color emission = target.GetColor(customPropertyArr[1]);
			EditorGUI.BeginChangeCheck();
			emission = EditorGUILayout.ColorField(MakeLabel("EmissionColor"), emission);
            editor.LightmapEmissionProperty(2);

			if (EditorGUI.EndChangeCheck())
			{
				foreach (Material m in editor.targets)
				{
					m.SetColor(customPropertyArr[1], emission);
				}
			}

			
			MaterialProperty emissiveMap = FindProperty(customPropertyArr[2]);
			EditorGUI.BeginChangeCheck();
			editor.TexturePropertySingleLine(MakeLabel("EmissiveMap", "EmissiveMap"), emissiveMap);
			if(EditorGUI.EndChangeCheck())
			{
				foreach (Material m in editor.targets)
				{
					m.SetTexture(customPropertyArr[2], emissiveMap.textureValue);
				}
			}
		}
	}
}
