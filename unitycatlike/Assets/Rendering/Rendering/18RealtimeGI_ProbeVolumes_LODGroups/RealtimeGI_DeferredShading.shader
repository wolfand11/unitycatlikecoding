﻿Shader "Hidden/Custom/RealtimeGIDeferredShading"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_SrcBlend ("", Float) = 1
		_DstBlend ("", Float) = 1
    }
    SubShader
    {
        Pass
        {
			//
			//Blend One One
			Blend [_SrcBlend] [_DstBlend]
			Cull Off 
			ZWrite Off 
			ZTest Always

            CGPROGRAM
			#pragma exclude_renderers nomrt
			#pragma target 3.0
            #pragma vertex vert
            #pragma fragment frag
			#pragma multi_compile_lightpass
			#pragma multi_compile _ UNITY_HDR_ON
            #include "RealtimeGI_DeferredShading.cginc"

            
            ENDCG
        }

		Pass
        {
			Cull Off 
			ZWrite Off 
			ZTest Always

			Stencil {
				Ref [_StencilNonBackground]
				ReadMask [_StencialNonBackground]
				CompBack Equal
				CompFront Equal
			}

            CGPROGRAM
			#pragma exclude_renderers nomrt
			#pragma target 3.0

            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
				float2 uv:TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
				float2 uv:TEXCOORD0;
            };
			sampler2D _LightBuffer;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
                return o;
            }


            fixed4 frag (v2f i) : SV_Target
            {
                return -log2(tex2D(_LightBuffer, i.uv));
            }
            ENDCG
        }

    }
}
