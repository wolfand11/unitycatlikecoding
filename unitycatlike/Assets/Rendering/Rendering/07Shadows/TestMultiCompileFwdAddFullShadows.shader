﻿Shader "Unlit/TestMultiCompileFwdAddFullShadows"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

		CGINCLUDE
		#include "UnityCG.cginc"
		struct appdata
		{
			float4 vertex : POSITION;
			float2 uv : TEXCOORD0;
		};

		struct v2f
		{
			float2 uv : TEXCOORD0;
			float4 vertex : SV_POSITION;
		};

		sampler2D _MainTex;
		float4 _MainTex_ST;

		v2f vert (appdata v)
		{
			v2f o;
			o.vertex = UnityObjectToClipPos(v.vertex);
			o.uv = TRANSFORM_TEX(v.uv, _MainTex);
			return o;
		}

		fixed4 frag (v2f i) : SV_Target
		{
			fixed4 col = tex2D(_MainTex, i.uv);
			return col;
		}

		fixed4 shadow_frag(v2f i) :SV_Target
		{
			return 0;
		}
		ENDCG

        Pass
        {
			Tags
            {
                "LightMode" = "ForwardBase"
            }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 3.0
            ENDCG
        }
		Pass
        {
			Tags
            {
                "LightMode" = "ForwardAdd"
				"SHADOWSUPPORT"="true"
            }
            Blend One One
            ZWrite Off
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 3.0
			#pragma multi_compile _ DIRECTIONAL
			#pragma multi_compile _ SHADOWS_SCREEN
			//#pragma multi_compile_fwdadd_fullshadows
            ENDCG
        }
		Pass
		{
			Tags
			{
				"LightMode" = "ShadowCaster"
			}
			CGPROGRAM
            #pragma vertex vert
            #pragma fragment shadow_frag
            #pragma target 3.0
			#pragma multi_compile_shadowcaster
			ENDCG
		}
    }
}
