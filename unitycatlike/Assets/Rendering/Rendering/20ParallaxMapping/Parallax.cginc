﻿// Upgrade NOTE: upgraded instancing buffer 'InstanceProperties' to new syntax.

#ifndef PARALLAX
#define PARALLAX

#include "UnityPBSLighting.cginc"
#include "AutoLight.cginc"

#if defined(SHADOWS_SCREEN) || defined(SHADOWS_DEPTH) || defined(SHADOWS_CUBE)
	#if defined(CUSTOM_SAMPLE_SHADOW) && defined(UNITY_NO_SCREENSPACE_SHADOWS)
		#define CUSTOM_SHADOWS_ON
	#endif
#endif
#if !defined(LIGHTMAP_ON) && defined(SHADOW_SCREEN)
	#if defined(SHADOWS_SHADOWMASK) && !defined(UNITY_NO_SCREENSPACE_SHADOWS)
//		#define ADDITIONAL_MASKED_DIRECTIONAL_SHADOWS 1
	#endif
#endif

#if defined(LIGHTMAP_ON) && defined(SHADOWS_SCREEN)
	#if defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK)
		#define SUBTRACTIVE_LIGHTING 1
	#endif
#endif

struct appdata
{
	UNITY_VERTEX_INPUT_INSTANCE_ID
	float4 vertex : POSITION;
	float2 uv : TEXCOORD0;
	float3 normal : NORMAL;
	float4 tangent: TANGENT;
	float2 uv1 : TEXCOORD1;
	float3 uv2 : TEXCOORD2;

};

struct vert_out
{
	UNITY_VERTEX_INPUT_INSTANCE_ID
	float4 uv : TEXCOORD0;
	float4 pos : SV_POSITION;
	float3 worldNormal : TEXCOORD1;
	float3 tangentToWorld[3] : TEXCOORD2;
	float3 worldPos : TEXCOORD5;
	UNITY_SHADOW_COORDS(6)
	DECLARE_LIGHT_COORDS(7)
#ifdef VERTEXLIGHT_ON
	float3 vertexLightColor : TEXCOORD8;
#elif defined(LIGHTMAP_ON) || ADDITIONAL_MASKED_DIRECTIONAL_SHADOWS
	float2 lightmapUV : TEXCOORD8;
#endif
#if defined(DYNAMICLIGHTMAP_ON)
	float2 dynamicLightmapUV : TEXCOORD9;
#endif
#if defined(_PARALLAX_ON)
	float3 tangentViewDir : TEXCOORD10;
#endif
};

struct frag_in
{
	UNITY_VERTEX_INPUT_INSTANCE_ID
	float4 uv : TEXCOORD0;
#if defined(LOD_FADE_CROSSFADE)
	UNITY_VPOS_TYPE vpos : VPOS;
#else
	float4 pos : SV_POSITION;
#endif
	float3 worldNormal : TEXCOORD1;
	float3 tangentToWorld[3] : TEXCOORD2;
	float3 worldPos : TEXCOORD5;
	UNITY_SHADOW_COORDS(6)
	DECLARE_LIGHT_COORDS(7)
#ifdef VERTEXLIGHT_ON
	float3 vertexLightColor : TEXCOORD8;
#elif defined(LIGHTMAP_ON) || ADDITIONAL_MASKED_DIRECTIONAL_SHADOWS
	float2 lightmapUV : TEXCOORD8;
#endif
#if defined(DYNAMICLIGHTMAP_ON)
	float2 dynamicLightmapUV : TEXCOORD9;
#endif
#if defined(_PARALLAX_ON)
	float3 tangentViewDir : TEXCOORD10;
#endif
};



struct frag_output
{
#if defined(DEFERRED_PASS)
	float4 gBuffer0 : SV_Target0;
	float4 gBuffer1 : SV_Target1;
	float4 gBuffer2 : SV_Target2;
	float4 gBuffer3 : SV_Target3;
	#if defined(SHADOWS_SHADOWMASK) && (UNITY_ALLOWED_MRT_COUNT>4)
		float4 gBuffer4 : SV_Target4;
	#endif
#else
	float4 color : SV_Target;
#endif
};

UNITY_INSTANCING_BUFFER_START(InstanceProperties)
//float4 _Color;
	UNITY_DEFINE_INSTANCED_PROP(float4, _Color)
#define _Color_arr InstanceProperties
UNITY_INSTANCING_BUFFER_END(InstanceProperties)

sampler2D _MainTex;
float4 _MainTex_ST;
float _Cutoff;
sampler2D _NormalMap;
float _BumpScale;
sampler2D _ParallaxMap;
float _ParallaxStrength;
fixed _Smoothness;
fixed _Metallic;
sampler2D _MetallicSmoothnessMap;
sampler2D _DetailTex;
float4 _DetailTex_ST;
sampler2D _DetailNormal;
sampler2D _DetailMask;
float3 _EmissionColor;
sampler2D _EmissiveMap;
sampler2D _OcclusionMap;
half _OcclusionStregth;

float3x3 MCreateTangentToWorldPerVertex(float3 normal, float3 tangent, half tangentSign)
{
	half sign = tangentSign * unity_WorldTransformParams.w;
	float3 binormal = cross(normal, tangent) * sign;
	return float3x3(tangent, binormal, normal);
}

void ComputeVertexLightColor(inout vert_out i)
{
#ifdef VERTEXLIGHT_ON
	i.vertexLightColor = Shade4PointLights(
		unity_4LightPosX0, unity_4LightPosY0, unity_4LightPosZ0,
		unity_LightColor[0].rgb, unity_LightColor[1].rgb,
		unity_LightColor[2].rgb, unity_LightColor[3].rgb,
		unity_4LightAtten0, i.worldPos, i.worldNormal
	);
#endif
}



vert_out vert (appdata v)
{
	vert_out o;
	UNITY_INITIALIZE_OUTPUT(vert_out, o);
	UNITY_SETUP_INSTANCE_ID(v);
	UNITY_TRANSFER_INSTANCE_ID(v, o);
	o.pos = UnityObjectToClipPos(v.vertex);
	o.uv.xy  = TRANSFORM_TEX(v.uv, _MainTex);
	o.uv.zw = TRANSFORM_TEX(v.uv, _DetailTex);
	o.worldNormal = mul(v.normal, (float3x3)unity_WorldToObject);
	o.worldPos = mul(unity_ObjectToWorld, v.vertex);
	float3 tangentWorld = UnityObjectToWorldDir(v.tangent.xyz);
	float3x3 tangentToWorld = MCreateTangentToWorldPerVertex(o.worldNormal, tangentWorld, v.tangent.w);
	o.tangentToWorld[0] = tangentToWorld[0];
	o.tangentToWorld[1] = tangentToWorld[1];
	o.tangentToWorld[2] = tangentToWorld[2];
	ComputeVertexLightColor(o);

	UNITY_TRANSFER_SHADOW(o, v.uv1);
	COMPUTE_LIGHT_COORDS(o);
#if defined(LIGHTMAP_ON) || ADDITIONAL_MASKED_DIRECTIONAL_SHADOWS
	o.lightmapUV = v.uv1 * unity_LightmapST.xy + unity_LightmapST.zw;
#endif
#if defined(DYNAMICLIGHTMAP_ON)
	o.dynamicLightmapUV = v.uv2 * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
#endif
#if defined(_PARALLAX_ON)
	#if defined(PARALLAX_SUPPORT_SCALED_DYNAMIC_BATCHING)
	v.tangent.xyz = normalize(v.tangent.xyz);
	v.normal = normalize(v.normal);
	#endif
	float3x3 objToTangent = float3x3(
		v.tangent.xyz,
		cross(v.normal, v.tangent.xyz) * v.tangent.w,
		v.normal
	);
	o.tangentViewDir = mul(objToTangent, ObjSpaceViewDir(v.vertex));
#endif
	return o;
}

float GetParallaxHeight(float2 uv)
{
	return tex2D(_ParallaxMap, uv).g;
}

float2 ParallaxOffset(float2 uv, float2 viewDir)
{
	float height = GetParallaxHeight(uv);
	//float height = 1;
	// height == 0.5 keep 
	// height <  0.5 reduce
	// height >  0.5 increase
	//height -= 0.5;
	height *= _ParallaxStrength;
	float2 uvOffset = viewDir * height;

	// uvOffset 为 viewDir 相同的方向
	return uvOffset;
}

float2 ParallaxRaymarching(float2 uv, float2 viewDir)
{
#if !defined(PARALLAX_RAYMARCHING_STEPS)
	#define PARALLAX_RAYMARCHING_STEPS 10
#endif

	float2 uvOffset = 0;
	float stepSize = 1.0/PARALLAX_RAYMARCHING_STEPS;
	float2 uvDelta = viewDir * stepSize * _ParallaxStrength;
	float stepHeight = 1;
	float surfaceHeight = GetParallaxHeight(uv);

	float2 prevUVOffset = uvOffset;
	float prevStepHeight = stepHeight;
	float prevSurfaceHeight = surfaceHeight;


	for (int i = 1; i<PARALLAX_RAYMARCHING_STEPS && stepHeight>surfaceHeight; i++)
	{
		prevUVOffset = uvOffset;
		prevStepHeight = stepHeight;
		prevSurfaceHeight = surfaceHeight;

		// Why use minus ? viewDir = CameraPos - VertexPos
		// we need VertexPos - CameraPos, so use -uvOffset
		uvOffset -= uvDelta;
		stepHeight -= stepSize;
		surfaceHeight = GetParallaxHeight(uv + uvOffset);
	}
#if !defined(PARALLAX_RAYMARCHING_SEARCH_STEPS)
	#define PARALLAX_RAYMARCHING_SEARCH_STEPS 0
#endif
#if PARALLAX_RAYMARCHING_SEARCH_STEPS>0
	for (int i = 0; i < PARALLAX_RAYMARCHING_SEARCH_STEPS; i++)
	{
		uvDelta *= 0.5;
		stepSize *= 0.5;

		if (stepHeight < surfaceHeight)
		{
			uvOffset += uvDelta;
			stepHeight += stepSize;
		}
		else
		{
			uvOffset -= uvDelta;
			stepHeight -= stepSize;
		}
		surfaceHeight = GetParallaxHeight(uv + uvOffset);
	}
#elif defined(PARALLAX_RAYMARCHING_INTERPOLATE)
	float prevDifference = prevStepHeight - prevSurfaceHeight;
	float difference = surfaceHeight - stepHeight;
	float t = prevDifference / (prevDifference + difference);
	uvOffset = lerp(prevUVOffset, uvOffset, t);
	//uvOffset = prevUVOffset - uvDelta * t;
#endif

	// uvOffset 为 viewDir 相反的方向
	return uvOffset;
}

void ApplyParallax(inout frag_in i)
{
#if defined(_PARALLAX_ON)
	i.tangentViewDir = normalize(i.tangentViewDir);
	#if !defined(PARALLAX_OFFSET_LIMITING)
		#if !defined(PARALLAX_BIAS)
			#define PARALLAX_BIAS 0.42
		#endif
		i.tangentViewDir.xy /= (i.tangentViewDir.z+PARALLAX_BIAS);
	#endif
	#if !defined(PARALLAX_FUNCTION)
		#define PARALLAX_FUNCTION ParallaxOffset
	#endif
	float2 uvOffset = PARALLAX_FUNCTION(i.uv.xy, i.tangentViewDir.xy);
	i.uv.xy += uvOffset;
	i.uv.zw += uvOffset * (_DetailTex_ST.xy/_MainTex_ST.xy);
	//i.uv.zw += uvOffset * _DetailTex_ST.xy;
#endif
}

float GetAlpha(frag_in i)
{
	float alpha = UNITY_ACCESS_INSTANCED_PROP(InstanceProperties, _Color).a;
	return tex2D(_MainTex, i.uv.xy).a * alpha;
}

float GetDetailMask(frag_in i)
{
	return tex2D(_DetailMask, i.uv.zw);
}

float3 GetAlbedo(frag_in i)
{
	float3 albedo = tex2D(_MainTex, i.uv.xy);
	albedo *= UNITY_ACCESS_INSTANCED_PROP(InstanceProperties, _Color).rgb;
#if defined(_ALBEDODETAIL_ON)
	float3 detailAlbedo = tex2D(_DetailTex, i.uv.zw);
	albedo = lerp(albedo, albedo*detailAlbedo*unity_ColorSpaceDouble, GetDetailMask(i));
#endif
	return albedo;
}

float3 GetNormal(frag_in i)
{
	float3 normal = float3(0, 1, 0);
#if defined(_NORMAL_ON)
	normal.xy = tex2D(_NormalMap, i.uv).wy * 2 - 1;
	normal.xy *= _BumpScale;
	normal.z = sqrt(1 - saturate(dot(normal.xy, normal.xy)));
	normal = normal.xzy;
#endif
#if defined(_NORMALDETAIL_ON)
	float3 detailNormal;
	detailNormal.xy = tex2D(_DetailNormal, i.uv).wy * 2 - 1;
	detailNormal.xy *= _BumpScale;
	detailNormal.z = sqrt(1 - saturate(dot(detailNormal.xy, detailNormal.xy)));
	detailNormal = lerp(float3(0,1,0), detailNormal.xzy, GetDetailMask(i));

	normal.x = normal.x + detailNormal.x;
	normal.y = normal.y * detailNormal.y;
	normal.z = normal.z + detailNormal.z;
#endif
	normal = normalize(normal);

#define __CALC_WORLDSPACE_NORMAL
#if defined(__CALC_WORLDSPACE_NORMAL)
	float3 wTangent = i.tangentToWorld[0];
	float3 wBinormal = i.tangentToWorld[1];
	float3 wNormal = i.tangentToWorld[2];
	normal = normalize(wTangent*normal.x + wNormal*normal.y + wBinormal*normal.z);
#endif
	return normal;
}

float GetMetallic(frag_in i)
{
#if defined(_METALLICMAP_ON)
	return tex2D(_MetallicSmoothnessMap, i.uv.xy).r * _Metallic;
#else
	return _Metallic;
#endif
}

float GetSmoothness(frag_in i)
{
#if defined(_SMOOTHNESSMAP_METALLIC) && defined(_METALLICMAP_ON)
	return tex2D(_MetallicSmoothnessMap, i.uv.xy).a * _Smoothness;
#elif defined(_SMOOTHNESSMAP_ALBEDO) 
	return tex2D(_MainTex, i.uv.xy).a * _Smoothness;
#else
	return _Smoothness;
#endif
}

float GetOcclusion(frag_in i)
{
	float occlusion = 1;
#if defined(_OCCLUSION_ON)
	occlusion = lerp(1, tex2D(_OcclusionMap, i.uv).g, _OcclusionStregth);
#endif
	return occlusion;
}

half3 BoxProjection(half3 dir, half3 pos, half4 cubemapPos, half3 boxMin, half3 boxMax)
{
	if (cubemapPos.w > 0)
	{
		half3 factors = ((dir > 0 ? boxMax : boxMin) - pos) / dir;
		half realFactor = min(min(factors.x, factors.y), factors.z);
		return dir * realFactor + (pos - cubemapPos);
	}
	return dir;
}

float3 GetEmissive(frag_in i)
{
	float3 emissive = 0;
#if defined(_EMISSION)
	if (dot(_EmissionColor, _EmissionColor) < 0.001)
	{
		emissive = tex2D(_EmissiveMap, i.uv);
	}
	else
	{
		emissive = _EmissionColor;
	}
#endif
	return emissive;
}

float GetShadowFade(frag_in i, float atten)
{
#if HANDLE_SHADOWS_BLENDING_IN_GI || ADDITIONAL_MASKED_DIRECTIONAL_SHADOWS
	#if ADDITIONAL_MASKED_DIRECTIONAL_SHADOWS
	atten = SHADOW_ATTENUATION(i);
	#endif
	// TODO: why
	float viewZ = dot(_WorldSpaceCameraPos - i.worldPos, UNITY_MATRIX_V[2].xyz);
	float shadowFadeDistance = UnityComputeShadowFadeDistance(i.worldPos, viewZ);
	float sFade = UnityComputeShadowFade(shadowFadeDistance);
	float bakedAtten = UnitySampleBakedOcclusion(i.lightmapUV, i.worldPos);
	return UnityMixRealtimeAndBakedShadows(atten, bakedAtten, sFade);

	// TODO: why atten + sFade
	//return saturate(atten + sFade);
#else
	return atten;
#endif
}

void ApplySubtractiveLighting(frag_in i, inout UnityIndirect indirect)
{
#if SUBTRACTIVE_LIGHTING
	UNITY_LIGHT_ATTENUATION(attenuation, i, i.worldPos.xyz);
	attenuation = GetShadowFade(i, attenuation);
	//float ndotl = 1;
	//使用顶点属性插值得到的normal计算所得的阴影非常淡 发现是因为没有对插值后的normal进行单位化导致的。
	//float ndotl = saturate(dot(i.worldNormal, _WorldSpaceLightPos0.xyz));
	float ndotl = saturate(dot(normalize(i.worldNormal), _WorldSpaceLightPos0.xyz));
	float3 needMinusLight = ndotl * _LightColor0.rgb * (1 - attenuation);
	float3 subtractedLight = indirect.diffuse - needMinusLight;
	subtractedLight = max(subtractedLight, unity_ShadowColor.rgb);
	// 支持ShadowStrength
	subtractedLight = lerp(subtractedLight, indirect.diffuse, _LightShadowData.x);

	// 静态物体上烘培的阴影也会显示unity_ShadowColor.rgb颜色
	//indirect.diffuse = indirect.diffuse;

	// 只修改动态物体在静态物体上投射的阴影
	indirect.diffuse = min(subtractedLight, indirect.diffuse);
#endif
}

UnityIndirect CreateIndirect(frag_in i, half3 viewDir, float smoothness)
{
	float occlusion = GetOcclusion(i);
	UnityIndirect indirect;
#ifdef VERTEXLIGHT_ON
	indirect.diffuse = i.vertexLightColor;
#else
	indirect.diffuse = 0;
#endif
	indirect.specular = 0;


#if defined(FORWARD_BASE_PASS) || defined(DEFERRED_PASS)
	#if defined(LIGHTMAP_ON)
		//indirect.diffuse = UNITY_SAMPLE_TEX2D(unity_Lightmap, i.lightmapUV);
		indirect.diffuse = DecodeLightmap(UNITY_SAMPLE_TEX2D(unity_Lightmap, i.lightmapUV));
		#if defined(DIRLIGHTMAP_COMBINED)
			float4 lightmapDir = UNITY_SAMPLE_TEX2D_SAMPLER(unity_LightmapInd, unity_Lightmap,i.lightmapUV);
			indirect.diffuse = DecodeDirectionalLightmap(indirect.diffuse, lightmapDir, GetNormal(i));
		#endif
		ApplySubtractiveLighting(i, indirect);
	#endif
	#if defined(DYNAMICLIGHTMAP_ON)
		// 动态Lightmap使用了和静态Lightmap不同的颜色格式，需要不同的解码
		float3 dynamicLightDiffuse = DecodeRealtimeLightmap(UNITY_SAMPLE_TEX2D(unity_DynamicLightmap, i.dynamicLightmapUV));
		#if defined(DIRLIGHTMAP_COMBINED)
			float4 dynamicLightmapDir = UNITY_SAMPLE_TEX2D_SAMPLER(unity_DynamicDirectionality, unity_DynamicLightmap,i.dynamicLightmapUV);
			indirect.diffuse += DecodeDirectionalLightmap(dynamicLightDiffuse, dynamicLightmapDir, GetNormal(i));
		#else
			indirect.diffuse += dynamicLightDiffuse;
		#endif
	#endif
	#if !defined(LIGHTMAP_ON) && !defined(DYNAMICLIGHTMAP_ON)
		#if UNITY_LIGHT_PROBE_PROXY_VOLUME
			if (unity_ProbeVolumeParams.x == 1)
			{
				indirect.diffuse = SHEvalLinearL0L1_SampleProbeVolume(float4(i.worldNormal, 1), i.worldPos);
				indirect.diffuse = max(0, indirect.diffuse);
				#if defined(UNITY_COLORSPACE_GAMMA)
					indirect.diffuse = LinearToGammaSpace(indirect.diffuse);
				#endif
			}
			else
			{
				// 叠加环境光 LightProbe
				indirect.diffuse += max(0, ShadeSH9(half4(i.worldNormal, 1)));
			}
		#else
			// 叠加环境光 LightProbe
			indirect.diffuse += max(0, ShadeSH9(half4(i.worldNormal, 1)));
		#endif
	#endif


	// 开启延迟渲染屏幕空间反射时，不计算环境反射
	#if defined(DEFERRED_PASS) && UNITY_ENABLE_REFLECTION_BUFFERS
		indirect.specular = fixed3(0,0,0);
	#else
		half roughness = 1 - smoothness;
		roughness *= 1.7 - 0.7 * roughness;

		half3 reflectDir = reflect(-viewDir, i.worldNormal);
		half3 reflectDir1 = BoxProjection(reflectDir, i.worldPos,
			unity_SpecCube0_ProbePosition,
			unity_SpecCube0_BoxMin,
			unity_SpecCube0_BoxMax);
		half4 envColor = UNITY_SAMPLE_TEXCUBE_LOD(unity_SpecCube0, reflectDir1, roughness*UNITY_SPECCUBE_LOD_STEPS);
		envColor.rgb = DecodeHDR(envColor, unity_SpecCube0_HDR);
		UNITY_BRANCH
		if (unity_SpecCube0_BoxMin.a < 0.9999)
		{
			half3 reflectDir2 = BoxProjection(reflectDir, i.worldPos,
				unity_SpecCube1_ProbePosition,
				unity_SpecCube1_BoxMin,
				unity_SpecCube1_BoxMax);
			half4 envColor2 = UNITY_SAMPLE_TEXCUBE_SAMPLER_LOD(unity_SpecCube1, unity_SpecCube0, reflectDir2, roughness*UNITY_SPECCUBE_LOD_STEPS);
			// DecodeHDR中代码处理了 intensity 逻辑
			envColor2.rgb = DecodeHDR(envColor2, unity_SpecCube1_HDR);
			envColor = lerp(envColor2, envColor, unity_SpecCube0_BoxMin.a);
		}
		//indirect.specular = half3(1, 0, 0);
		// 叠加环境反射
		//indirect.specular = envColor.rgb;

		indirect.diffuse *= occlusion;
		indirect.specular *= occlusion;
		//indirect.diffuse = fixed3(1,1,0);
		//indirect.specular = fixed3(1,0,0);
	#endif
#endif
	return indirect;
}



#if defined(CUSTOM_SHADOWS_ON)
	#if defined(SHADOWS_SCREEN) || defined(SHADOWS_CUBE)
		float4 _ShadowMapTexture_TexelSize;
	#endif

fixed CustomPCF(unityShadowCoord4 shadowCoord, half3 worldPos)
{
	//return UnitySampleShadowmap_PCF3x3Gaussian(shadowCoord, 0);
	half shadow = 0;
	fixed2 texelSize = _ShadowMapTexture_TexelSize.xy;
	half halfUVOffset = 1;
	for (int i = -halfUVOffset; i <= halfUVOffset; ++i)
	{
		for (int j = -halfUVOffset; j <= halfUVOffset; j++)
		{
			fixed4 uv = shadowCoord; 
			uv.xy += texelSize * half2(i, j);
#if defined(SHADOWS_SCREEN)
			shadow += UNITY_SAMPLE_SHADOW(_ShadowMapTexture, uv.xyz);
#elif defined(SHADOWS_DEPTH)
			shadow += UnitySampleShadowmap(uv);
#elif defined(SHADOWS_CUBE)
			shadow += UnitySampleShadowmap(worldPos - _LightPositionRange.xyz);
#endif
			//shadow += LerpShadow(uv);
		}
	}
	shadow /= ((halfUVOffset*2+1)*(halfUVOffset*2+1));
	return shadow;
}

fixed ComputeAttenuation(frag_in i)
{
	#if defined(SHADOWS_NATIVE)
		//unityShadowCoord4 shadowCoord = mul(unity_WorldToShadow[0], unityShadowCoord4(i.worldPos, 1));
		unityShadowCoord4 shadowCoord = i._ShadowCoord;
		fixed shadow = CustomPCF(shadowCoord, i.worldPos);

		shadow = _LightShadowData.r + shadow * (1-_LightShadowData.r);
		#if defined(SPOT)
			shadow = (i._LightCoord.z > 0) * UnitySpotCookie(i._LightCoord) * UnitySpotAttenuate(i._LightCoord.xyz) * shadow;
		#elif defined(POINT)
			unityShadowCoord3 lightCoord = mul(unity_WorldToLight, unityShadowCoord4(i.worldPos, 1)).xyz;
			shadow = tex2D(_LightTexture0, dot(lightCoord, lightCoord).rr).r * shadow;
		#endif
		return shadow;
	#else
		unityShadowCoord dist = SAMPLE_DEPTH_TEXTURE(_ShadowMapTexture, shadowCoord.xy);
		// tegra is confused if we use _LightShadowData.x directly
		// with "ambiguous overloaded function reference max(mediump float, float)"
		unityShadowCoord lightShadowDataX = _LightShadowData.x;
		unityShadowCoord threshold = shadowCoord.z;
		return max(dist > threshold, lightShadowDataX);
	#endif
}
#endif



UnityLight CreateLight(frag_in i)
{
	UnityLight light;
#if defined(DEFERRED_PASS) || SUBTRACTIVE_LIGHTING 
	light.dir = float3(0, 1, 0);
	light.color = 0;
#else
	#if defined(POINT) || defined(SPOT) || defined(POINT_COOKIE)
		half3 lightVect = _WorldSpaceLightPos0.xyz - i.worldPos;
	#else
		half3 lightVect = _WorldSpaceLightPos0.xyz;
	#endif
		light.dir = normalize(lightVect);

	#if defined(CUSTOM_SHADOWS_ON)
		half attenuation = ComputeAttenuation(i);
	#else
		UNITY_LIGHT_ATTENUATION(attenuation, i, i.worldPos);
	#endif
	attenuation = GetShadowFade(i, attenuation);

	light.color = _LightColor0 * attenuation;
	//light.color = _LightColor0;
#endif
	//light.ndotl = DotClamped(i.worldNormal, light.dir);
	return light;
}

frag_output frag (frag_in i)
{
#if defined(LOD_FADE_CROSSFADE)
	UnityApplyDitherCrossFade(i.vpos);
#endif
	ApplyParallax(i);
	frag_output output;
	float alpha = GetAlpha(i);
#if defined(_RENDERING_CUTOUT)
	clip(alpha - _Cutoff);
#endif

	fixed4 col; 
#if defined(_RENDERING_FADE) || defined(_RENDERING_TRANSPARENT)
	col.w = alpha;
#else
	col.w = 1;
#endif
	i.worldNormal = normalize(i.worldNormal);

	half3 normal = GetNormal(i);
	float metallic = GetMetallic(i);
	float smoothness = GetSmoothness(i);
	half oneMinusReflectivity = unity_ColorSpaceDielectricSpec.a - unity_ColorSpaceDielectricSpec.a * metallic;
	half3 mainTexCol = GetAlbedo(i);
	fixed3 albedo = mainTexCol * oneMinusReflectivity;
#if defined(_RENDERING_TRANSPARENT)
	albedo *= alpha;
	col.w = 1 - oneMinusReflectivity + alpha * oneMinusReflectivity;
#endif
	fixed3 specColor = lerp(unity_ColorSpaceDielectricSpec.rgb, mainTexCol, metallic);
	half3 viewDir = normalize(_WorldSpaceCameraPos.xyz - i.worldPos);
	UnityLight light = CreateLight(i);
	UnityIndirect indirect = CreateIndirect(i, viewDir, smoothness);
	col.xyz = UNITY_BRDF_PBS(albedo, specColor, oneMinusReflectivity, smoothness, normal, viewDir, light, indirect);
	col.xyz += GetEmissive(i);
#if defined(DEFERRED_PASS)
	output.gBuffer0.rgb = albedo;
	output.gBuffer0.a = GetOcclusion(i);
	output.gBuffer1.rgb = specColor;
	output.gBuffer1.a = smoothness;
	output.gBuffer2 = float4(normal * 0.5 + 0.5, 1);

	//col.rgb = indirect.diffuse.rgb;
	//col.rgb = 1;
	#if !defined(UNITY_HDR_ON)
		col.rgb = exp2(-col.rgb);
	#endif
	output.gBuffer3 = col;
	#if defined(SHADOWS_SHADOWMASK) && (UNITY_ALLOWED_MRT_COUNT>4)
		float2 shadowUV = 0;
		#if defined(LIGHTMAP_ON)
			shadowUV = i.lightmapUV;
		#endif
		output.gBuffer4 = UnityGetRawBakedOcclusions(shadowUV, i.worldPos.xyz);
	#endif
#else
	//output.color = indirect.diffuse.rgbr;
	output.color = col;
	//output.color = float4(1, 0, 0, 1);
#endif // DEFERRED_PASS
	return output;
}
#endif