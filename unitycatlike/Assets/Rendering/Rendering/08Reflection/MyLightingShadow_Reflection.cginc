﻿#ifndef MY_LIGHTING_SHADOW_REFLECTION
#define MY_LIGHTING_SHADOW_REFLECTION


#include "UnityPBSLighting.cginc"
#include "AutoLight.cginc"

#if defined(SHADOWS_SCREEN) || defined(SHADOWS_DEPTH) || defined(SHADOWS_CUBE)
    #if defined(CUSTOM_SAMPLE_SHADOW) && defined(UNITY_NO_SCREENSPACE_SHADOWS)
        #define CUSTOM_SHADOWS_ON
    #endif
#endif

struct appdata
{
    float4 vertex : POSITION;
    float2 uv : TEXCOORD0;
    float3 normal : NORMAL;
};

struct v2f
{
    float2 uv : TEXCOORD0;
    float4 pos : SV_POSITION;
    float3 worldNormal : TEXCOORD1;
    float3 worldPos : TEXCOORD2;
#if defined(UNITY_NO_SCREENSPACE_SHADOWS)
    UNITY_SHADOW_COORDS(3)
#else
    SHADOW_COORDS(3)
#endif
    DECLARE_LIGHT_COORDS(4)
};

sampler2D _MainTex;
float4 _MainTex_ST;
fixed _Smoothness;
fixed _Metallic;

v2f vert (appdata v)
{
    v2f o;
	UNITY_INITIALIZE_OUTPUT(v2f, o);
    o.pos = UnityObjectToClipPos(v.vertex);
    o.uv  = TRANSFORM_TEX(v.uv, _MainTex);
    o.worldNormal = mul(v.normal, (float3x3)unity_WorldToObject);
    o.worldPos = mul(unity_ObjectToWorld, v.vertex);
#if defined(UNITY_NO_SCREENSPACE_SHADOWS)
    TRANSFER_SHADOW(o);
#else
    UNITY_TRANSFER_SHADOW(o, 0);
#endif
    COMPUTE_LIGHT_COORDS(o);
    return o;
}

half3 BoxProjection(half3 dir, half3 pos, half4 cubemapPos, half3 boxMin, half3 boxMax)
{
    if (cubemapPos.w > 0)
    {
        half3 factors = ((dir > 0 ? boxMax : boxMin) - pos) / dir;
        half realFactor = min(min(factors.x, factors.y), factors.z);
        return dir * realFactor + (pos - cubemapPos);
    }
    return dir;
}

UnityIndirect CreateIndirect(v2f i, half3 viewDir)
{
    UnityIndirect indirect;
    indirect.diffuse = 0;
    indirect.specular = 0;
#if defined(FORWARD_BASE_PASS)
    indirect.diffuse += max(0, ShadeSH9(half4(i.worldNormal, 1)));

    half roughness = 1 - _Smoothness;
    roughness *= 1.7 - 0.7 * roughness;

    half3 reflectDir = reflect(-viewDir, i.worldNormal);
    half3 reflectDir1 = BoxProjection(reflectDir, i.worldPos,
        unity_SpecCube0_ProbePosition,
        unity_SpecCube0_BoxMin,
        unity_SpecCube0_BoxMax);
    half4 envColor = UNITY_SAMPLE_TEXCUBE_LOD(unity_SpecCube0, reflectDir1, roughness*UNITY_SPECCUBE_LOD_STEPS);
    envColor.rgb = DecodeHDR(envColor, unity_SpecCube0_HDR);
    UNITY_BRANCH
    if (unity_SpecCube0_BoxMin.a < 0.9999)
    {
        half3 reflectDir2 = BoxProjection(reflectDir, i.worldPos,
            unity_SpecCube1_ProbePosition,
            unity_SpecCube1_BoxMin,
            unity_SpecCube1_BoxMax);
        half4 envColor2 = UNITY_SAMPLE_TEXCUBE_SAMPLER_LOD(unity_SpecCube1, unity_SpecCube0, reflectDir2, roughness*UNITY_SPECCUBE_LOD_STEPS);
        // DecodeHDR中代码处理了 intensity 逻辑
        envColor2.rgb = DecodeHDR(envColor2, unity_SpecCube1_HDR);
        envColor = lerp(envColor2, envColor, unity_SpecCube0_BoxMin.a);
    }
    //indirect.specular = half3(1, 0, 0);
    indirect.specular = envColor.rgb;
#endif
    return indirect;
}



#if defined(CUSTOM_SHADOWS_ON)
    #if defined(SHADOWS_SCREEN) || defined(SHADOWS_CUBE)
        float4 _ShadowMapTexture_TexelSize;
    #endif

fixed CustomPCF(unityShadowCoord4 shadowCoord, half3 worldPos)
{
    //return UnitySampleShadowmap_PCF3x3Gaussian(shadowCoord, 0);
    half shadow = 0;
    fixed2 texelSize = _ShadowMapTexture_TexelSize.xy;
    half halfUVOffset = 1;
    for (int i = -halfUVOffset; i <= halfUVOffset; ++i)
    {
        for (int j = -halfUVOffset; j <= halfUVOffset; j++)
        {
            fixed4 uv = shadowCoord; 
            uv.xy += texelSize * half2(i, j);
#if defined(SHADOWS_SCREEN)
            shadow += UNITY_SAMPLE_SHADOW(_ShadowMapTexture, uv.xyz);
#elif defined(SHADOWS_DEPTH)
            shadow += UnitySampleShadowmap(uv);
#elif defined(SHADOWS_CUBE)
            shadow += UnitySampleShadowmap(worldPos - _LightPositionRange.xyz);
#endif
            //shadow += LerpShadow(uv);
        }
    }
    shadow /= ((halfUVOffset*2+1)*(halfUVOffset*2+1));
    return shadow;
}

fixed ComputeAttenuation(v2f i)
{
    #if defined(SHADOWS_NATIVE)
        //unityShadowCoord4 shadowCoord = mul(unity_WorldToShadow[0], unityShadowCoord4(i.worldPos, 1));
        unityShadowCoord4 shadowCoord = i._ShadowCoord;
        fixed shadow = CustomPCF(shadowCoord, i.worldPos);

        shadow = _LightShadowData.r + shadow * (1-_LightShadowData.r);
        #if defined(SPOT)
            shadow = (i._LightCoord.z > 0) * UnitySpotCookie(i._LightCoord) * UnitySpotAttenuate(i._LightCoord.xyz) * shadow;
        #elif defined(POINT)
            unityShadowCoord3 lightCoord = mul(unity_WorldToLight, unityShadowCoord4(i.worldPos, 1)).xyz;
            shadow = tex2D(_LightTexture0, dot(lightCoord, lightCoord).rr).r * shadow;
        #endif
        return shadow;
    #else
        unityShadowCoord dist = SAMPLE_DEPTH_TEXTURE(_ShadowMapTexture, shadowCoord.xy);
        // tegra is confused if we use _LightShadowData.x directly
        // with "ambiguous overloaded function reference max(mediump float, float)"
        unityShadowCoord lightShadowDataX = _LightShadowData.x;
        unityShadowCoord threshold = shadowCoord.z;
        return max(dist > threshold, lightShadowDataX);
    #endif
}
#endif

UnityLight CreateLight(v2f i)
{
    UnityLight light;
#if defined(POINT) || defined(SPOT) || defined(POINT_COOKIE)
    half3 lightVect = _WorldSpaceLightPos0.xyz - i.worldPos;
#else
    half3 lightVect = _WorldSpaceLightPos0.xyz;
#endif
    light.dir = normalize(lightVect);

#if defined(CUSTOM_SHADOWS_ON)
    half attenuation = ComputeAttenuation(i);
#else
    UNITY_LIGHT_ATTENUATION(attenuation, i, i.worldPos);
#endif
    light.color = _LightColor0 * attenuation;
    light.ndotl = DotClamped(i.worldNormal, light.dir);
    return light;
}

fixed4 frag (v2f i) : SV_Target
{
    fixed4 col; 
    col.w = 1;
    half3 normal = normalize(i.worldNormal);
    half oneMinusReflectivity = unity_ColorSpaceDielectricSpec.a - unity_ColorSpaceDielectricSpec.a * _Metallic;
    half3 mainTexCol = tex2D(_MainTex, i.uv);
    fixed3 albedo = mainTexCol * oneMinusReflectivity;
    fixed3 specColor = lerp(unity_ColorSpaceDielectricSpec.rgb, mainTexCol, _Metallic);
    half3 viewDir = normalize(_WorldSpaceCameraPos.xyz - i.worldPos);
    UnityLight light = CreateLight(i);
    UnityIndirect indirect = CreateIndirect(i, viewDir);

    col.xyz = UNITY_BRDF_PBS(albedo, specColor, oneMinusReflectivity, _Smoothness, normal, viewDir, light, indirect);
    //col.xyz = i._ShadowCoord.yzw;
    return col;
}

#endif