﻿Shader "Custom/Reflection"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Metallic("Metallic", Range(0, 1)) = 0.5
        _Smoothness("Smoothness", Range(0, 1)) = 0.5
		[Toggle(CUSTOM_SAMPLE_SHADOW)] CUSTOM_SAMPLE_SHADOW("SoftShadow", Float) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            Tags
            {
                "LightMode" = "ForwardBase"
            }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 3.0
            #define FORWARD_BASE_PASS
			#pragma multi_compile _ SHADOWS_SCREEN
			#pragma multi_compile _ CUSTOM_SAMPLE_SHADOW
            #include "MyLightingShadow_Reflection.cginc"
            ENDCG
        }
        Pass
        {
            Tags
            {
                "LightMode" = "ForwardAdd"
            }
			Blend One One
			ZWrite Off
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 3.0

            //#pragma multi_compile DIRECTIONAL
			//#pragma multi_compile SHADOWS_SCREEN
			#pragma multi_compile_fwdadd_fullshadows
			#pragma multi_compile _ CUSTOM_SAMPLE_SHADOW
            #include "MyLightingShadow_Reflection.cginc"
            ENDCG
        }
		Pass
		{
			Tags
			{
				"LightMode" = "ShadowCaster"
			}
			CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 3.0
			#pragma multi_compile_shadowcaster
			#include "MyShadow_Reflection.cginc"
			ENDCG
		}
    }
}
