﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GPUInstancingTest : MonoBehaviour
{
    public enum GenGObjType
    {
        kSphere,
        kCube,
        kSphereAndCube,
    }
    public GenGObjType genGObjType = GenGObjType.kCube;
    public Transform prefabSphere;
    public Transform prefabCube;
    public int instances = 5000;
    public float radius = 50f;

    void GenGObj()
    {
        if (prefabSphere == null) return;
        if (prefabCube == null) return;

        for(int i=transform.childCount-1; i>=0; i--)
        {
            GameObject.DestroyImmediate(transform.GetChild(i).gameObject);
        }

        MaterialPropertyBlock mpb = new MaterialPropertyBlock();
        for(int i=0; i<instances; i++)
        {
            Transform t = null;
            if(genGObjType == GenGObjType.kCube)
            {
                t = Instantiate(prefabCube);
            }
            else if(genGObjType == GenGObjType.kSphere)
            {
                t = Instantiate(prefabSphere);
            }
            else
            {
                t = i % 2 == 0 ? Instantiate(prefabCube) : Instantiate(prefabSphere);
            }
            mpb.SetColor("_Color", new Color(Random.value, Random.value, Random.value));
            t.GetComponent<MeshRenderer>().SetPropertyBlock(mpb);
            t.localPosition = Random.insideUnitSphere * radius;
            t.SetParent(transform);
        }
    }

    GenGObjType _tmpGenGObjType;
    void Update()
    {
        if(_tmpGenGObjType!=genGObjType)
        {
            GenGObj();

            _tmpGenGObjType = genGObjType;
        }
    }
}
