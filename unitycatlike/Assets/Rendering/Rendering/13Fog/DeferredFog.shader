﻿Shader "Custom/DeferredFog"
{
	CGINCLUDE
	#include "UnityCG.cginc"
	sampler2D _MainTex;
	sampler2D _CameraDepthTexture;
	float3 _FrustumCorners[4];

	struct VertexData {
		float4 vertex : POSITION;
	};

	struct Interpolators {
		float4 pos : SV_POSITION;
		float2 uv : TEXCOORD0;
#if defined(FOG_DISTANCE)
		float3 ray;
#endif
	};

	Interpolators VertexProgram (VertexData v) {
		Interpolators i;
		i.pos = float4(v.vertex.xy, 0.0, 1.0);
        // 直接通过三角形顶点坐标求的uv坐标
		i.uv = (v.vertex + 1) * 0.5;
#if UNITY_UV_STARTS_AT_TOP
		i.uv= i.uv * float2(1.0, -1.0) + float2(0.0, 1.0);
#endif
#if defined(FOG_DISTANCE)
		// 将 i.uv 映射为数组的索引值
		i.ray = _FrustumCorners[i.uv.x+2*i.uv.y]
#endif
		return i;
	}

	float4 FragmentProgram (Interpolators i) : SV_Target {
		float depth = SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, i.uv);
		depth = Linear01Depth(depth);
		// 为了和Forward Path中深度雾计算一直需要减掉near , 参考 UNITY_Z_0_FAR_FROM_CLIPSPACE 函数中的处理
		// _ProjectionParams // x=1 or -1  y=near z=far w=1/far
#if defined(FOG_DISTANCE)
		float viewDistance = length(depth * i.ray);
#else
		float viewDistance = depth * _ProjectionParams.z - _ProjectionParams.y;
#endif
		UNITY_CALC_FOG_FACTOR_RAW(viewDistance);
		unityFogFactor = saturate(unityFogFactor);
		// 天空盒深度值为1，通过下面判断，让雾不影响天空盒
		if (depth > 0.9999)
		{
			unityFogFactor = 1;
		}
		
		float3 color = tex2D(_MainTex, i.uv).rgb;
		float3 foggedColor = lerp(unity_FogColor.rgb, color, unityFogFactor);
		return float4(foggedColor, 1);
	}
	ENDCG

    SubShader
    {
        Cull Off ZWrite Off ZTest Always
		
		Pass {
			CGPROGRAM

			#pragma vertex VertexProgram
			#pragma fragment FragmentProgram

			#pragma multi_compile_fog
			ENDCG
		}
		Pass {
			CGPROGRAM

			#define FOG_DISTANCE
			#pragma vertex VertexProgram
			#pragma fragment FragmentProgram

			#pragma multi_compile_fog
			ENDCG
		}
    }
}
