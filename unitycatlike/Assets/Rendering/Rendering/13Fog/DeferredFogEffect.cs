﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;


namespace UnityEngine.Rendering.PostProcessing
{
    [Serializable]
    [PostProcess(typeof(DeferredFogEffectRenderer), PostProcessEvent.BeforeTransparent, "Custom/DeferredFog")]
    public sealed class DeferredFogEffect : PostProcessEffectSettings
    {
        public enum FogType
        {
            kDepth,
            kDistance
        }
        [Serializable]
        public class FogTypeParameter : ParameterOverride<FogType> { }

        public FogTypeParameter fogType = new FogTypeParameter { value = FogType.kDepth };

        public override bool IsEnabledAndSupported(PostProcessRenderContext context)
        {
            return enabled.value;
        }
    }

    public sealed class DeferredFogEffectRenderer : PostProcessEffectRenderer<DeferredFogEffect>
    {
        Shader shader;
        Camera deferredCamera;
        Vector3[] frustumCorners = new Vector3[4];
        Vector4[] vect4Array = new Vector4[4];
        Rect screenRect = new Rect(0, 0, 1, 1);
        public override void Init()
        {
            shader = Shader.Find("Custom/DeferredFog");
        }

        public override void Render(PostProcessRenderContext context)
        {
            if (shader == null) return;
            var sheet = context.propertySheets.Get(shader);
            if (sheet == null) return;
            context.camera.CalculateFrustumCorners(
                screenRect,
                context.camera.farClipPlane,
                context.camera.stereoActiveEye,
                frustumCorners
            );
            vect4Array[0] = frustumCorners[0];
            vect4Array[1] = frustumCorners[3];
            vect4Array[2] = frustumCorners[1];
            vect4Array[3] = frustumCorners[2];
            sheet.properties.SetVectorArray("_FrustumCorners", vect4Array);
            if(settings.fogType.value == DeferredFogEffect.FogType.kDepth)
            {
                context.command.BlitFullscreenTriangle(context.source, context.destination, sheet, 0);
            }
            else
            {
                context.command.BlitFullscreenTriangle(context.source, context.destination, sheet, 1);
            }
        }
    }
}
