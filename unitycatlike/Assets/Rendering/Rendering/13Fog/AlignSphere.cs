﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class AlignSphere : MonoBehaviour
{
    public Vector3 pos;
    public float xSpan = 1;
    public float zSpan = 1;
    public int xCount = 4;
    public bool realign;

    private void Start()
    {
        realign = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (realign)
        {
            realign = false;
            Vector3 offset = Vector3.zero;

            if (transform.childCount % 2 == 0)
            {
                offset.x = (xCount / 2 - 0.5f) * (-xSpan);
            }
            else
            {
                offset.x = (xCount - 1) / 2 * -xSpan;
            }

            for (var i = 0; i < transform.childCount; i++)
            {
                int row = i / xCount;
                int column = i % xCount;
                Vector3 tmpPos = offset;
                tmpPos.x += (column * xSpan);
                tmpPos.z += (row * zSpan);
                transform.GetChild(i).localPosition = tmpPos;
            }
        }
    }
}
