﻿#ifndef MY_FOG
#define MY_FOG

#include "UnityPBSLighting.cginc"
#include "AutoLight.cginc"

#if defined(FOG_LINEAR) || defined(FOG_EXP) || defined(FOG_EXP2)
	#define FOG_DEPTH 1
#endif

#if defined(SHADOWS_SCREEN) || defined(SHADOWS_DEPTH) || defined(SHADOWS_CUBE)
	#if defined(CUSTOM_SAMPLE_SHADOW) && defined(UNITY_NO_SCREENSPACE_SHADOWS)
		#define CUSTOM_SHADOWS_ON
	#endif
#endif

struct appdata
{
    float4 vertex : POSITION;
    float2 uv : TEXCOORD0;
	float3 normal : NORMAL;
	float4 tangent: TANGENT;
};

struct v2f
{
    float4 uv : TEXCOORD0;
	float4 pos : SV_POSITION;
	float3 worldNormal : TEXCOORD1;
	float3 tangentToWorld[3] : TEXCOORD2;
#if FOG_DEPTH
	float4 worldPos : TEXCOORD5;
#else
	float3 worldPos : TEXCOORD5;
#endif
#if defined(UNITY_NO_SCREENSPACE_SHADOWS)
	UNITY_SHADOW_COORDS(6)
#else
	SHADOW_COORDS(6)
#endif
	DECLARE_LIGHT_COORDS(7)
#ifdef VERTEXLIGHT_ON
	float3 vertexLightColor : TEXCOORD8;
#endif

};

struct frag_output 
{
#if defined(DEFERRED_PASS)
	float4 gBuffer0 : SV_Target0;
	float4 gBuffer1 : SV_Target1;
	float4 gBuffer2 : SV_Target2;
	float4 gBuffer3 : SV_Target3;
#else
	float4 color : SV_Target;
#endif
};

sampler2D _MainTex;
float4 _MainTex_ST;
float _Cutoff;
sampler2D _NormalMap;
float _BumpScale;
fixed _Smoothness;
fixed _Metallic;
sampler2D _MetallicSmoothnessMap;
sampler2D _DetailTex;
float4 _DetailTex_ST;
sampler2D _DetailNormal;
sampler2D _DetailMask;
sampler2D _EmissiveMap;
sampler2D _OcclusionMap;
half _OcclusionStregth;

float3x3 MCreateTangentToWorldPerVertex(float3 normal, float3 tangent, half tangentSign)
{
	half sign = tangentSign * unity_WorldTransformParams.w;
	float3 binormal = cross(normal, tangent) * sign;
	return float3x3(tangent, binormal, normal);
}

void ComputeVertexLightColor(inout v2f i)
{
#ifdef VERTEXLIGHT_ON
	i.vertexLightColor = Shade4PointLights(
		unity_4LightPosX0, unity_4LightPosY0, unity_4LightPosZ0,
		unity_LightColor[0].rgb, unity_LightColor[1].rgb,
		unity_LightColor[2].rgb, unity_LightColor[3].rgb,
		unity_4LightAtten0, i.worldPos, i.worldNormal
	);
#endif
}

v2f vert (appdata v)
{
    v2f o;
	UNITY_INITIALIZE_OUTPUT(v2f, o);
    o.pos = UnityObjectToClipPos(v.vertex);
    o.uv.xy  = TRANSFORM_TEX(v.uv, _MainTex);
	o.uv.zw = TRANSFORM_TEX(v.uv, _DetailTex);
	o.worldNormal = mul(v.normal, (float3x3)unity_WorldToObject);
	o.worldPos = mul(unity_ObjectToWorld, v.vertex);
#if FOG_DEPTH
	// clip space z
	o.worldPos.w = o.pos.z;
#endif
	float3 tangentWorld = UnityObjectToWorldDir(v.tangent.xyz);
	float3x3 tangentToWorld = MCreateTangentToWorldPerVertex(o.worldNormal, tangentWorld, v.tangent.w);
	o.tangentToWorld[0] = tangentToWorld[0];
	o.tangentToWorld[1] = tangentToWorld[1];
	o.tangentToWorld[2] = tangentToWorld[2];
	ComputeVertexLightColor(o);

#if defined(UNITY_NO_SCREENSPACE_SHADOWS)
	TRANSFER_SHADOW(o);
#else
	UNITY_TRANSFER_SHADOW(o, 0);
#endif
	COMPUTE_LIGHT_COORDS(o);
    return o;
}

float GetAlpha(v2f i)
{
	return tex2D(_MainTex, i.uv.xy).a;
}

float GetDetailMask(v2f i)
{
	return tex2D(_DetailMask, i.uv.zw);
}

float3 GetAlbedo(v2f i)
{
	float3 albedo = tex2D(_MainTex, i.uv.xy);
#if defined(_ALBEDODETAIL_ON)
	float3 detailAlbedo = tex2D(_DetailTex, i.uv.zw);
	albedo = lerp(albedo, albedo*detailAlbedo*unity_ColorSpaceDouble, GetDetailMask(i));
#endif
	return albedo;
}

float3 GetNormal(v2f i)
{
	float3 normal = float3(0, 1, 0);
#if defined(_NORMAL_ON)
	normal.xy = tex2D(_NormalMap, i.uv).wy * 2 - 1;
	normal.xy *= _BumpScale;
	normal.z = sqrt(1 - saturate(dot(normal.xy, normal.xy)));
	normal = normal.xzy;
#endif
#if defined(_NORMALDETAIL_ON)
	float3 detailNormal;
	detailNormal.xy = tex2D(_DetailNormal, i.uv).wy * 2 - 1;
	detailNormal.xy *= _BumpScale;
	detailNormal.z = sqrt(1 - saturate(dot(detailNormal.xy, detailNormal.xy)));
	detailNormal = lerp(float3(0,1,0), detailNormal.xzy, GetDetailMask(i));

	normal.x = normal.x + detailNormal.x;
	normal.y = normal.y * detailNormal.y;
	normal.z = normal.z + detailNormal.z;
#endif
	normal = normalize(normal);

#define __CALC_WORLDSPACE_NORMAL
#if defined(__CALC_WORLDSPACE_NORMAL)
	float3 wTangent = i.tangentToWorld[0];
	float3 wBinormal = i.tangentToWorld[1];
	float3 wNormal = i.tangentToWorld[2];
	normal = normalize(wTangent*normal.x + wBinormal*normal.y + wNormal*normal.z);
#endif
	return normal;
}

float GetMetallic(v2f i)
{
#if defined(_METALLICMAP_ON)
	return tex2D(_MetallicSmoothnessMap, i.uv.xy).r * _Metallic;
#else
	return _Metallic;
#endif
}

float GetSmoothness(v2f i)
{
#if defined(_SMOOTHNESSMAP_METALLIC) && defined(_METALLICMAP_ON)
	return tex2D(_MetallicSmoothnessMap, i.uv.xy).a * _Smoothness;
#elif defined(_SMOOTHNESSMAP_ALBEDO) 
	return tex2D(_MainTex, i.uv.xy).a * _Smoothness;
#else
	return _Smoothness;
#endif
}

float GetOcclusion(v2f i)
{
	float occlusion = 1;
#if defined(_OCCLUSION_ON)
	occlusion = lerp(1, tex2D(_OcclusionMap, i.uv).g, _OcclusionStregth);
#endif
	return occlusion;
}

half3 BoxProjection(half3 dir, half3 pos, half4 cubemapPos, half3 boxMin, half3 boxMax)
{
	if (cubemapPos.w > 0)
	{
		half3 factors = ((dir > 0 ? boxMax : boxMin) - pos) / dir;
		half realFactor = min(min(factors.x, factors.y), factors.z);
		return dir * realFactor + (pos - cubemapPos);
	}
	return dir;
}

float3 GetEmissive(v2f i)
{
	float3 emissive = 0;
#if defined(_EMISSION)
	emissive = tex2D(_EmissiveMap, i.uv);
#endif
	return emissive;
}

UnityIndirect CreateIndirect(v2f i, half3 viewDir, float smoothness)
{
	float occlusion = GetOcclusion(i);
	UnityIndirect indirect;
#ifdef VERTEXLIGHT_ON
	indirectLight.diffuse = i.vertexLightColor;
#else
	indirect.diffuse = 0;
#endif
	indirect.specular = 0;


#if defined(FORWARD_BASE_PASS) && defined(DEFFERRED_PASS)
	// 叠加环境光
	indirect.diffuse += max(0, ShadeSH9(half4(i.worldNormal, 1)));

	// 开启延迟渲染屏幕空间反射时，不计算环境反
	#if defined(DEFERRED_PASS) && UNITY_ENABLE_REFLECTION_BUFFERS
		indirectLight.specular = 0;

		indirect.diffuse = fixed(0,0,0)
		indirectLight.specular = fixed(0,0,0);
	#else
		half roughness = 1 - smoothness;
		roughness *= 1.7 - 0.7 * roughness;

		half3 reflectDir = reflect(-viewDir, i.worldNormal);
		half3 reflectDir1 = BoxProjection(reflectDir, i.worldPos,
			unity_SpecCube0_ProbePosition,
			unity_SpecCube0_BoxMin,
			unity_SpecCube0_BoxMax);
		half4 envColor = UNITY_SAMPLE_TEXCUBE_LOD(unity_SpecCube0, reflectDir1, roughness*UNITY_SPECCUBE_LOD_STEPS);
		envColor.rgb = DecodeHDR(envColor, unity_SpecCube0_HDR);
		UNITY_BRANCH
		if (unity_SpecCube0_BoxMin.a < 0.9999)
		{
			half3 reflectDir2 = BoxProjection(reflectDir, i.worldPos,
				unity_SpecCube1_ProbePosition,
				unity_SpecCube1_BoxMin,
				unity_SpecCube1_BoxMax);
			half4 envColor2 = UNITY_SAMPLE_TEXCUBE_SAMPLER_LOD(unity_SpecCube1, unity_SpecCube0, reflectDir2, roughness*UNITY_SPECCUBE_LOD_STEPS);
			// DecodeHDR中代码处理了 intensity 逻辑
			envColor2.rgb = DecodeHDR(envColor2, unity_SpecCube1_HDR);
			envColor = lerp(envColor2, envColor, unity_SpecCube0_BoxMin.a);
		}
		//indirect.specular = half3(1, 0, 0);
		// 叠加环境反射
		indirect.specular = envColor.rgb;

		//indirect.diffuse *= occlusion;
		//indirect.specular *= occlusion;
		indirect.diffuse = fixed(1,0,0);
		indirect.specular = fixed(1,0,0);
	#endif
#endif
	return indirect;
}


#if defined(CUSTOM_SHADOWS_ON)
	#if defined(SHADOWS_SCREEN) || defined(SHADOWS_CUBE)
		float4 _ShadowMapTexture_TexelSize;
	#endif

fixed CustomPCF(unityShadowCoord4 shadowCoord, half3 worldPos)
{
	//return UnitySampleShadowmap_PCF3x3Gaussian(shadowCoord, 0);
	half shadow = 0;
	fixed2 texelSize = _ShadowMapTexture_TexelSize.xy;
	half halfUVOffset = 1;
	for (int i = -halfUVOffset; i <= halfUVOffset; ++i)
	{
		for (int j = -halfUVOffset; j <= halfUVOffset; j++)
		{
			fixed4 uv = shadowCoord; 
			uv.xy += texelSize * half2(i, j);
#if defined(SHADOWS_SCREEN)
			shadow += UNITY_SAMPLE_SHADOW(_ShadowMapTexture, uv.xyz);
#elif defined(SHADOWS_DEPTH)
			shadow += UnitySampleShadowmap(uv);
#elif defined(SHADOWS_CUBE)
			shadow += UnitySampleShadowmap(worldPos - _LightPositionRange.xyz);
#endif
			//shadow += LerpShadow(uv);
		}
	}
	shadow /= ((halfUVOffset*2+1)*(halfUVOffset*2+1));
	return shadow;
}

fixed ComputeAttenuation(v2f i)
{
	#if defined(SHADOWS_NATIVE)
		//unityShadowCoord4 shadowCoord = mul(unity_WorldToShadow[0], unityShadowCoord4(i.worldPos, 1));
		unityShadowCoord4 shadowCoord = i._ShadowCoord;
		fixed shadow = CustomPCF(shadowCoord, i.worldPos);

		shadow = _LightShadowData.r + shadow * (1-_LightShadowData.r);
		#if defined(SPOT)
			shadow = (i._LightCoord.z > 0) * UnitySpotCookie(i._LightCoord) * UnitySpotAttenuate(i._LightCoord.xyz) * shadow;
		#elif defined(POINT)
			unityShadowCoord3 lightCoord = mul(unity_WorldToLight, unityShadowCoord4(i.worldPos, 1)).xyz;
			shadow = tex2D(_LightTexture0, dot(lightCoord, lightCoord).rr).r * shadow;
		#endif
		return shadow;
	#else
		unityShadowCoord dist = SAMPLE_DEPTH_TEXTURE(_ShadowMapTexture, shadowCoord.xy);
		// tegra is confused if we use _LightShadowData.x directly
		// with "ambiguous overloaded function reference max(mediump float, float)"
		unityShadowCoord lightShadowDataX = _LightShadowData.x;
		unityShadowCoord threshold = shadowCoord.z;
		return max(dist > threshold, lightShadowDataX);
	#endif
}
#endif

UnityLight CreateLight(v2f i)
{
	UnityLight light;
#if defined(DEFFERRED_PASS)
	light.dir = float3(0, 1, 0);
	light.color = 0;
#else
	#if defined(POINT) || defined(SPOT) || defined(POINT_COOKIE)
		half3 lightVect = _WorldSpaceLightPos0.xyz - i.worldPos;
	#else
		half3 lightVect = _WorldSpaceLightPos0.xyz;
	#endif
		light.dir = normalize(lightVect);

	#if defined(CUSTOM_SHADOWS_ON)
		half attenuation = ComputeAttenuation(i);
	#else
		UNITY_LIGHT_ATTENUATION(attenuation, i, i.worldPos);
	#endif
	light.color = _LightColor0 * attenuation;
#endif
	//light.ndotl = DotClamped(i.worldNormal, light.dir);
	return light;
}

float4 ApplyFog(float color, v2f i)
{
    float viewDistance = length(_WorldSpaceCameraPos - i.worldPos);
#if FOG_DEPTH
	viewDistance = UNITY_Z_0_FAR_FROM_CLIPSPACE(i.worldPos.w);
#endif
    UNITY_CALC_FOG_FACTOR_RAW(viewDistance);
    return lerp(unity_FogColor, color, unityFogFactor);
}

frag_output frag (v2f i)
{
	frag_output output;
	float alpha = GetAlpha(i);
#if defined(_RENDERING_CUTOUT)
	clip(alpha - _Cutoff);
#endif

	fixed4 col; 
#if defined(_RENDERING_FADE) || defined(_RENDERING_TRANSPARENT)
	col.w = alpha;
#else
	col.w = 1;
#endif

	half3 normal = GetNormal(i);
	float metallic = GetMetallic(i);
	float smoothness = GetSmoothness(i);
	half oneMinusReflectivity = unity_ColorSpaceDielectricSpec.a - unity_ColorSpaceDielectricSpec.a * metallic;
    half3 mainTexCol = GetAlbedo(i);
	fixed3 albedo = mainTexCol * oneMinusReflectivity;
#if defined(_RENDERING_TRANSPARENT)
	albedo *= alpha;
	col.w = 1 - oneMinusReflectivity + alpha * oneMinusReflectivity;
#endif
	fixed3 specColor = lerp(unity_ColorSpaceDielectricSpec.rgb, mainTexCol, metallic);
	half3 viewDir = normalize(_WorldSpaceCameraPos.xyz - i.worldPos);
	UnityLight light = CreateLight(i);
	UnityIndirect indirect = CreateIndirect(i, viewDir, smoothness);
	col.xyz = UNITY_BRDF_PBS(albedo, specColor, oneMinusReflectivity, smoothness, normal, viewDir, light, indirect);
	col.xyz += GetEmissive(i);
#if defined(DEFERRED_PASS)
	output.gBuffer0.rgb = albedo;
	output.gBuffer0.a = GetOcclusion(i);
	output.gBuffer1.rgb = specColor;
	output.gBuffer1.a = smoothness;
	output.gBuffer2 = float4(normal * 0.5 + 0.5, 1);
	#if !defined(UNITY_HDR_ON)
		col.rgb = exp2(-col.rgb);
	#endif
	output.gBuffer3 = col;
#else
	//return indirect.specular.rgbr;
	output.color = ApplyFog(col, i);
#endif // DEFFERRED_PASS
    return output;
}

#endif