﻿#ifndef MY_STATICLIGHTING_LIGHTMAPPING_UNITY
#define MY_STATICLIGHTING_LIGHTMAPPING_UNITY

#include "UnityStandardMeta.cginc"

v2f_meta vert_my(VertexInput v)
{
	v2f_meta o;
	//o.pos = UnityObjectToClipPos(v.vertex);
	o.pos = UnityMetaVertexPosition(v.vertex, v.uv1.xy, v.uv2.xy, unity_LightmapST, unity_DynamicLightmapST); 
	o.uv = TexCoords(v);
	return o;
}
 
float4 frag_my(v2f_meta i): SV_Target
{
	UnityMetaInput o;
	UNITY_INITIALIZE_OUTPUT(UnityMetaInput, o);
	o.Albedo = fixed3(0, 0, 0);
	o.Emission = fixed3(1, 0, 1);
	return UnityMetaFragment(o);
}

#endif