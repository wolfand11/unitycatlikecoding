﻿#ifndef MY_STATICLIGHTING_LIGHTMAPPING_TEST
#define MY_STATICLIGHTING_LIGHTMAPPING_TEST

#include "UnityPBSLighting.cginc"
#include "UnityMetaPass.cginc"

struct appdata
{
	float4 vertex : POSITION;
	float2 uv : TEXCOORD0;
	float2 uv1 : TEXCOORD1;
};

struct v2f
{
	float4 pos : SV_POSITION;
	float4 uv : TEXCOORD0;
};

v2f vert(appdata v)
{
	v2f o;
	UNITY_INITIALIZE_OUTPUT(v2f, o);
	o.pos = UnityMetaVertexPosition(v.vertex, v.uv1.xy, v.uv1.xy, unity_LightmapST, unity_LightmapST); 
	//v.vertex.xy = v.uv1 * unity_LightmapST.xy + unity_LightmapST.zw;
	//v.vertex.z = v.vertex.z > 0 ? 0.0001 : 0;
	//o.pos = UnityObjectToClipPos(v.vertex);

	o.uv.xy = v.uv;
	o.uv.zw = v.uv;
	return o;
}

float4 frag(v2f i) : SV_TARGET
{
	UnityMetaInput o;
	o.Emission = float3(1, 0, 1);
	//o.Emission = GetEmissive(i);
	o.Albedo = float3(0, 0, 0);
	//o.Albedo = GetAlbedo(i);
	o.SpecularColor = 0;
	return UnityMetaFragment(o);
}

#endif