﻿Shader "Custom/TestMetaPass"
{
	Properties {
		_Color ("Color", Color)=(1,1,1,1)
		[Enum(UnityEngine.Rendering.CullMode)] _Cull ("Cull Mode", Float) = 1
	}
 
	SubShader 
	{
		Pass
		{
			Name "META"
			Tags {"LightMode"="Meta"}
			Cull Off
			CGPROGRAM
 
			#include"UnityStandardMeta.cginc"
			
			v2f_meta vert_test(VertexInput v)
			{
				v2f_meta o;
				//o.pos = UnityObjectToClipPos(v.vertex);
				o.pos = UnityMetaVertexPosition(v.vertex, v.uv1.xy, v.uv2.xy, unity_LightmapST, unity_DynamicLightmapST); 
				o.uv = TexCoords(v);
				return o;
			}
			
			float4 frag_test(v2f_meta i): SV_Target
			{
				UnityMetaInput o;
				UNITY_INITIALIZE_OUTPUT(UnityMetaInput, o);
				o.Albedo = fixed3(0, 0, 0);
				//o.Emission = fixed3(0, 1, 0);
				o.Emission = fixed3(0, 1, 1);
				return UnityMetaFragment(o);
			}
		   
			#pragma vertex vert_test
			#pragma fragment frag_test
			#pragma shader_feature _EMISSION
			#pragma shader_feature _METALLICGLOSSMAP
			#pragma shader_feature ___ _DETAIL_MULX2
			ENDCG
		}
	   
		Tags {"RenderType"="Opaque"}
		LOD 200
		Pass
		{
			Cull [_Cull]
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"


			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float3 wNormal : TEXCOORD1;
				float4 wPos : TEXCOORD2;

			};

			float4 _Color;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				o.wNormal = UnityObjectToWorldNormal(v.normal);
				o.wPos = mul(unity_ObjectToWorld, v.vertex);
				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				float ndl = dot(WorldSpaceLightDir(i.wPos), normalize(i.wNormal));
				return _Color * ndl + 0.1;
			}
			ENDCG
		} 
	}
}
