﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/MyFirstShader"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Pass
		{
			CGPROGRAM
			struct MVertexInput
			{
				float4 pos : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct VertexToFragment 
			{
				float4 pos : SV_POSITION;
				float3 localPos : TEXCOORD0;
				float2 uv : TEXCOORD1;
			};
			sampler2D _MainTex;
			float4 _MainTex_ST;

			#pragma vertex MyVertexProgram
			#pragma fragment MyFragmentProgram
			
			#include "UnityCG.cginc"

			VertexToFragment MyVertexProgram(MVertexInput a2v)
			{
				VertexToFragment o;
				o.localPos = a2v.pos.xyz;
				o.pos = UnityObjectToClipPos(a2v.pos);
				o.uv = a2v.uv;
				return o;
			}
			
			float4 MyFragmentProgram(VertexToFragment v2f) : SV_TARGET
			{
				//return float4(v2f.localPos + 0.5, 1);
				//return float4(v2f.uv, 1, 1);
				return tex2D(_MainTex, v2f.uv*_MainTex_ST.xy+_MainTex_ST.zw);
			}

			ENDCG
		}
	}
}
