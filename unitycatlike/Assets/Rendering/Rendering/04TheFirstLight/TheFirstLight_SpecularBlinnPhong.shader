﻿Shader "Custom/TheFirstLight_SpecularBlinnPhong"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_Smoothness("Smoothness", Range(0, 1)) = 0.5
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float3 normal : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			fixed _Smoothness;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.normal = mul(v.normal, (float3x3)unity_WorldToObject);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				fixed4 col;
				col.w = 1;
				half3 normal = normalize(i.normal);
				half3 viewDir = normalize(_WorldSpaceCameraPos - i.worldPos);
				half3 lightDir = normalize(_WorldSpaceLightPos0);
				half3 halfDir = normalize(viewDir + lightDir);
				col.xyz = pow(saturate(dot(halfDir, normal)), _Smoothness*100);
				return col;
            }
            ENDCG
        }
    }
}
