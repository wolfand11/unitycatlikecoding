﻿Shader "Custom/MyDeferred"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_Cutoff("Alpha Cutoff", Range(0, 1)) = 0.5
		[Toggle] _Normal("Normals ?", Float) = 0
		[NoScaleOffset] _NormalMap("Normals", 2D) = "bump" {}
		_BumpScale("Bump Scale", Float) = 1

		[Toggle]_MetallicMap("MetallicMap?", Float) = 0
		[NoScaleOffset] _MetallicSmoothnessMap("Metallic(r)Smoothness(a)", 2D) = "white" {}
        [Gamma]_Metallic("Metallic", Range(0, 1)) = 0.5
		[KeywordEnum(None, ALBEDO, METALLIC)] _SmoothnessMap("Smoothness Map Source", Float) = 0
        _Smoothness("Smoothness", Range(0, 1)) = 0.5
		[Toggle(CUSTOM_SAMPLE_SHADOW)] CUSTOM_SAMPLE_SHADOW("SoftShadow", Float) = 0

		[Toggle] _AlbedoDetail("Albedo Detail?", Float) = 0
		_DetailTex("Detail Albedo", 2D) = "gray" {}
		[Toggle] _NormalDetail("Normal Detail?", Float) = 0
		[NoScaleOffset] _DetailNormal("Detail Normals", 2D) = "bump" {}
		_DetailBumpScale("Detail Bump Scale", Float) = 1
		[NoScaleOffset]_DetailMask("DetailMask", 2D) = "white" {}
		[Toggle] _Emissive("Emissive?", Float) = 0
		_EmissiveMap("Emissive Map", 2D) = "black" {}
		[Toggle] _Occlusion("Occlusion?", Float) = 0
		_OcclusionMap("Occlusion Map", 2D) = "white" {}
		_OcclusionStregth("Occlusion Stregth", Range(0, 1)) = 1
		[HideInInspector] _SrcBlend ("_SrcBlend", Float) = 1
		[HideInInspector] _DstBlend ("_DstBlend", Float) = 0	
		[HideInInspector] _ZWrite ("_ZWrite", Float) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" } 
        LOD 100

        Pass
        {
            Tags
            {
                "LightMode" = "ForwardBase"
            }
			Blend [_SrcBlend] [_DstBlend]
			ZWrite [_ZWrite]
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 3.0
            #define FORWARD_BASE_PASS
			#pragma multi_compile _ SHADOWS_SCREEN
			#pragma multi_compile _ CUSTOM_SAMPLE_SHADOW
			#pragma shader_feature _ _RENDERING_CUTOUT _RENDERING_FADE _RENDERING_TRANSPARENT
			#pragma shader_feature _ _NORMAL_ON
			#pragma shader_feature _ _METALLICMAP_ON
			#pragma shader_feature _ _SMOOTHNESSMAP_ALBEDO _SMOOTHNESSMAP_METALLIC
			#pragma shader_feature _ _ALBEDODETAIL_ON
			#pragma shader_feature _ _NORMALDETAIL_ON
			#pragma shader_feature _ _EMISSION
			#pragma shader_feature _ _OCCLUSION_ON

            #include "MyDeferred.cginc"
            ENDCG
        }
        Pass
        {
            Tags
            {
                "LightMode" = "ForwardAdd"
				"SHADOWSUPPORT"="true"
            }
			Blend One One
			ZWrite Off
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 3.0

            #pragma multi_compile DIRECTIONAL
			#pragma multi_compile SHADOWS_SCREEN
			//#pragma multi_compile_fwdadd_fullshadows
			#pragma multi_compile _ CUSTOM_SAMPLE_SHADOW
			#pragma shader_feature _ _RENDERING_CUTOUT _RENDERING_FADE _RENDERING_TRANSPARENT
			#pragma shader_feature _ _NORMAL_ON
			#pragma shader_feature _ _METALLICMAP_ON
			#pragma shader_feature _ _SMOOTHNESSMAP_ALBEDO _SMOOTHNESSMAP_METALLIC
			#pragma shader_feature _ _ALBEDODETAIL_ON
			#pragma shader_feature _ _NORMALDETAIL_ON
			#pragma shader_feature _ _EMISSION
			#pragma shader_feature _ _OCCLUSION_ON

            #include "MyDeferred.cginc"
            ENDCG
        }
		Pass
		{
			Tags
			{
				"LightMode" = "Deferred"
			}
			CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 3.0
			#pragma exclude_renderers nomrt
			#define DEFERRED_PASS

            //#pragma multi_compile DIRECTIONAL
			//#pragma multi_compile SHADOWS_SCREEN
			//#pragma multi_compile_fwdadd_fullshadows
			#pragma multi_compile _ CUSTOM_SAMPLE_SHADOW
			#pragma shader_feature _ _RENDERING_CUTOUT _RENDERING_FADE _RENDERING_TRANSPARENT
			#pragma shader_feature _ _NORMAL_ON
			#pragma shader_feature _ _METALLICMAP_ON
			#pragma shader_feature _ _SMOOTHNESSMAP_ALBEDO _SMOOTHNESSMAP_METALLIC
			#pragma shader_feature _ _ALBEDODETAIL_ON
			#pragma shader_feature _ _NORMALDETAIL_ON
			#pragma shader_feature _ _EMISSION
			#pragma shader_feature _ _OCCLUSION_ON
			#pragma multi_compile _ UNITY_HDR_ON

            #include "MyDeferred.cginc"
            ENDCG

		}
		Pass
		{
			Tags
			{
				"LightMode" = "ShadowCaster"
			}
			//Blend One Zero
			//ZWrite On ZTest LEqual
			CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma shader_feature _ _RENDERING_CUTOUT _RENDERING_FADE _RENDERING_TRANSPARENT
            #include "MyDeferred_Shadow.cginc"
			ENDCG
		}
    }
	CustomEditor "MyTransparencyShaderGUI"
}
