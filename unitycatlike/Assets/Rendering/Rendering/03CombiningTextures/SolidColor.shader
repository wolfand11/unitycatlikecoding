﻿Shader "Unlit/LinearGammaSpaceTest"
{
    Properties
    {
		_Color("Color", Color) = (1,1,1,1)
		_ColorV("ColorV", Vector) = (1,1,1,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
            };

			float4 _Color;
			float4 _ColorV;


            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				if (i.uv.x < 0.5)
				{
					return _Color;
				}
				else
				{
					return _ColorV;
 }
            }
            ENDCG
        }
    }
}
