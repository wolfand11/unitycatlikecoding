#ifndef MY_COMPLEX_SHADOW
#define MY_COMPLEX_SHADOW


#include "UnityCG.cginc"

struct appdata 
{
	half3 pos : POSITION;
	half3 normal : NORMAL;
};

struct v2f 
{
	half4 screenPos : SV_POSITION;
};

v2f vert(appdata i)
{
	v2f o;
	// unity_LightShadowBias.z save normal bias
	o.screenPos = UnityClipSpaceShadowCasterPos(i.pos, i.normal);
	// unity_LightShadowBias.x save bias
	o.screenPos = UnityApplyLinearShadowBias(o.screenPos);
	return o;
}

fixed4 frag(v2f i) : SV_TARGET
{
	return 0;
}

#endif