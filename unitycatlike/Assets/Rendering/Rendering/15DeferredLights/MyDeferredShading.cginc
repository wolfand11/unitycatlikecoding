﻿#ifndef MY_DEFERRED_SHADING
#define MY_DEFERRED_SHADING

#include "UnityPBSLighting.cginc"

struct appdata
{
    float4 vertex : POSITION;
	float3 normal : NORMAL;
};

struct v2f
{
    float4 pos : SV_POSITION;
	float4 uv : TEXCOORD0;
	float3 ray : TEXCOORD1;
};

UNITY_DECLARE_DEPTH_TEXTURE(_CameraDepthTexture);
sampler2D _CameraGBufferTexture0;
sampler2D _CameraGBufferTexture1;
sampler2D _CameraGBufferTexture2;
#if defined(SHADOWS_SCREEN)
sampler2D _ShadowMapTexture;
#endif
#if defined(POINT_COOKIE)
	samplerCUBE _LightTexture0;
#else
	sampler2D _LightTexture0;
#endif
sampler2D _LightTextureB0;
float4x4 unity_WorldToLight;
float4 _LightColor;
float4 _LightDir;
float4 _LightPos;
float _LightAsQuad;

v2f vert (appdata v)
{
    v2f o;
    o.pos = UnityObjectToClipPos(v.vertex);
	o.uv = ComputeScreenPos(o.pos);
#if defined(DIRECTIONAL_COOKIE) || defined(DIRECTIONAL)
	o.ray = v.normal;
#else
	o.ray = UnityObjectToViewPos(v.vertex) * float3(-1, -1, 1);
#endif
    return o;
}

float GetShadowFade(float3 worldPos, float viewZ)
{
	float shadowFadeDistance = UnityComputeShadowFadeDistance(worldPos, viewZ);
	return UnityComputeShadowFade(shadowFadeDistance);
}

float CalcSAtten(float sFade, float3 worldPosOrLightVect)
{
	float sAtten = 1;
#if defined(UNITY_FAST_COHERENT_DYNAMIC_BRANCHING) && defined(SHADOWS_SOFT)
	UNITY_BRANCH
	if (sFade > 0.99)
	{
		sAtten = 1;
	}
	else
	{
		#if defined(SHADOWS_DEPTH)
			sAtten = UnitySampleShadowmap(mul(unity_WorldToShadow[0], float4(worldPosOrLightVect, 1)));
		#elif defined(SHADOWS_CUBE)
			sAtten = UnitySampleShadowmap(-worldPosOrLightVect);
		#endif
	}
#endif
	return sAtten;
}

UnityLight CreateLight(float2 uv, float3 worldPos, float viewZ)
{
	UnityLight light;
	// 光照方向需要取反
	float atten = 1;
	float sAtten = 1;
	float sFade = GetShadowFade(worldPos, viewZ);
	bool shadowed = false;

#if defined(DIRECTIONAL_COOKIE) || defined(DIRECTIONAL)
	light.dir = -_LightDir;
	#if defined(DIRECTIONAL_COOKIE)
		float2 uvCookie = mul(unity_WorldToLight, float4(worldPos, 1)).xy;
		//atten *= tex2D(_LightTexture0, uvCookie).w;
		atten *= tex2Dbias(_LightTexture0, float4(uvCookie, 0, -8)).w;
	#endif 
	#if defined(SHADOWS_SCREEN)
		shadowed = true;
		sAtten = tex2D(_ShadowMapTexture, uv).r;
	#endif
#else
	float3 lightVec = _LightPos.xyz - worldPos;
	float attenUV = dot(lightVec, lightVec) * _LightPos.w;
	atten *= tex2D(_LightTextureB0, attenUV.rr).UNITY_ATTEN_CHANNEL;
	light.dir = normalize(lightVec);
	#if defined(SPOT)
		float4 uvCookie = mul(unity_WorldToLight, float4(worldPos, 1));
		uvCookie.xy /= uvCookie.w;
		atten *= tex2Dbias(_LightTexture0, float4(uvCookie.xy, 0, -8)).w;
		atten *= uvCookie.w < 0;
		#if defined(SHADOWS_DEPTH)
			shadowed = true;
			sAtten = CalcSAtten(sFade, worldPos);
		#endif
	#else
		#if defined(POINT_COOKIE)
			float3 uvCookie = mul(unity_WorldToLight, float4(worldPos, 1)).xyz;
			atten *= texCUBEbias(_LightTexture0, float4(uvCookie, -8)).w;
		#endif
		#if defined(SHADOWS_CUBE)
			shadowed = true;
			sAtten = CalcSAtten(sFade, lightVec);
		#endif
	#endif
#endif
	if (shadowed)
	{
		sAtten = saturate(sAtten + GetShadowFade(worldPos, viewZ));
	}
	light.color = _LightColor.rgb * atten * sAtten;
	return light;
}

fixed4 frag (v2f i) : SV_Target
{
	float2 uv = i.uv.xy / i.uv.w;
	float depth = SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, uv);
	depth = Linear01Depth(depth);
	float3 rayToFarPlane = i.ray * _ProjectionParams.z / i.ray.z;
	float3 viewPos = rayToFarPlane * depth;
	float3 worldPos = mul(unity_CameraToWorld, float4(viewPos, 1)).xyz;
	float3 albedo = tex2D(_CameraGBufferTexture0, uv).rgb;
	float3 specularTint = tex2D(_CameraGBufferTexture1, uv).rgb;
	float smoothness = tex2D(_CameraGBufferTexture1, uv).a;
	float3 normal = tex2D(_CameraGBufferTexture2, uv).rgb * 2 - 1;
	float3 viewDir = normalize(_WorldSpaceCameraPos - worldPos);
	float oneMinusReflectivity = 1 - SpecularStrength(specularTint);

	UnityLight light = CreateLight(uv, worldPos, viewPos.z);
	UnityIndirect indirectLight;
	indirectLight.diffuse = 0;
	indirectLight.specular = 0;
	float4 color = UNITY_BRDF_PBS(albedo, specularTint, oneMinusReflectivity, smoothness,
		normal, viewDir, light, indirectLight);
#if !defined(UNITY_HDR_ON)
	color = exp2(-color);
#endif
	return color;
}
#endif