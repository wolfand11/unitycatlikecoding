﻿Shader "Hidden/Custom/GBloom"
{
    Properties
    {
        //_MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
		// No culling or depth
        Cull Off ZWrite Off ZTest Always

		CGINCLUDE
		#include "UnityCG.cginc"

		struct appdata
		{
			float4 vertex : POSITION;
			float2 uv : TEXCOORD0;
		};

		struct v2f
		{
			float2 uv : TEXCOORD0;
			float4 vertex : SV_POSITION;
		};

		v2f vert (appdata v)
		{
			v2f o;
			o.vertex = float4(v.vertex.xy, 0, 1);
			o.uv = (v.vertex + 1) * 0.5;
#if UNITY_UV_STARTS_AT_TOP
			o.uv= o.uv * float2(1.0, -1.0) + float2(0.0, 1.0);
#endif
			return o;
		}

		sampler2D _MainTex;
		sampler2D _SourceTex;
		float _Threshold, _SoftKnee;
		float4 _MainTex_TexelSize;
		half _Intensity;

		half3 Sample(float2 uv)
		{
			return tex2D(_MainTex, uv).rgb;
		}

		half3 Sample(float2 uv, float delta)
		{
			return tex2D(_MainTex, uv).rgb;
		}

		half3 SampleBox(float2 uv, float delta)
		{
			float4 o = _MainTex_TexelSize.xyxy * float2(-delta, delta).xxyy;
			half3 s = Sample(uv + o.xy) + Sample(uv + o.zy) + Sample(uv + o.xw) + Sample(uv + o.zw);
			return s * 0.25f;
		}
//#define SampleBox Sample

#define USE_SOFT_KNEE
		half3 Prefilter(half3 c)
		{
			half brightness = max(c.r, max(c.g, c.b));
#if defined(USE_SOFT_KNEE)
			half knee = _Threshold * _SoftKnee;
			half soft = brightness - _Threshold + knee;
			soft = clamp(soft, 0, 2 * knee);
			soft = soft * soft / (4 * knee + 0.001);
			half contribution = max(soft, brightness - _Threshold);
#else
			half contribution = max(0, brightness - _Threshold);
#endif
			contribution /= max(brightness, 0.001);
			return c * contribution;
		}

		
		ENDCG
		Pass // 0
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			fixed4 frag (v2f i) : SV_Target
			{
				return half4(Prefilter(SampleBox(i.uv, 1)), 1);
			}
            ENDCG
        }
        Pass // 1
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			fixed4 frag (v2f i) : SV_Target
			{
				return half4(SampleBox(i.uv, 1), 1);
			}
            ENDCG
        }
		Pass // 2
        {
			Blend One One

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            fixed4 frag (v2f i) : SV_Target
			{
				return half4(SampleBox(i.uv, 0.5), 1);
			}
            ENDCG
        }
		Pass // 3
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            fixed4 frag (v2f i) : SV_Target
			{
				half4 c = tex2D(_SourceTex, i.uv);
				c.rgb += _Intensity * SampleBox(i.uv, 0.5);
				return half4(c);
			}
            ENDCG
        }
		Pass // 4
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            fixed4 frag (v2f i) : SV_Target
			{
				return half4(_Intensity * SampleBox(i.uv, 0.5), 1);
			}
            ENDCG
        }
    }
}
