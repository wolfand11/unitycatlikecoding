﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

[Serializable]
[PostProcess(typeof(GBloomRenderer), PostProcessEvent.BeforeStack, "Custom/GBloom")]
public class GBloom : PostProcessEffectSettings
{
    [Range(1, 16)]
    public IntParameter iterations = new IntParameter { value = 1 };
    [Range(0, 10)]
    public FloatParameter threshold = new FloatParameter { value = 1.0f };
    [Range(0, 1)]
    public FloatParameter softKnee = new FloatParameter { value = 0.5f };
    [Range(0, 10)]
    public FloatParameter intensity = new FloatParameter { value = 1.0f };

    public BoolParameter debug = new BoolParameter{ value = false };
    public override bool IsEnabledAndSupported(PostProcessRenderContext context)
    {
        return enabled.value;
    }
}

public class GBloomRenderer : PostProcessEffectRenderer<GBloom>
{
    Shader shader;
    public override void Init()
    {
        shader = Shader.Find("Hidden/Custom/GBloom");
    }

    List<int> rtIDs = new List<int>();
    public override void Render(PostProcessRenderContext context)
    {
        var cmd = context.command;

        if (shader == null) return;
        var sheet = context.propertySheets.Get(shader);
        if (sheet == null) return;

        cmd.BeginSample("GBloomPyramid");
        rtIDs.Clear();

        // test downsampling
#if true
        float scaleFactor = 1.0f / 8;
        var downSamplingRT = context.GetScreenSpaceTemporaryRT(0, 
            context.sourceFormat, 
            RenderTextureReadWrite.Default, 
            (int)(context.screenWidth * scaleFactor), (int)(context.screenHeight*scaleFactor));
        cmd.BlitFullscreenTriangle(context.source, downSamplingRT, sheet, 1);
        cmd.BlitFullscreenTriangle(downSamplingRT, context.destination);
#else

        // downsampling
        int curWidth = context.screenWidth;
        int curHeight = context.screenHeight;
        var lastDown = context.source;
        cmd.SetGlobalFloat("_Threshold", settings.threshold);
        cmd.SetGlobalFloat("_SoftKnee", settings.softKnee);
        cmd.SetGlobalFloat("_Intensity", settings.intensity);
        for(var i=0; i<settings.iterations; i++)
        {
            int curDes = Shader.PropertyToID("_GBloomDownsampling" + i);
            curWidth = curWidth / 2;
            curHeight = curHeight / 2;
            context.GetScreenSpaceTemporaryRT(cmd, curDes, 0, context.sourceFormat, RenderTextureReadWrite.Default, FilterMode.Bilinear, curWidth, curHeight);
            cmd.BlitFullscreenTriangle(lastDown, curDes, sheet, i==0?0:1);
            rtIDs.Add(curDes);

            lastDown= curDes;
        }

        // upsampling
        var lastUp = lastDown;
        for(var i=0; i<settings.iterations-1; i++)
        {
            int curDes = rtIDs[rtIDs.Count-2-i];
            cmd.BlitFullscreenTriangle(lastUp, curDes, sheet, 2);

            lastUp = curDes;
        }
        if(settings.debug)
        {
            cmd.BlitFullscreenTriangle(lastUp, context.destination, sheet, 4);
        }
        else
        {
            cmd.SetGlobalTexture("_SourceTex", context.source);
            cmd.BlitFullscreenTriangle(lastUp, context.destination, sheet, 3);
        }
        
        for(var i=0; i<rtIDs.Count; i++)
        {
            cmd.ReleaseTemporaryRT(rtIDs[i]);
        }
#endif

        cmd.EndSample("GBloomPyramid End");
    }
}
