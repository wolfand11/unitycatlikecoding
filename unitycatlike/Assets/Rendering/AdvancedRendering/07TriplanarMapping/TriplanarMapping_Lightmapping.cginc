﻿#ifndef TRIPLANAR_MAPPING_LIGHTMAPPING
#define TRIPLANAR_MAPPING_LIGHTMAPPING

#include "UnityPBSLighting.cginc"
#include "UnityMetaPass.cginc"

struct appdata
{
	float4 vertex : POSITION;
	float2 uv : TEXCOORD0;
	float2 uv1 : TEXCOORD1;
	float2 uv2 : TEXCOORD2;
	float3 normal : NORMAL;
};

struct v2f
{
	float4 pos : SV_POSITION;
	float4 uv : TEXCOORD0;
	float4 worldPos : TEXCOORD1;
	float3 worldNormal : TEXCOORD2;
};


float4 _Color;
sampler2D _MainTex;
float4 _MainTex_ST;
float _Cutoff;
sampler2D _DetailTex;
float4 _DetailTex_ST;
sampler2D _DetailMask;
float3 _EmissionColor;
sampler2D _EmissiveMap;
fixed _Smoothness;
fixed _Metallic;
sampler2D _MetallicSmoothnessMap;
sampler2D _NormalMap;

float4 GetDefaultUV(v2f i)
{
#if defined(NO_DEFAULT_UV)
	return float4(0, 0, 0, 0);
#else
	return i.uv;
#endif
}

#if !defined(UV_FUNCTION)
#define UV_FUNCTION GetDefaultUV
#endif

#include "TriplanarMapping_Triplanar.cginc"

v2f vert(appdata v)
{
	v2f o;
	UNITY_INITIALIZE_OUTPUT(v2f, o);
	o.pos = UnityMetaVertexPosition(v.vertex, v.uv1.xy, v.uv2.xy, unity_LightmapST, unity_DynamicLightmapST); 

	//v.vertex.xy = v.uv1 * unity_LightmapST.xy + unity_LightmapST.zw;
	//v.vertex.z = v.vertex.z > 0 ? 0.0001 : 0;
	//o.pos = UnityObjectToClipPos(v.vertex);
#if defined(META_PASS_NEEDS_NORMALS)
	o.worldNormal = UnityObjectToWorldNormal(v.normal);
#else
	o.worldNormal = float3(0, 1, 0);
#endif

#if defined(META_PASS_NEEDS_POSITION)
	o.worldPos.xyz = mul(unity_ObjectToWorld, v.vertex);
#else
	o.worldPos.xyz = 0;
#endif

#if !defined(NO_DEFAULT_UV)
	o.uv.xy = TRANSFORM_TEX(v.uv, _MainTex);
	o.uv.zw = TRANSFORM_TEX(v.uv, _DetailTex);
#endif
	return o;
}

float GetAlpha(v2f i)
{
	return tex2D(_MainTex, i.uv.xy).a * _Color.a;
}

float GetDetailMask(v2f i)
{
	return tex2D(_DetailMask, i.uv.zw);
}

float3 GetAlbedo(v2f i)
{
	float3 albedo = tex2D(_MainTex, i.uv.xy);
	albedo *= _Color;
#if defined(_ALBEDODETAIL_ON)
	float3 detailAlbedo = tex2D(_DetailTex, i.uv.zw);
	albedo = lerp(albedo, albedo*detailAlbedo*unity_ColorSpaceDouble, GetDetailMask(i));
#endif
	return albedo;
}
float3 GetEmissive(v2f i)
{
	float3 emissive = 0;
#if defined(_EMISSION)
	emissive = tex2D(_EmissiveMap, i.uv);
	if (dot(emissive, emissive) < 0.001)
	{
		emissive = _EmissionColor;
	}
	else
	{
		emissive = emissive * _EmissionColor;
	}
#endif
	return emissive;
}

float GetMetallic(v2f i)
{
#if defined(_METALLICMAP_ON)
	return tex2D(_MetallicSmoothnessMap, i.uv.xy).r * _Metallic;
#else
	return _Metallic;
#endif
}

float GetSmoothness(v2f i)
{
#if defined(_SMOOTHNESSMAP_METALLIC) && defined(_METALLICMAP_ON)
	return tex2D(_MetallicSmoothnessMap, i.uv.xy).a * _Smoothness;
#elif defined(_SMOOTHNESSMAP_ALBEDO) 
	return tex2D(_MainTex, i.uv.xy).a * _Smoothness;
#else
	return _Smoothness;
#endif
}

#if !defined(ALBEDO_FUNCTION)
#define ALBEDO_FUNCTION GetAlbedo
#endif

float4 frag(v2f i) : SV_TARGET
{
	SurfaceData surface;
	surface.normal = i.worldNormal;
	surface.albedo = 1;
	surface.alpha = 1;
	surface.emission = 0;
	surface.metallic = 0;
	surface.occlusion = 1;
	surface.smoothness = 0.5;

#if defined(SURFACE_FUNCTION)
	SurfaceParameters sp;
	sp.normal = i.worldNormal;
	sp.position = i.worldPos.xyz;
	sp.uv = UV_FUNCTION(i);
	SURFACE_FUNCTION(surface, sp);
#else
	surface.albedo = ALBEDO_FUNCTION(i);
	surface.alpha = GetAlpha(i);
	surface.emission = GetEmissive(i);
	surface.metallic = GetMetallic(i);
	surface.smoothness = GetSmoothness(i);
#endif
	UnityMetaInput o;
	//o.Emission = float3(0, 0, 1);
	o.Emission = surface.emission;
	//o.Albedo = float3(1, 0, 0); 	
	float oneMinusReflectivity;
	o.Albedo = DiffuseAndSpecularFromMetallic(surface.albedo, surface.metallic, o.SpecularColor, oneMinusReflectivity);

	// 越粗糙的表面提供的间接光照会越多
	float roughness = SmoothnessToRoughness(surface.smoothness) * 0.5;
	o.Albedo += o.SpecularColor * roughness;
	//o.Albedo = GetAlbedo(i);
	o.SpecularColor = 0;
	return UnityMetaFragment(o);
}
#endif