﻿Shader "Custom/TriplanarMapping"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		[NoScaleOffset]_MainTex ("Main Texture", 2D) = "white" {}
		[NoScaleOffset]_MOHSMap("MOHS Texture", 2D) = "white" {}
		[Toggle] _Normal("Normals ?", Float) = 0
		[NoScaleOffset] _NormalMap("Normals", 2D) = "bump" {}
		_BumpScale("Bump Scale", Float) = 1

		[NoScaleOffset] _TopMainTex ("Top Albedo", 2D) = "white" {}
		[NoScaleOffset] _TopMOHSMap ("Top MOHS", 2D) = "white" {}
		[NoScaleOffset] _TopNormalMap ("Top Normals", 2D) = "white" {}

		_MapScale("Map Scale", Float) = 1
		_BlendOffset("Blend Offset", Range(0, 0.5)) = 0.25
		_BlendExponent("Blend Exponent", Range(1, 8)) = 2
		_BlendHeightStrength("Blend Height Strength", Range(0, 0.99)) = 0.5
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" } 
		LOD 100

		Pass
		{
			Tags
			{
				"LightMode" = "ForwardBase"
			}
			//Blend [_SrcBlend] [_DstBlend]
			//ZWrite [_ZWrite]
			CGPROGRAM
			#pragma vertex vert
			//#pragma vertex transport_appdata
			#pragma fragment frag

			#pragma target 3.0
			#define FORWARD_BASE_PASS

			#pragma multi_compile_instancing
			//#pragma multi_compile DIRECTIONAL
			#pragma multi_compile _ VERTEXLIGHT_ON LIGHTMAP_ON 
			#pragma multi_compile _ SHADOWS_SHADOWMASK
			#pragma multi_compile _ LIGHTMAP_SHADOW_MIXING
			#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			#pragma multi_compile _ DYNAMICLIGHTMAP_ON 
			#pragma multi_compile _ LOD_FADE_CROSSFADE
			#pragma multi_compile _ SHADOWS_SCREEN
			#pragma multi_compile _ CUSTOM_SAMPLE_SHADOW
			#pragma shader_feature _ _RENDERING_CUTOUT _RENDERING_FADE _RENDERING_TRANSPARENT
			#pragma shader_feature _ _NORMAL_ON
			#pragma shader_feature _ _PARALLAX_ON

			#pragma shader_feature _ _METALLICMAP_ON
			#pragma shader_feature _ _SMOOTHNESSMAP_ALBEDO _SMOOTHNESSMAP_METALLIC
			#pragma shader_feature _ _ALBEDODETAIL_ON
			#pragma shader_feature _ _NORMALDETAIL_ON
			#pragma shader_feature _ _EMISSION
			#pragma shader_feature _ _OCCLUSION_ON

			#include "TriplanarMapping_Input.cginc"
			#include "TriplanarMapping_Triplanar.cginc"
			#include "TriplanarMapping.cginc"
			ENDCG
		}
		Pass
		{
			Tags
			{
				"LightMode" = "ForwardAdd"
				"SHADOWSUPPORT"="true"
			}
			Blend One One
			ZWrite Off
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#pragma multi_compile DIRECTIONAL
			#pragma multi_compile SHADOWS_SCREEN
			#pragma multi_compile _ SHADOWS_SHADOWMASK
			#pragma multi_compile _ LOD_FADE_CROSSFADE
			//#pragma multi_compile_fwdadd_fullshadows
			#pragma multi_compile _ CUSTOM_SAMPLE_SHADOW
			#pragma shader_feature _ _RENDERING_CUTOUT _RENDERING_FADE _RENDERING_TRANSPARENT
			#pragma shader_feature _ _NORMAL_ON
			#pragma shader_feature _ _METALLICMAP_ON
			#pragma shader_feature _ _SMOOTHNESSMAP_ALBEDO _SMOOTHNESSMAP_METALLIC
			#pragma shader_feature _ _ALBEDODETAIL_ON
			#pragma shader_feature _ _NORMALDETAIL_ON
			#pragma shader_feature _ _EMISSION
			#pragma shader_feature _ _OCCLUSION_ON

			#include "TriplanarMapping_Input.cginc"
			#include "TriplanarMapping_Triplanar.cginc"
			#include "TriplanarMapping.cginc"
			ENDCG
		}
		Pass
		{
			Tags
			{
				"LightMode" = "Deferred"
			}
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma exclude_renderers nomrt
			#define DEFERRED_PASS

			#pragma multi_compile_instancing
			//#pragma multi_compile DIRECTIONAL
			//#pragma multi_compile SHADOWS_SCREEN
			//#pragma multi_compile_fwdadd_fullshadows
			#pragma multi_compile _ VERTEXLIGHT_ON LIGHTMAP_ON 
			#pragma multi_compile _ SHADOWS_SHADOWMASK
			#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			#pragma multi_compile _ DYNAMICLIGHTMAP_ON 
			#pragma multi_compile _ LOD_FADE_CROSSFADE
			#pragma multi_compile _ CUSTOM_SAMPLE_SHADOW
			#pragma shader_feature _ _RENDERING_CUTOUT _RENDERING_FADE _RENDERING_TRANSPARENT
			#pragma shader_feature _ _NORMAL_ON
			#pragma shader_feature _ _METALLICMAP_ON
			#pragma shader_feature _ _SMOOTHNESSMAP_ALBEDO _SMOOTHNESSMAP_METALLIC
			#pragma shader_feature _ _ALBEDODETAIL_ON
			#pragma shader_feature _ _NORMALDETAIL_ON
			#pragma shader_feature _ _EMISSION
			#pragma shader_feature _ _OCCLUSION_ON
			#pragma multi_compile _ UNITY_HDR_ON
			#pragma multi_compile _ LIGHTMAP_ON

			#include "TriplanarMapping_Input.cginc"
			#include "TriplanarMapping_Triplanar.cginc"
			#include "TriplanarMapping.cginc"

			ENDCG
		}
		Pass
		{
			Tags
			{
				"LightMode" = "ShadowCaster"
			}
			//Blend One Zero
			//ZWrite On ZTest LEqual
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#pragma target 3.0
			#pragma multi_compile_instancing
			#pragma multi_compile_shadowcaster
			#pragma multi_compile _ LOD_FADE_CROSSFADE
			#pragma shader_feature _ _RENDERING_CUTOUT _RENDERING_FADE _RENDERING_TRANSPARENT
			#pragma shader_feature _ _PARALLAX_ON

			#include "TriplanarMapping_Shadow.cginc"

			ENDCG
		}
		Pass
		{
			Tags
			{
				"LightMode" = "Meta"
			}
			//Blend One Zero
			//ZWrite On ZTest LEqual
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#pragma target 3.0

			#define META_PASS_NEEDS_NORMALS
			#define META_PASS_NEEDS_POSITION
			#include "TriplanarMapping_Surface.cginc"
			#include "TriplanarMapping_Lightmapping.cginc"

			ENDCG
		}
	}
	//CustomEditor "GPUInstancingShaderGUI"
}
