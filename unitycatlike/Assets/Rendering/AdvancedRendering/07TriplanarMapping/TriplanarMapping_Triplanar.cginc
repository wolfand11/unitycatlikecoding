﻿// Upgrade NOTE: upgraded instancing buffer 'InstanceProperties' to new syntax.

#ifndef TRIPLANAR_MAPPING_TRIPLANAR
#define TRIPLANAR_MAPPING_TRIPLANAR

#define NO_DEFAULT_UV

struct TriplanarUV 
{
	float2 x, y, z;
};

sampler _MOHSMap;
half _MapScale;
half _BlendOffset, _BlendExponent, _BlendHeightStrength;

sampler2D _TopMainTex, _TopMOHSMap, _TopNormalMap;

TriplanarUV GetTriplanarUV(SurfaceParameters param)
{
	TriplanarUV triUV;
	float3 p = param.position * _MapScale;
	triUV.x = p.zy;
	triUV.y = p.xz;
	triUV.z = p.xy;
	if (param.normal.x < 0)
	{
		triUV.x.x = -triUV.x.x;
	}
	if (param.normal.y < 0)
	{
		triUV.y.x = -triUV.y.x;
	}
	if (param.normal.z >= 0)
	{
		triUV.z.x = -triUV.z.x;
	}
	return triUV;
}

float3 GetTriplanarWeights(SurfaceParameters param)
{
	float3 triW = abs(param.normal);
	triW = saturate(triW - _BlendOffset);
	triW = pow(triW, _BlendExponent);
	return triW / (triW.x + triW.y + triW.z);
}


float3 GetTriplanarWeights(SurfaceParameters param, float heightX, float heightY, float heightZ)
{
	float3 triW = abs(param.normal);
	triW = saturate(triW - _BlendOffset);
	triW *= lerp(1, float3(heightX, heightY, heightZ), _BlendHeightStrength);
	triW = pow(triW, _BlendExponent);
	return triW / (triW.x + triW.y + triW.z);
}

float3 BlendTriplanarNormal(float3 mappedNormal, float3 surfaceNormal)
{
	float3 n;
	n.xy = mappedNormal.xy + surfaceNormal.xy;
	n.z = mappedNormal.z * surfaceNormal.z;
	return n;
}

void TriplanarSurfaceFunction(inout SurfaceData surface, SurfaceParameters param)
{
	//surface.albedo = param.normal * 0.5 + 0.5;
	TriplanarUV triUV = GetTriplanarUV(param);
	float3 albedoX = tex2D(_MainTex, triUV.x).rgb;
	float3 albedoY = tex2D(_MainTex, triUV.y).rgb;
	float3 albedoZ = tex2D(_MainTex, triUV.z).rgb;

	float4 mohsX = tex2D(_MOHSMap, triUV.x);
	float4 mohsY = tex2D(_MOHSMap, triUV.y);
	float4 mohsZ = tex2D(_MOHSMap, triUV.z);

	float3 tangentNormalX = UnpackNormal(tex2D(_NormalMap, triUV.x));
	float3 tangentNormalY = UnpackNormal(tex2D(_NormalMap, triUV.y));
	float3 tangentNormalZ = UnpackNormal(tex2D(_NormalMap, triUV.z));

	if (param.normal.y > 0)
	{
		albedoY = tex2D(_TopMainTex, triUV.y).rgb;
		mohsY = tex2D(_TopMOHSMap, triUV.y);
		tangentNormalY = UnpackNormal(tex2D(_TopNormalMap, triUV.y));
	}

	float3 triW = GetTriplanarWeights(param, mohsX.z, mohsY.z, mohsZ.z);
	float4 mohs = mohsX*triW.x + mohsY*triW.y + mohsZ*triW.z;

	surface.albedo = albedoX*triW.x + albedoY*triW.y + albedoZ*triW.z;
	surface.metallic = mohs.x;
	surface.occlusion = mohs.y;
	surface.smoothness = mohs.a;

	if (param.normal.x < 0)
	{
		// 消除Mirror
		tangentNormalX.x = -tangentNormalX.x;
		// 纠正Normal UpDir
		//tangentNormalX.z = -tangentNormalX.z;
	}
	if (param.normal.y < 0)
	{
		tangentNormalY.x = -tangentNormalY.x;
		//tangentNormalY.z = -tangentNormalY.z;
	}
	if (param.normal.z >= 0)
	{
		tangentNormalZ.x = -tangentNormalZ.x;
	}
	else
	{
		//tangentNormalZ.z = -tangentNormalZ.z;
	}

	float3 worldNormalX = BlendTriplanarNormal(tangentNormalX, param.normal.zyx).zyx;
	float3 worldNormalY = BlendTriplanarNormal(tangentNormalY, param.normal.xzy).xzy;
	float3 worldNormalZ = BlendTriplanarNormal(tangentNormalZ, param.normal);

	surface.normal = worldNormalX*triW.x + worldNormalY*triW.y + worldNormalZ*triW.z;
	//surface.albedo = surface.normal * 0.5 + 0.5;
}

#define SURFACE_FUNCTION TriplanarSurfaceFunction

#endif