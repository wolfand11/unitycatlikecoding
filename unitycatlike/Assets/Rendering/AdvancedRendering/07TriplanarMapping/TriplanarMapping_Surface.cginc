﻿// Upgrade NOTE: upgraded instancing buffer 'InstanceProperties' to new syntax.
#ifndef TRIPLANAR_MAPPING_SURFACE
#define TRIPLANAR_MAPPING_SURFACE

struct SurfaceData 
{
	float3 albedo, emission, normal;
	float alpha, metallic, occlusion, smoothness;
};

struct SurfaceParameters 
{
	float3 normal, position;
	float4 uv;
};

#endif