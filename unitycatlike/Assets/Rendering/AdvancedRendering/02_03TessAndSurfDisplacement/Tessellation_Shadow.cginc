#ifndef TESSELLATION_SHADOW
#define TESSELLATION_SHADOW

#include "UnityCG.cginc"

#if defined(_RENDERING_FADE) || defined(_RENDERING_TRANSPARENT)
	#define SHADOWS_SEMITRANSPARENT 1
#endif

#if SHADOWS_SEMITRANSPARENT || defined(_RENDERING_CUTOUT)
	#if !defined(_SMOOTHNESS_ALBEDO)
		#define SHADOWS_NEED_UV 1
	#endif
#endif

#if defined(VERTEX_DISPLACEMENT_INSTEAD_OF_PARALLAX)
	#define VERTEX_DISPLACEMENT 1
	#define _DisplacementMap _ParallaxMap
	#define _DisplacementStrength _ParallaxStrength
	#if !defined(SHADOWS_NEED_UV)
		#define SHADOWS_NEED_UV 1
	#endif
#endif

struct appdata 
{
	UNITY_VERTEX_INPUT_INSTANCE_ID
	float4 vertex : POSITION;

#if defined(SHADOWS_NEED_UV)
	float2 uv : TEXCOORD0;
#endif
	float3 normal : NORMAL;
};

struct vert_out 
{
	UNITY_VERTEX_INPUT_INSTANCE_ID
	float4 screenPos: SV_POSITION;

#if defined(SHADOWS_NEED_UV)
	float4 uv : TEXCOORD0;
#endif
#if defined(SHADOWS_CUBE)
	float3 lightVec : TEXCOORD1;
#endif

};

struct frag_in
{
#if SHADOWS_SEMITRANSPARENT || LOD_FADE_CROSSFADE
	UNITY_VPOS_TYPE vpos : VPOS;
#else
	float4 screenPos: SV_POSITION;
#endif

#if defined(SHADOWS_NEED_UV)
	float4 uv : TEXCOORD0;
#endif
#if defined(SHADOWS_CUBE)
	float3 lightVec : TEXCOORD1;
#endif
};

#define VertOut vert_out
#define FragIN frag_in
//#define FragIN vert_out

sampler2D _MainTex;
float4 _MainTex_ST;
float _Cutoff;
sampler3D _DitherMaskLOD;
sampler2D _ParallaxMap;
float _ParallaxStrength;

inline void DisplaceVertex(inout appdata v)
{
#if defined(VERTEX_DISPLACEMENT)
	float displacement = tex2Dlod(_DisplacementMap, float4(v.uv.xy, 0, 0)).g;
	displacement = (displacement - 0.5) * _DisplacementStrength;
	v.vertex.xyz += normalize(v.normal)*displacement;
#endif
}

VertOut vert(appdata i)
{
	VertOut o;
	UNITY_INITIALIZE_OUTPUT(VertOut, o);
	UNITY_SETUP_INSTANCE_ID(i);
#if defined(SHADOWS_NEED_UV)
	o.uv.xy = TRANSFORM_TEX(i.uv, _MainTex);
#endif
	DisplaceVertex(i);

#if defined(SHADOWS_CUBE)
	o.screenPos= UnityObjectToClipPos(i.vertex);
	o.lightVec = mul(unity_ObjectToWorld, i.vertex).xyz - _LightPositionRange.xyz;
#else
	// unity_LightShadowBias.z save normal bias
	o.screenPos = UnityClipSpaceShadowCasterPos(i.vertex, i.normal);
	// unity_LightShadowBias.x save bias
	o.screenPos = UnityApplyLinearShadowBias(o.screenPos);
#endif
	return o;
}

float GetAlpha (FragIN i) 
{
#if defined(LOD_FADE_CROSSFADE)
	UnityApplyDitherCrossFade(i.vpos);
#endif

	float alpha = 1;
#if defined(SHADOWS_NEED_UV)
	alpha = tex2D(_MainTex, i.uv.xy).a;
#endif
	return alpha;
}

fixed4 frag(FragIN i) : SV_TARGET
{
#if defined(LOD_FADE_CROSSFADE)
	UnityApplyDitherCrossFade(i.vpos);
#endif

	float alpha = GetAlpha(i);
#if defined(_RENDERING_CUTOUT)
	clip(alpha - _Cutoff);
#endif
#if SHADOWS_SEMITRANSPARENT
	float dither = tex3D(_DitherMaskLOD, float3(i.vpos.xy*0.25, alpha*0.9375)).a;
	//float dither = tex3D(_DitherMaskLOD, float3(i.screenPos.xy*0.25, alpha*0.9375)).a;
	clip(dither - 0.01);
#endif

#if defined(SHADOWS_CUBE)
	float depth = length(i.lightVec) + unity_LightShadowBias.x;
	depth *= _LightPositionRange.w;
	return UnityEncodeCubeShadowDepth(depth);
#else
	return 0;
#endif
}

#endif