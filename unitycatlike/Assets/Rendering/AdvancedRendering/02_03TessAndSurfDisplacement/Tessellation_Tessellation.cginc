﻿#ifndef TESSELLATION_TESSELLATION
#define TESSELLATION_TESSELLATION

struct TessControlPoint 
{
	float4 vertex : INTERNALTESSPOS;
	float2 uv : TEXCOORD0;
	float3 normal : NORMAL;
#if TESS_TANGENT
	float4 tangent: TANGENT;
#endif
#if TESS_UV1
	float2 uv1 : TEXCOORD1;
#endif
#if TESS_UV2
	float3 uv2 : TEXCOORD2;
#endif
};

TessControlPoint transport_appdata(appdata i)
{
	TessControlPoint tcp;
	tcp.vertex = i.vertex;
	tcp.uv = i.uv;
	tcp.normal = i.normal;
#if TESS_TANGENT
	tcp.tangent = i.tangent;
#endif
#if TESS_UV1
	tcp.uv1 = i.uv1;
#endif
#if TESS_UV2
	tcp.uv2 = i.uv2;
#endif
	return tcp;
}

int _TessellationUniform;
float _TessellationEdgeLength;



[UNITY_domain("tri")]
[UNITY_outputcontrolpoints(3)]
[UNITY_outputtopology("triangle_cw")]
//[UNITY_partitioning("integer")]
[UNITY_partitioning("fractional_odd")]
[UNITY_patchconstantfunc("patch_constant_func")]
TessControlPoint hull_func(InputPatch<TessControlPoint, 3> patch, uint id : SV_OutputControlPointID)
{
	return patch[id];
}

struct TessellationFactors 
{
	float edge[3] : SV_TessFactor;
	float inside : SV_InsideTessFactor;
};

float GetTessEdgeFactor(TessControlPoint cp0, TessControlPoint cp1)
{
#define TESS_EDGE
#if defined(TESS_EDGE)
	//#define USE_WORLDSPACE_CALC_TESS_FACTOR
	#define USE_SCREENSPACE_CALC_TESS_FACTOR
	//#define USE_VIEW_DISTANCE_CALC_TESS_FACTOR

	#if defined(USE_WORLDSPACE_CALC_TESS_FACTOR)
		float3 p0 = mul(unity_ObjectToWorld, float4(cp0.vertex.xyz, 1)).xyz;
		float3 p1 = mul(unity_ObjectToWorld, float4(cp1.vertex.xyz, 1)).xyz;
		float edgeLength = distance(p0, p1);
		return edgeLength / ((_TessellationEdgeLength-4)/100.0);
	#elif defined(USE_SCREENSPACE_CALC_TESS_FACTOR)
		float4 p0 = UnityObjectToClipPos(cp0.vertex);
		float4 p1 = UnityObjectToClipPos(cp1.vertex);
		float edgeLength = distance(p0.xy/p0.w, p1.xy/p1.w);
		return edgeLength * _ScreenParams.y / _TessellationEdgeLength;
	#elif defined(USE_VIEW_DISTANCE_CALC_TESS_FACTOR)
		float3 p0 = mul(unity_ObjectToWorld, float4(cp0.vertex.xyz, 1)).xyz;
		float3 p1 = mul(unity_ObjectToWorld, float4(cp1.vertex.xyz, 1)).xyz;
		float edgeLength = distance(p0, p1);
		float3 edgeCenter = (p0 + p1)*0.5;
		float viewDistance = distance(edgeCenter, _WorldSpaceCameraPos);
		//return edgeLength / (_TessellationEdgeLength * viewDistance);
		return edgeLength * _ScreenParams.y / (_TessellationEdgeLength * viewDistance);
	#endif
#else
	return _TessellationUniform;
#endif
}

bool IsTriangleBelowClipPlane(float3 p0, float3 p1, float3 p2, int planeIdx, float bias)
{
	float4 plane = unity_CameraWorldClipPlanes[planeIdx];
	return dot(float4(p0, 1), plane) < bias && dot(float4(p1, 1), plane) < bias && dot(float4(p2, 1), plane) < bias;
}

bool IsTriangleCulled(float3 p0, float3 p1, float3 p2, float bias)
{
	return 
		IsTriangleBelowClipPlane(p0, p1, p2, 0, bias) || 
		IsTriangleBelowClipPlane(p0, p1, p2, 1, bias) || 
		IsTriangleBelowClipPlane(p0, p1, p2, 2, bias) || 
		IsTriangleBelowClipPlane(p0, p1, p2, 3, bias);
}

TessellationFactors patch_constant_func(InputPatch<TessControlPoint, 3> patch)
{
	float3 p0 = mul(unity_ObjectToWorld, float4(patch[0].vertex.xyz, 1)).xyz;
	float3 p1 = mul(unity_ObjectToWorld, float4(patch[1].vertex.xyz, 1)).xyz;
	float3 p2 = mul(unity_ObjectToWorld, float4(patch[2].vertex.xyz, 1)).xyz;

	TessellationFactors f;
	float bias = 0;
#if VERTEX_DISPLACEMENT
	bias = -0.5 * _DisplacementStrength;
#endif
	if (IsTriangleCulled(p0, p1, p2, bias))
	{
		f.edge[0] = f.edge[1] = f.edge[2] = f.inside = 0;
	}
	else
	{
		f.edge[0] = GetTessEdgeFactor(patch[1], patch[2]);
		f.edge[1] = GetTessEdgeFactor(patch[2], patch[0]);
		f.edge[2] = GetTessEdgeFactor(patch[0], patch[1]);
		f.inside = (f.edge[0] + f.edge[1] + f.edge[2]) / 3.0;
	}
	return f;
}

#define DOMAIN_FUNC_INTERPOLATE(fieldName) data.fieldName = \
	patch[0].fieldName * barycentricCoord.x + \
	patch[1].fieldName * barycentricCoord.y + \
	patch[2].fieldName * barycentricCoord.z;


[UNITY_domain("tri")]
vert_out domain_func(TessellationFactors factors, OutputPatch<TessControlPoint, 3> patch, float3 barycentricCoord : SV_DomainLocation)
{
	TessControlPoint data;
	DOMAIN_FUNC_INTERPOLATE(vertex);
	DOMAIN_FUNC_INTERPOLATE(normal);
#if TESS_TANGENT
	DOMAIN_FUNC_INTERPOLATE(tangent);
#endif
	DOMAIN_FUNC_INTERPOLATE(uv);
#if TESS_UV1
	DOMAIN_FUNC_INTERPOLATE(uv1);
#endif
#if TESS_UV2
	DOMAIN_FUNC_INTERPOLATE(uv2);
#endif
	return vert(data);
}
#endif // TESSELLATION_TESSELLATION



