﻿// Upgrade NOTE: upgraded instancing buffer 'InstanceProperties' to new syntax.

#ifndef FLAT_INPUT
#define FLAT_INPUT

#include "UnityPBSLighting.cginc"
#include "AutoLight.cginc"

#if defined(SHADOWS_SCREEN) || defined(SHADOWS_DEPTH) || defined(SHADOWS_CUBE)
	#if defined(CUSTOM_SAMPLE_SHADOW) && defined(UNITY_NO_SCREENSPACE_SHADOWS)
		#define CUSTOM_SHADOWS_ON
	#endif
#endif
#if !defined(LIGHTMAP_ON) && defined(SHADOW_SCREEN)
	#if defined(SHADOWS_SHADOWMASK) && !defined(UNITY_NO_SCREENSPACE_SHADOWS)
//		#define ADDITIONAL_MASKED_DIRECTIONAL_SHADOWS 1
	#endif
#endif

#if defined(LIGHTMAP_ON) && defined(SHADOWS_SCREEN)
	#if defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK)
		#define SUBTRACTIVE_LIGHTING 1
	#endif
#endif

#if defined(WIREFRAME_SHADING)
	#define BARYCENTRIC_COORDINATE \
		float2 barycentricCoordinate : TEXCOORD11;
#else
	#define BARYCENTRIC_COORDINATE
#endif

#if !defined(GET_ALBEDO)
	#define GET_ALBEDO GetAlbedo
#endif

struct appdata
{
	UNITY_VERTEX_INPUT_INSTANCE_ID
	float4 vertex : POSITION;
	float2 uv : TEXCOORD0;
	float3 normal : NORMAL;
	float4 tangent: TANGENT;
	float2 uv1 : TEXCOORD1;
	float3 uv2 : TEXCOORD2;

};

struct vert_out
{
	UNITY_VERTEX_INPUT_INSTANCE_ID
	float4 uv : TEXCOORD0;
	float4 pos : SV_POSITION;
	float3 worldNormal : TEXCOORD1;
	float3 tangentToWorld[3] : TEXCOORD2;
	float3 worldPos : TEXCOORD5;
	UNITY_SHADOW_COORDS(6)
	DECLARE_LIGHT_COORDS(7)
#ifdef VERTEXLIGHT_ON
	float3 vertexLightColor : TEXCOORD8;
#elif defined(LIGHTMAP_ON) || ADDITIONAL_MASKED_DIRECTIONAL_SHADOWS
	float2 lightmapUV : TEXCOORD8;
#endif
#if defined(DYNAMICLIGHTMAP_ON)
	float2 dynamicLightmapUV : TEXCOORD9;
#endif
#if defined(_PARALLAX_ON)
	float3 tangentViewDir : TEXCOORD10;
#endif
};

struct frag_in
{
	UNITY_VERTEX_INPUT_INSTANCE_ID
	float4 uv : TEXCOORD0;
#if defined(LOD_FADE_CROSSFADE)
	UNITY_VPOS_TYPE vpos : VPOS;
#else
	float4 pos : SV_POSITION;
#endif
	float3 worldNormal : TEXCOORD1;
	float3 tangentToWorld[3] : TEXCOORD2;
	float3 worldPos : TEXCOORD5;
	UNITY_SHADOW_COORDS(6)
	DECLARE_LIGHT_COORDS(7)
#ifdef VERTEXLIGHT_ON
	float3 vertexLightColor : TEXCOORD8;
#elif defined(LIGHTMAP_ON) || ADDITIONAL_MASKED_DIRECTIONAL_SHADOWS
	float2 lightmapUV : TEXCOORD8;
#endif
#if defined(DYNAMICLIGHTMAP_ON)
	float2 dynamicLightmapUV : TEXCOORD9;
#endif
#if defined(_PARALLAX_ON)
	float3 tangentViewDir : TEXCOORD10;
#endif
	BARYCENTRIC_COORDINATE
};

struct frag_output
{
#if defined(DEFERRED_PASS)
	float4 gBuffer0 : SV_Target0;
	float4 gBuffer1 : SV_Target1;
	float4 gBuffer2 : SV_Target2;
	float4 gBuffer3 : SV_Target3;
	#if defined(SHADOWS_SHADOWMASK) && (UNITY_ALLOWED_MRT_COUNT>4)
		float4 gBuffer4 : SV_Target4;
	#endif
#else
	float4 color : SV_Target;
#endif
};

UNITY_INSTANCING_BUFFER_START(InstanceProperties)
//float4 _Color;
	UNITY_DEFINE_INSTANCED_PROP(float4, _Color)
#define _Color_arr InstanceProperties
UNITY_INSTANCING_BUFFER_END(InstanceProperties)

sampler2D _MainTex;
float4 _MainTex_ST;
float _Cutoff;
sampler2D _NormalMap;
float _BumpScale;
sampler2D _ParallaxMap;
float _ParallaxStrength;
fixed _Smoothness;
fixed _Metallic;
sampler2D _MetallicSmoothnessMap;
sampler2D _DetailTex;
float4 _DetailTex_ST;
sampler2D _DetailNormal;
sampler2D _DetailMask;
float3 _EmissionColor;
sampler2D _EmissiveMap;
sampler2D _OcclusionMap;
half _OcclusionStregth;

#if defined(WIREFRAME_SHADING)
float3 _WireframeColor;
half _WireframeSmoothing; 
half _WireframeThickness;
#endif

float GetParallaxHeight(float2 uv)
{
	return tex2D(_ParallaxMap, uv).g;
}

float GetAlpha(frag_in i)
{
	float alpha = UNITY_ACCESS_INSTANCED_PROP(InstanceProperties, _Color).a;
	return tex2D(_MainTex, i.uv.xy).a * alpha;
}

float GetDetailMask(frag_in i)
{
	return tex2D(_DetailMask, i.uv.zw);
}

float3 GetAlbedo(frag_in i)
{
	float3 albedo = tex2D(_MainTex, i.uv.xy);
	albedo *= UNITY_ACCESS_INSTANCED_PROP(InstanceProperties, _Color).rgb;
#if defined(_ALBEDODETAIL_ON)
	float3 detailAlbedo = tex2D(_DetailTex, i.uv.zw);
	albedo = lerp(albedo, albedo*detailAlbedo*unity_ColorSpaceDouble, GetDetailMask(i));
#endif
	return albedo;
}

float3 GetNormal(frag_in i)
{
	float3 normal = float3(0, 1, 0);
#if defined(_NORMAL_ON)
	normal.xy = tex2D(_NormalMap, i.uv).wy * 2 - 1;
	normal.xy *= _BumpScale;
	normal.z = sqrt(1 - saturate(dot(normal.xy, normal.xy)));
	normal = normal.xzy;
#endif
#if defined(_NORMALDETAIL_ON)
	float3 detailNormal;
	detailNormal.xy = tex2D(_DetailNormal, i.uv).wy * 2 - 1;
	detailNormal.xy *= _BumpScale;
	detailNormal.z = sqrt(1 - saturate(dot(detailNormal.xy, detailNormal.xy)));
	detailNormal = lerp(float3(0,1,0), detailNormal.xzy, GetDetailMask(i));

	normal.x = normal.x + detailNormal.x;
	normal.y = normal.y * detailNormal.y;
	normal.z = normal.z + detailNormal.z;
#endif
	normal = normalize(normal);

#define __CALC_WORLDSPACE_NORMAL
#if defined(__CALC_WORLDSPACE_NORMAL)
	float3 wTangent = i.tangentToWorld[0];
	float3 wBinormal = i.tangentToWorld[1];
	float3 wNormal = i.tangentToWorld[2];
	normal = normalize(wTangent*normal.x + wNormal*normal.y + wBinormal*normal.z);
#endif
	return normal;
}

float GetMetallic(frag_in i)
{
#if defined(_METALLICMAP_ON)
	return tex2D(_MetallicSmoothnessMap, i.uv.xy).r * _Metallic;
#else
	return _Metallic;
#endif
}

float GetSmoothness(frag_in i)
{
#if defined(_SMOOTHNESSMAP_METALLIC) && defined(_METALLICMAP_ON)
	return tex2D(_MetallicSmoothnessMap, i.uv.xy).a * _Smoothness;
#elif defined(_SMOOTHNESSMAP_ALBEDO) 
	return tex2D(_MainTex, i.uv.xy).a * _Smoothness;
#else
	return _Smoothness;
#endif
}

float GetOcclusion(frag_in i)
{
	float occlusion = 1;
#if defined(_OCCLUSION_ON)
	occlusion = lerp(1, tex2D(_OcclusionMap, i.uv).g, _OcclusionStregth);
#endif
	return occlusion;
}

float3 GetEmissive(frag_in i)
{
	float3 emissive = 0;
#if defined(_EMISSION)
	emissive = tex2D(_EmissiveMap, i.uv);
	if (dot(emissive, emissive) < 0.001)
	{
		emissive = _EmissionColor;
	}
	else
	{
		emissive *= _EmissionColor;
	}
#endif
	return emissive;
}

float GetShadowFade(frag_in i, float atten)
{
#if HANDLE_SHADOWS_BLENDING_IN_GI || ADDITIONAL_MASKED_DIRECTIONAL_SHADOWS
	#if ADDITIONAL_MASKED_DIRECTIONAL_SHADOWS
	atten = SHADOW_ATTENUATION(i);
	#endif
	// TODO: why
	float viewZ = dot(_WorldSpaceCameraPos - i.worldPos, UNITY_MATRIX_V[2].xyz);
	float shadowFadeDistance = UnityComputeShadowFadeDistance(i.worldPos, viewZ);
	float sFade = UnityComputeShadowFade(shadowFadeDistance);
	float bakedAtten = UnitySampleBakedOcclusion(i.lightmapUV, i.worldPos);
	return UnityMixRealtimeAndBakedShadows(atten, bakedAtten, sFade);

	// TODO: why atten + sFade
	//return saturate(atten + sFade);
#else
	return atten;
#endif
}

#endif