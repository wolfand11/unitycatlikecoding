﻿#ifndef FLAT_GEOMETRY
#define FLAT_GEOMETRY

struct geom_out 
{
	vert_out data;
	BARYCENTRIC_COORDINATE
};

#if defined(WIREFRAME_SHADING)
	#define GEOM_OUT geom_out
#else
	#define GEOM_OUT vert_out
#endif

float3 GetAlbedoWithWireFrame(frag_in i)
{
	float3 albedo = GetAlbedo(i);
#if defined(WIREFRAME_SHADING)
	float3 barycentricCoord;
	barycentricCoord.xy = i.barycentricCoordinate.xy;
	barycentricCoord.z = 1 - barycentricCoord.x - barycentricCoord.y;

	float3 deltas = fwidth(barycentricCoord);
	float3 smoothing = deltas * _WireframeSmoothing;
	float3 thickness = deltas * _WireframeThickness;
	barycentricCoord = smoothstep(thickness, thickness+smoothing, barycentricCoord);
	float minBary = min(barycentricCoord.x, min(barycentricCoord.y, barycentricCoord.z));
	albedo = lerp(_WireframeColor, albedo, minBary);
#endif
	return albedo;
}
#define GET_ALBEDO GetAlbedoWithWireFrame

// 注意：TODO GPU会将原始的三角形拆成多个更小的三角形
[maxvertexcount(3)]
void geometry_func(triangle vert_out i[3], inout TriangleStream<GEOM_OUT> stream)
{
	GEOM_OUT g0;
	GEOM_OUT g1;
	GEOM_OUT g2;
#if defined(FLAT_SHADING)
	#if !defined(USE_DDX_DDY_CALC_TRIANGLE_NORMAL)
		float3 p0 = i[0].worldPos.xyz;
		float3 p1 = i[1].worldPos.xyz;
		float3 p2 = i[2].worldPos.xyz;
		float3 triangleNormal = normalize(cross(p1 - p0, p2 - p0));
		i[0].worldNormal = triangleNormal;
		i[1].worldNormal = triangleNormal;
		i[2].worldNormal = triangleNormal;
		i[0].tangentToWorld[2] = triangleNormal;
		i[1].tangentToWorld[2] = triangleNormal;
		i[2].tangentToWorld[2] = triangleNormal;
		#if defined(WIREFRAME_SHADING)
			g0.data = i[0];
			g1.data = i[1];
			g2.data = i[2];
			g0.barycentricCoordinate = float2(1, 0);
			g1.barycentricCoordinate = float2(0, 1);
			g2.barycentricCoordinate = float2(0, 0);
		#endif
	#endif
#endif
#if !defined(WIREFRAME_SHADING)
	g0 = i[0];
	g1 = i[1];
	g2 = i[2];
#endif
	stream.Append(g0);
	stream.Append(g1);
	stream.Append(g2);
}

#endif