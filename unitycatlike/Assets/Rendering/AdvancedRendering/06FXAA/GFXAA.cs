﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

[Serializable]
[PostProcess(typeof(GFXAARenderer), PostProcessEvent.BeforeStack, "Custom/GFXAA")]
public class GFXAA : PostProcessEffectSettings
{
    public enum LuminanceMode
    {
        Alpha,
        Green,
        Calculate
    }

    public enum DebugMode
    {
        None,
        Point,
        Edge,
    }

    [Serializable]
    public class LuminanceModeParameter : ParameterOverride<LuminanceMode> { }
    [Serializable]
    public class DebugModeParameter : ParameterOverride<DebugMode> { }
    public LuminanceModeParameter luminanceMode = new LuminanceModeParameter { value = LuminanceMode.Green };
    public DebugModeParameter debug = new DebugModeParameter{ value = DebugMode.None};
    [Range(0.0312f, 0.0833f)]
    public FloatParameter contrastThreshold = new FloatParameter { value = 0.0312f };
    [Range(0.063f, 0.333f)]
    public FloatParameter relativeThreshold = new FloatParameter { value = 0.333f };
    [Range(0f, 1f)]
    public FloatParameter subpixelBlending = new FloatParameter { value = 1.0f };
    public BoolParameter lowQuality = new BoolParameter { value = true };
}

public class GFXAARenderer : PostProcessEffectRenderer<GFXAA>
{
    Shader shader;
    public override void Init()
    {
        shader = Shader.Find("Hidden/Custom/GFXAA");
    }

    public override void Render(PostProcessRenderContext context)
    {
        var cmd = context.command;

        if (shader == null) return;
        var sheet = context.propertySheets.Get(shader);
        if (sheet == null) return;

        sheet.ClearKeywords();
        if(settings.luminanceMode == GFXAA.LuminanceMode.Green)
        {
            sheet.EnableKeyword("LUMINANCE_GREEN");
        }
        else if(settings.luminanceMode == GFXAA.LuminanceMode.Alpha)
        {
            sheet.EnableKeyword("LUMINANCE_ALPHA");
        }
        else
        {
            sheet.EnableKeyword("LUMINANCE_CALCULATE");
        }
        if(settings.lowQuality)
        {
            sheet.EnableKeyword("LOW_QUALITY");
        }
        sheet.properties.SetFloat("_DEBUG", (int)settings.debug.value);
        sheet.properties.SetFloat("_ContrastThreshold", settings.contrastThreshold);
        sheet.properties.SetFloat("_RelativeThreshold", settings.relativeThreshold);
        sheet.properties.SetFloat("_SubpixelBlending", settings.subpixelBlending);


        cmd.BeginSample("GFXAA");
        cmd.BlitFullscreenTriangle(context.source, context.destination, sheet, 0);
        cmd.EndSample("GFXAA-End");
    }
}
