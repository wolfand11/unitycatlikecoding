﻿Shader "Hidden/Custom/GFXAA"
{
    Properties
    {
        //_MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
		// No culling or depth
        Cull Off ZWrite Off ZTest Always

		CGINCLUDE
		#include "UnityCG.cginc"

		struct appdata
		{
			float4 vertex : POSITION;
			float2 uv : TEXCOORD0;
		};

		struct v2f
		{
			float2 uv : TEXCOORD0;
			float4 vertex : SV_POSITION;
		};

		struct LuminanceData 
		{
			float m, n, e, s, w;
			float ne, nw, se, sw;
			float highest, lowest, contrast;
		};

		struct EdgeData
		{
			bool isHorizontal;
			float pixelStep;
			float oppositeLuminance, gradient;
		};

		v2f vert (appdata v)
		{
			v2f o;
			o.vertex = float4(v.vertex.xy, 0, 1);
			o.uv = (v.vertex + 1) * 0.5;
#if UNITY_UV_STARTS_AT_TOP
			o.uv= o.uv * float2(1.0, -1.0) + float2(0.0, 1.0);
#endif
			return o;
		}

		sampler2D _MainTex;
		float4 _MainTex_TexelSize;
		float _DEBUG;
		half _ContrastThreshold;
		half _RelativeThreshold;
		half _SubpixelBlending;

		half4 Sample(float2 uv)
		{
			return tex2Dlod(_MainTex, float4(uv, 0, 0));
		}

		float SampleLuminance(float2 uv)
		{
#if defined(LUMINANCE_GREEN)
			return saturate(Sample(uv).g);
#elif defined(LUMINANCE_ALPHA)
			return saturate(Sample(uv).a);
#else
			return LinearRgbToLuminance(saturate(Sample(uv)));
#endif
		}

		float SampleLuminance(float2 uv, float uOffset, float vOffset)
		{
			uv += _MainTex_TexelSize * float2(uOffset, vOffset);
			return SampleLuminance(uv);
		}

		LuminanceData SampleLuminanceNeighborhood(float2 uv)
		{
			LuminanceData l;
			l.m = SampleLuminance(uv);
			l.n = SampleLuminance(uv, 0, 1);
			l.e = SampleLuminance(uv, 1, 0);
			l.s = SampleLuminance(uv, 0, -1);
			l.w = SampleLuminance(uv, -1, 0);
			l.ne = SampleLuminance(uv, 1, 1);
			l.nw = SampleLuminance(uv, -1, 1);
			l.se = SampleLuminance(uv, 1, -1);
			l.sw = SampleLuminance(uv, -1, -1);

			l.highest = max(max(max(max(l.n, l.e), l.s), l.w), l.m);
			l.lowest = min(min(min(min(l.n, l.e), l.s), l.w), l.m);
			l.contrast = l.highest - l.lowest;
			return l;
		}

		bool ShouldSkipPixel(LuminanceData l)
		{
			return l.contrast < max(_RelativeThreshold*l.highest, _ContrastThreshold);
		}

		float DeterminePixelBlendFactor(LuminanceData l)
		{
			float filter = 2 * (l.n + l.e + l.s + l.w);
			filter += l.ne + l.nw + l.se + l.sw;
			filter *= 1.0 / 12;
			filter = abs(filter - l.m);
			filter = saturate(filter / l.contrast);
			float blendFactor = smoothstep(0, 1, filter);
			return blendFactor * blendFactor * _SubpixelBlending;
		}
		
		EdgeData DetermineEdge(LuminanceData l)
		{
			EdgeData e;
			float horizontal =
				abs(l.n + l.s - 2 * l.m) * 2 +
				abs(l.ne + l.se - 2 * l.e) +
				abs(l.nw + l.sw - 2 * l.w);
			float vertical =
				abs(l.e + l.w - 2 * l.m) * 2 +
				abs(l.ne + l.nw - 2 * l.n) +
				abs(l.se + l.sw - 2 * l.s);
			e.isHorizontal = horizontal >= vertical;
			e.pixelStep = e.isHorizontal ? _MainTex_TexelSize.y : _MainTex_TexelSize.x;
			float pLuminance = e.isHorizontal ? l.n : l.e;
			float nLuminance = e.isHorizontal ? l.s : l.w;
			float pGradient = abs(pLuminance - l.m);
			float nGradient = abs(nLuminance - l.m);
			if (pGradient < nGradient)
			{
				e.pixelStep = -e.pixelStep;
				e.oppositeLuminance = nLuminance;
				e.gradient = nGradient;
			}
			else
			{
				e.oppositeLuminance = pLuminance;
				e.gradient = pGradient;

			}
			return e;
		}
#if defined(LOW_QUALITY)
	#define EDGE_STEP_COUNT 4
	#define EDGE_STEPS 1, 1.5, 2, 4
	#define EDGE_GUESS 12
#else
	#define EDGE_STEP_COUNT 10
	#define EDGE_STEPS 1, 1.5, 2, 2, 2, 2, 2, 2, 2, 4
	#define EDGE_GUESS 8
#endif
		static const float edgeSteps[EDGE_STEP_COUNT] = { EDGE_STEPS };

		float DetermineEdgeBlendFactor(LuminanceData l, EdgeData e, float2 uv)
		{
			float2 uvEdge = uv;
			float2 edgeStep;
			if (e.isHorizontal)
			{
				uvEdge.y += e.pixelStep * 0.5;
				edgeStep = float2(_MainTex_TexelSize.x, 0);
			}
			else
			{
				uvEdge.x += e.pixelStep * 0.5;
				edgeStep = float2(0, _MainTex_TexelSize.y);
			}
			float edgeLuminance = (l.m + e.oppositeLuminance) * 0.5;
			if (_DEBUG == 6)
			{
				return edgeLuminance;
			}
			float gradientThreshold = e.gradient * 0.25;

			float2 puv = uvEdge + edgeStep * edgeSteps[0];
			float pLuminanceDelta = SampleLuminance(puv) - edgeLuminance;
			bool pAtEnd = abs(pLuminanceDelta) >= gradientThreshold;

			int i=0;
			UNITY_UNROLL
			for (i = 1; i < EDGE_STEP_COUNT && !pAtEnd; i++)
			{
				puv += edgeStep * edgeSteps[i];
				pLuminanceDelta = SampleLuminance(puv) - edgeLuminance;
				pAtEnd = abs(pLuminanceDelta) >= gradientThreshold;
			}
			if (!pAtEnd)
			{
				puv += edgeStep * EDGE_GUESS;
			}

			float2 nuv = uvEdge - edgeStep * edgeSteps[0];
			float nLuminanceDelta = SampleLuminance(nuv) - edgeLuminance;
			bool nAtEnd = abs(nLuminanceDelta) >= gradientThreshold;
			UNITY_UNROLL
			for (i = 1; i < EDGE_STEP_COUNT && !nAtEnd; i++)
			{
				nuv -= edgeStep * edgeSteps[i];
				nLuminanceDelta = SampleLuminance(nuv) - edgeLuminance;
				nAtEnd = abs(nLuminanceDelta) >= gradientThreshold;
			}
			if (!nAtEnd)
			{
				nuv -= edgeStep * EDGE_GUESS;
			}

			float pDistance, nDistance;
			if (e.isHorizontal)
			{
				pDistance = puv.x - uv.x;
				nDistance = uv.x - nuv.x;
			}
			else
			{
				pDistance = puv.y - uv.y;
				nDistance = uv.y - nuv.y;
			}

			float shortestDistance;
			bool deltaSign;
			if (pDistance <= nDistance)
			{
				shortestDistance = pDistance;
				deltaSign = pLuminanceDelta >= 0;
			}
			else
			{
				shortestDistance = nDistance;
				deltaSign = nLuminanceDelta >= 0;
			}
			//return shortestDistance*10;

			if (deltaSign == (l.m >= edgeLuminance))
			{
				return 0;
			}
			return 0.5 - shortestDistance/(pDistance+nDistance);
		}

		float4 ApplyFXAA(float2 uv)
		{
			LuminanceData l = SampleLuminanceNeighborhood(uv);
			EdgeData e = DetermineEdge(l);
			
			if (ShouldSkipPixel(l))
			{
				return Sample(uv);
			}

			float pixelBlend = DeterminePixelBlendFactor(l);
			float edgeBlend = DetermineEdgeBlendFactor(l, e, uv);

			if (_DEBUG > 0)
			{
				float4 color = 0;
				if (_DEBUG == 1)
				{
					color = l.contrast;

					if (l.contrast < _ContrastThreshold)
					{
						color.r = 1;
						color.b = 0;
					}
					if (l.contrast < _RelativeThreshold * l.highest)
					{
						color.g = 1;
						color.b = 0;
					}
				}
				else if (_DEBUG == 2)
				{
					if (edgeBlend > pixelBlend)
					{
						if (e.isHorizontal)
						{
							color.r = 1;
						}
						else
						{
							color.g = 1;
						}
					}
				}
				else if (_DEBUG == 3)
				{
					if (pixelBlend > edgeBlend)
					{
						color.r = 1;
					}
					else
					{
						color.g = 1;
					}
				}
				else if (_DEBUG == 4)
				{
					//if (edgeBlend > pixelBlend)
					{
						if (e.pixelStep > 0)
						{
							color.r = e.oppositeLuminance;
						}
						else
						{
							color.g = e.oppositeLuminance;
						}
					}
				}
				else if (_DEBUG == 5)
				{
					//if (edgeBlend > pixelBlend)
					{
						color.r = e.gradient;
					}
				}
				else if (_DEBUG == 6)
				{
					color.r = edgeBlend;
				}
				return color;
			}
			else
			{
				
				
				float finalBlend = max(pixelBlend, edgeBlend);
				if (e.isHorizontal)
				{
					uv.y += e.pixelStep * finalBlend;
				}
				else
				{
					uv.x += e.pixelStep * finalBlend;
				}
				return float4(Sample(uv).rgb, l.m);
			}
		}
				
		ENDCG
		Pass
        {
			Name "FXAA"
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			#pragma multi_compile LUMINANCE_ALPHA LUMINANCE_GREEN LUMINANCE_CALCULATE
			#pragma multi_compile _ LOW_QUALITY
			#pragma multi_compile _ GAMMA_BLENDING

			fixed4 frag (v2f i) : SV_Target
			{
				float4 col = ApplyFXAA(i.uv);

				#if defined(GAMMA_BLENDING)
				col.rgb = GammaToLinearSpace(col.rgb);
				#endif
				return col;
			}
            ENDCG
        }
    }
}
