﻿Shader "Hidden/Custom/GDepthOfField"
{
    Properties
    {
        //_MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
		// No culling or depth
        Cull Off ZWrite Off ZTest Always

		CGINCLUDE
		#include "UnityCG.cginc"

		struct appdata
		{
			float4 vertex : POSITION;
			float2 uv : TEXCOORD0;
		};

		struct v2f
		{
			float2 uv : TEXCOORD0;
			float4 vertex : SV_POSITION;
		};

		v2f vert (appdata v)
		{
			v2f o;
			o.vertex = float4(v.vertex.xy, 0, 1);
			o.uv = (v.vertex + 1) * 0.5;
#if UNITY_UV_STARTS_AT_TOP
			o.uv= o.uv * float2(1.0, -1.0) + float2(0.0, 1.0);
#endif
			return o;
		}

		sampler2D _MainTex;
		float4 _MainTex_TexelSize;
		sampler2D _CameraDepthTexture;
		sampler2D _CoCTex;
		sampler2D _DoFTex;
		float _FocusDistance;
		float _FocusRange;
		float _BokehRadius;
		float _Debug;

		half3 Sample(float2 uv)
		{
			return tex2D(_MainTex, uv).rgb;
		}
				
		ENDCG
		Pass // CircleOfConfusionPass
        {
			Name "COC"
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			fixed4 frag (v2f i) : SV_Target
			{
				half depth = SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, i.uv);
				depth = LinearEyeDepth(depth);
				float coc = (depth - _FocusDistance) / _FocusRange;
				coc = clamp(coc, -1, 1);
				if (_Debug > 0)
				{
					coc = coc > 0 ? coc : -coc;
				}
				else
				{
					coc = coc;
				}
				return coc;
			}
            ENDCG
        }
		Pass // PreFilterPass
		{
			Name "PreFilterPass"
			CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			half ColorWeight(half3 c)
			{
				return 1 / (1 + max(max(c.r, c.g),c.b));
			}
			fixed4 frag (v2f i) : SV_Target
			{
				float4 o = _MainTex_TexelSize.xyxy * float2(-0.5, 0.5).xxyy;

				half3 s0 = tex2D(_MainTex, i.uv + o.xy).rgb;
				half3 s1 = tex2D(_MainTex, i.uv + o.zy).rgb;
				half3 s2 = tex2D(_MainTex, i.uv + o.xw).rgb;
				half3 s3 = tex2D(_MainTex, i.uv + o.zw).rgb;

				half w0 = ColorWeight(s0);
				half w1 = ColorWeight(s1);
				half w2 = ColorWeight(s2);
				half w3 = ColorWeight(s3);
				half3 color = s0 * w0 + s1 * w1 + s2 * w2 + s3 * w3;
				color /= max(w0 + w1 + w2 + w3, 0.001);

				half coc0 = tex2D(_CoCTex,i.uv + o.xy).r;
				half coc1 = tex2D(_CoCTex,i.uv + o.zy).r;
				half coc2 = tex2D(_CoCTex,i.uv + o.xw).r;
				half coc3 = tex2D(_CoCTex,i.uv + o.zw).r;
				half cocMin = min(min(min(coc0, coc1), coc2), coc3);
				half cocMax = max(max(max(coc0, coc1), coc2), coc3);
				half coc = cocMax >= -cocMin ? cocMax : cocMin;
				return half4(color, coc);
			}
            ENDCG

		}
		Pass // Bokeh
        {
			Name "Bokeh"
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
#define KERNEL_MEDIUM
            #include "Packages/com.unity.postprocessing/PostProcessing/Shaders/Builtins/DiskKernels.hlsl"
			half Weight(half coc, half radius)
			{
				//return coc >= radius;
				return saturate((coc - radius + 2) / 2);
			}
			fixed4 frag (v2f i) : SV_Target
			{
				half3 color = 0;
				half3 bgColor = 0;
				half3 fgColor = 0;
				half bgWeight = 0, fgWeight = 0;
				half coc = tex2D(_MainTex, i.uv).a;
				for (int j = 0; j < kSampleCount; j++)
				{
					//float2 o = kDiskKernel[j]*_MainTex_TexelSize.xy*_BokehRadius;
					float2 o = kDiskKernel[j];
					half radius = length(o);
					o = o * _MainTex_TexelSize.xy * _BokehRadius;
					half4 s = tex2D(_MainTex, i.uv+o);
					half tmpWeight = Weight(max(-s.a, 0), radius);
					fgWeight += tmpWeight;
					fgColor += s.rgb * tmpWeight;

					tmpWeight = Weight(max(s.a, 0), radius);
					bgWeight += tmpWeight;
					bgColor  += s.rgb * tmpWeight;
				}
				fgColor *= 1 / (fgWeight + (fgWeight == 0));
				bgColor *= 1 / (bgWeight + (bgWeight == 0));
				half bgfg = min(1, fgWeight);
				//half bgfg = fgWeight / kSampleCount;
				color = lerp(bgColor, fgColor, bgfg);
				return half4(color, bgfg);
			}
            ENDCG
        }
		Pass // PostFilterPass
		{
			Name "PostFilterPass"
			CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			fixed4 frag (v2f i) : SV_Target
			{
				float4 o = _MainTex_TexelSize.xyxy * float2(-0.5, 0.5).xxyy;
				half3 s =
					Sample(i.uv + o.xy) +
					Sample(i.uv + o.zy) +
					Sample(i.uv + o.xw) +
					Sample(i.uv + o.zw);
				return half4(s * 0.25, 1);
			}
            ENDCG

		}
		Pass // CombinePass
		{
			Name "CombinePass"
			CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			fixed4 frag (v2f i) : SV_Target
			{
				half4 source = tex2D(_MainTex, i.uv);
				half coc = tex2D(_CoCTex, i.uv).r;
				half4 dof = tex2D(_DoFTex, i.uv);
				half dofStrength = smoothstep(0.1, 1, abs(coc));
				half3 color = lerp(source.rgb, dof.rgb, dofStrength);
				//half dofStrength = smoothstep(0.1, 1, abs(coc));
				//half3 color = lerp(source.rgb, dof.rgb, dofStrength + dof.a - dofStrength * dof.a);
				return half4(color, source.a);
			}
            ENDCG
		}
    }
}
