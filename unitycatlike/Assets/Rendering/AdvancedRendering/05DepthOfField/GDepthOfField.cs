﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;


[Serializable]
[PostProcess(typeof(GDepthOfFieldRenderer), PostProcessEvent.BeforeStack, "Custom/GDepthOfField")]
public class GDepthOfField : PostProcessEffectSettings
{
    [Range(0.1f, 300f)]
    public FloatParameter focusDistance = new FloatParameter { value = 10.0f };
    [Range(0.01f, 50f)]
    public FloatParameter focusRange = new FloatParameter { value = 3.0f };
    [Range(1f, 10f)]
    public FloatParameter bokehRadius = new FloatParameter { value = 4.0f };
    public BoolParameter debug = new BoolParameter{ value = false };
}

public class GDepthOfFieldRenderer : PostProcessEffectRenderer<GDepthOfField>
{
    Shader shader;
    const int circleOfConfusionPass = 0;
    const int preFilterPass = 1;
    const int bokehPass = 2;
    const int postFilterPass = 3;
    const int combinePass = 4;
    public override void Init()
    {
        shader = Shader.Find("Hidden/Custom/GDepthOfField");
    }

    public override void Render(PostProcessRenderContext context)
    {
        var cmd = context.command;

        if (shader == null) return;
        var sheet = context.propertySheets.Get(shader);
        if (sheet == null) return;

        cmd.BeginSample("GDepthOfField");
        cmd.SetGlobalFloat("_FocusDistance", settings.focusDistance);
        cmd.SetGlobalFloat("_FocusRange", settings.focusRange);
        cmd.SetGlobalFloat("_BokehRadius", settings.bokehRadius);
        cmd.SetGlobalFloat("_Debug", settings.debug?1:0);
        var coc = context.GetScreenSpaceTemporaryRT(0, RenderTextureFormat.RHalf, RenderTextureReadWrite.Linear);
        if (settings.debug)
        {
            cmd.BlitFullscreenTriangle(context.source, coc, sheet, circleOfConfusionPass);
            cmd.BlitFullscreenTriangle(coc, context.destination);
        }
        else
        {
            cmd.BlitFullscreenTriangle(context.source, coc, sheet, circleOfConfusionPass);
            int w = context.screenWidth / 2;
            int h = context.screenHeight / 2;
            RenderTexture dof0 = context.GetScreenSpaceTemporaryRT(0, context.sourceFormat, RenderTextureReadWrite.Default, w, h);
            RenderTexture dof1 = context.GetScreenSpaceTemporaryRT(0, context.sourceFormat, RenderTextureReadWrite.Default, w, h);
            cmd.SetGlobalTexture("_CoCTex", coc);
            cmd.BlitFullscreenTriangle(context.source, dof0, sheet, preFilterPass);
            cmd.BlitFullscreenTriangle(dof0, dof1, sheet, bokehPass);
            cmd.BlitFullscreenTriangle(dof1, dof0, sheet, postFilterPass);
            cmd.SetGlobalTexture("_DoFTex", dof0);
            cmd.BlitFullscreenTriangle(context.source, context.destination, sheet, combinePass);
            RenderTexture.ReleaseTemporary(coc);
            RenderTexture.ReleaseTemporary(dof0);
            RenderTexture.ReleaseTemporary(dof1);
        }

        cmd.EndSample("GDepthOfField End");
    }
}
