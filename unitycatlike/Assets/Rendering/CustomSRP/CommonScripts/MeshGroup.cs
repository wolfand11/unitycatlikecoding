﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

[ExecuteInEditMode]
public class MeshGroup : MonoBehaviour
{
    static int baseColorId = Shader.PropertyToID("_BaseColor");
    static int metallicId = Shader.PropertyToID("_Metallic");
    static int smoothnessId = Shader.PropertyToID("_Smoothness");

    public bool isRunInEditMode = false;
	[SerializeField]
	Mesh mesh = default;

	[SerializeField]
	Material material = default;

    [SerializeField]
    LightProbeProxyVolume lightProbeProxyVolume = null;

    Matrix4x4[] matrices = new Matrix4x4[1023];
	Vector4[] baseColors = new Vector4[1023];
    float[] metallic = new float[1023];
    float[] smoothness = new float[1023];

	MaterialPropertyBlock block;

    private void Awake()
    {
        var r = GetComponent<Renderer>();
        material = r.sharedMaterial;
        var mf = GetComponent<MeshFilter>();
        mesh = mf.sharedMesh;
        r.enabled = false;

        for (int i = 0; i < matrices.Length; i++)
        {
			matrices[i] = Matrix4x4.TRS(
				Random.insideUnitSphere * 10f, Quaternion.identity, Vector3.one
			);
			baseColors[i] = new Vector4(Random.value, Random.value, Random.value, Random.Range(0.5f, 1f));
            metallic[i] = Random.value < 0.25f ? 1f : 0;
            smoothness[i] = Random.Range(0.05f, 0.95f);
		}
    }

    private void Update()
    {
        if (!isRunInEditMode) return;

        if (block == null)
        {
            block = new MaterialPropertyBlock();
            block.SetVectorArray(baseColorId, baseColors);
            block.SetFloatArray(metallicId, metallic);
            block.SetFloatArray(smoothnessId, smoothness);

            if (!lightProbeProxyVolume)
            {
                var positions = new Vector3[1023];
                for (var i = 0; i < matrices.Length; i++)
                {
                    positions[i] = matrices[i].GetColumn(3);
                }
                var lightProbes = new SphericalHarmonicsL2[1023];
                var occlusionProbes = new Vector4[1023];
                LightProbes.CalculateInterpolatedLightAndOcclusionProbes(positions, lightProbes, occlusionProbes);
                block.CopySHCoefficientArraysFrom(lightProbes);
                block.CopyProbeOcclusionArrayFrom(occlusionProbes);
            }
        }
        Graphics.DrawMeshInstanced(mesh, 0, material, matrices, 1023, block, 
            ShadowCastingMode.On, true, 0, null, 
            lightProbeProxyVolume ? LightProbeUsage.UseProxyVolume : LightProbeUsage.CustomProvided,
            lightProbeProxyVolume);
    }
}
