#ifndef STANDARD_SURFACE_URP_DEPTHONLYPASS
#define STANDARD_SURFACE_URP_DEPTHONLYPASS
#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

Surface_Varyings Surface_DepthOnlyVertex(Surface_Attributes input)
{
    Surface_Varyings output = (Surface_Varyings)0;
	SURFACE_vert_func(input);

#if defined(XD_NEED_LINK_BASEMAP)
	output.uv = TRANSFORM_TEX(input.texcoord, Surface_BaseMap);
#else
	output.uv = TRANSFORM_TEX(input.texcoord, _BaseMap);
#endif
    output.positionCS = TransformObjectToHClip(input.positionOS.xyz);
    return output;
}

half4 Surface_DepthOnlyFragment(Surface_Varyings input) : SV_TARGET
{
    SurfaceData surfaceData = (SurfaceData)0;
	SURFACE_surf_func(input, surfaceData);
#if defined(XD_NEED_LINK_BASEMAP)
    Alpha(surfaceData.alpha, Surface_BaseColor, _Cutoff);
#else
    Alpha(surfaceData.alpha, _BaseColor, _Cutoff);
#endif
    return 0;
}
#endif // STANDARD_SURFACE_URP_DEPTHONLYPASS
