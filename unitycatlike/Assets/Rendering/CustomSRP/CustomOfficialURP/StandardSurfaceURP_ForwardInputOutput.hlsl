#ifndef STANDARD_SURFACE_URP_FORWARD_INOUT
#define STANDARD_SURFACE_URP_FORWARD_INOUT

// GLES2 has limited amount of interpolators
#if defined(_PARALLAXMAP) && !defined(SHADER_API_GLES)
#define REQUIRES_TANGENT_SPACE_VIEW_DIR_INTERPOLATOR
#endif

#if (defined(_NORMALMAP) || (defined(_PARALLAXMAP) && !defined(REQUIRES_TANGENT_SPACE_VIEW_DIR_INTERPOLATOR))) || defined(_DETAIL)
#define REQUIRES_WORLD_SPACE_TANGENT_INTERPOLATOR
#endif

struct Surface_Attributes
{
	float4 positionOS   : POSITION;
	float3 normalOS     : NORMAL;
	float4 tangentOS    : TANGENT;
	float2 texcoord     : TEXCOORD0;
	float2 lightmapUV   : TEXCOORD1;
	float2 dynamicLightmapUV : TEXCOORD2;

	float4 vertColor : COLOR;
	UNITY_VERTEX_INPUT_INSTANCE_ID
};

struct Surface_Varyings
{
	float2 uv                       : TEXCOORD0;
	DECLARE_LIGHTMAP_OR_SH(lightmapUV, vertexSH, 1);

#if defined(REQUIRES_WORLD_SPACE_POS_INTERPOLATOR)
	float3 positionWS               : TEXCOORD2;
#endif

	float3 normalWS                 : TEXCOORD3;
#if defined(REQUIRES_WORLD_SPACE_TANGENT_INTERPOLATOR)
	float4 tangentWS                : TEXCOORD4;    // xyz: tangent, w: sign
#endif
	float3 viewDirWS                : TEXCOORD5;

	half4 fogFactorAndVertexLight   : TEXCOORD6; // x: fogFactor, yzw: vertex light

#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
	float4 shadowCoord              : TEXCOORD7;
#endif

#if defined(REQUIRES_TANGENT_SPACE_VIEW_DIR_INTERPOLATOR)
	float3 viewDirTS                : TEXCOORD8;
#endif

	float4 positionCS               : SV_POSITION;
#if defined(XD_REQUIRES_SCREEN_UV)
    float2 screenUV                 : TEXCOORD9;
#endif

#if defined(XD_REQUIRES_VERTEX_COLOR)
	float4 vertColor				: TEXCOORD10;
#endif
	
	UNITY_VERTEX_INPUT_INSTANCE_ID
	UNITY_VERTEX_OUTPUT_STEREO
};
#endif // STANDARD_SURFACE_URP_FORWARD_INOUT 
