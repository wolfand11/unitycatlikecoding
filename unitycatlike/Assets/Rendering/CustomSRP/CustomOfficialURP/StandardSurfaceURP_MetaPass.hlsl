#ifndef STANDARD_SURFACE_URP_METAPASS
#define STANDARD_SURFACE_URP_METAPASS
#include "Packages/com.unity.render-pipelines.universal/Shaders/LitMetaPass.hlsl"


#ifndef SURFACE_vert_func
void DefaultLitSurfaceVertData(inout Surface_Attributes input)
{
	//do nothing
}
#define SURFACE_vert_func DefaultLitSurfaceVertData
#endif

#ifndef SURFACE_surf_func
void DefaultLitSurfaceData(Surface_Varyings input, inout SurfaceData surfaceData)
{
	 InitializeStandardLitSurfaceData(input.uv, surfaceData);
}
#define SURFACE_surf_func DefaultLitSurfaceData
#endif

Surface_Varyings Surface_VertexMeta(Surface_Attributes input)
{
	Surface_Varyings output = (Surface_Varyings)0;
	SURFACE_vert_func(input);

    VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
	VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS, input.tangentOS);

	half3 viewDirWS = GetWorldSpaceViewDir(vertexInput.positionWS);
	half3 vertexLight = VertexLighting(vertexInput.positionWS, normalInput.normalWS);
	half fogFactor = ComputeFogFactor(vertexInput.positionCS.z);

#if defined(XD_NEED_LINK_BASEMAP)
	output.uv = TRANSFORM_TEX(input.texcoord, Surface_BaseMap);
#else
	output.uv = TRANSFORM_TEX(input.texcoord, _BaseMap);
#endif

	// already normalized from normal transform to WS.
	output.normalWS = normalInput.normalWS;
	output.viewDirWS = viewDirWS;
#if defined(REQUIRES_WORLD_SPACE_TANGENT_INTERPOLATOR) || defined(REQUIRES_TANGENT_SPACE_VIEW_DIR_INTERPOLATOR)
	real sign = input.tangentOS.w * GetOddNegativeScale();
	half4 tangentWS = half4(normalInput.tangentWS.xyz, sign);
#endif
#if defined(REQUIRES_WORLD_SPACE_TANGENT_INTERPOLATOR)
	output.tangentWS = tangentWS;
#endif

#if defined(REQUIRES_TANGENT_SPACE_VIEW_DIR_INTERPOLATOR)
	half3 viewDirTS = GetViewDirectionTangentSpace(tangentWS, output.normalWS, viewDirWS);
	output.viewDirTS = viewDirTS;
#endif

#if defined(REQUIRES_WORLD_SPACE_POS_INTERPOLATOR)
	output.positionWS = vertexInput.positionWS;
#endif

	output.positionCS = MetaVertexPosition(input.positionOS, input.lightmapUV, input.dynamicLightmapUV, unity_LightmapST, unity_DynamicLightmapST);
#if defined(XD_REQUIRES_SCREEN_UV)
	output.screenUV = ComputeScreenPos(output.positionCS / output.positionCS.w).xy;
#endif

	return output;
}

half4 Surface_FragmentMeta(Surface_Varyings input) : SV_Target
{
	SurfaceData surfaceData = (SurfaceData)0;
	SURFACE_surf_func(input, surfaceData);

	MetaInput metaInput;
#if defined(XD_META_NEED_BRDF_EFFECT)
	BRDFData brdfData;
	InitializeBRDFData(surfaceData.albedo, surfaceData.metallic, surfaceData.specular, surfaceData.smoothness, surfaceData.alpha, brdfData);
	metaInput.Albedo = brdfData.diffuse + brdfData.specular * brdfData.roughness * 0.5;
#else
	metaInput.Albedo = surfaceData.albedo;
#endif
	//metaInput.SpecularColor = surfaceData.specular;
	metaInput.Emission = surfaceData.emission;

	return MetaFragment(metaInput);
}
#endif // STANDARD_SURFACE_URP_METAPASS
