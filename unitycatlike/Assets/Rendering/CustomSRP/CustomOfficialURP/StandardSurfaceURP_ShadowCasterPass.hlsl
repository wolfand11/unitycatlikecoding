#ifndef STANDARD_SURFACE_URP_SHADOWCASTPASS
#define STANDARD_SURFACE_URP_SHADOWCASTPASS

#include "Packages/com.unity.render-pipelines.universal/Shaders/ShadowCasterPass.hlsl"

float4 Surface_GetShadowPositionHClip(Surface_Attributes input)
{
    float3 positionWS = TransformObjectToWorld(input.positionOS.xyz);
    float3 normalWS = TransformObjectToWorldNormal(input.normalOS);

    float4 positionCS = TransformWorldToHClip(ApplyShadowBias(positionWS, normalWS, _LightDirection));

#if UNITY_REVERSED_Z
    positionCS.z = min(positionCS.z, positionCS.w * UNITY_NEAR_CLIP_VALUE);
#else
    positionCS.z = max(positionCS.z, positionCS.w * UNITY_NEAR_CLIP_VALUE);
#endif

    return positionCS;
}

Surface_Varyings Surface_ShadowPassVertex(Surface_Attributes input)
{
	Surface_Varyings output = (Surface_Varyings)0;
	SURFACE_vert_func(input);

#if defined(XD_NEED_LINK_BASEMAP)
	output.uv = TRANSFORM_TEX(input.texcoord, Surface_BaseMap);
#else
	output.uv = TRANSFORM_TEX(input.texcoord, _BaseMap);
#endif

    output.positionCS = Surface_GetShadowPositionHClip(input);
	return output;
}

half4 Surface_ShadowPassFragment(Surface_Varyings input) : SV_Target
{
	SurfaceData surfaceData = (SurfaceData)0;
	SURFACE_surf_func(input, surfaceData);
#if defined(XD_NEED_LINK_BASEMAP)
    Alpha(surfaceData.alpha, Surface_BaseColor, _Cutoff);
#else
    Alpha(surfaceData.alpha, _BaseColor, _Cutoff);
#endif

	return 0;
}
#endif // STANDARD_SURFACE_URP_SHADOWCASTPASS
