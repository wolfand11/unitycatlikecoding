#ifndef STANDARD_SURFACE_URP_FORWARDPASS
#define STANDARD_SURFACE_URP_RORWARDPASS

#ifndef SURFACE_vert_func
void DefaultLitSurfaceVertData(inout Surface_Attributes input)
{
	//do nothing
}
#define SURFACE_vert_func DefaultLitSurfaceVertData
#endif

#ifndef SURFACE_surf_func
void DefaultLitSurfaceData(Surface_Varyings input, inout SurfaceData surfaceData)
{
	 InitializeStandardLitSurfaceData(input.uv, surfaceData);
}
#define SURFACE_surf_func DefaultLitSurfaceData
#endif

Surface_Varyings Surface_LitPassVertex(Surface_Attributes input)
{
	Surface_Varyings output = (Surface_Varyings)0;

	UNITY_SETUP_INSTANCE_ID(input);
	UNITY_TRANSFER_INSTANCE_ID(input, output);
	UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

	SURFACE_vert_func(input);

	VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);

	// normalWS and tangentWS already normalize.
	// this is required to avoid skewing the direction during interpolation
	// also required for per-vertex lighting and SH evaluation
	VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS, input.tangentOS);

	half3 viewDirWS = GetWorldSpaceViewDir(vertexInput.positionWS);
	half3 vertexLight = VertexLighting(vertexInput.positionWS, normalInput.normalWS);
	half fogFactor = ComputeFogFactor(vertexInput.positionCS.z);

#if defined(XD_NEED_LINK_BASEMAP)
	output.uv = TRANSFORM_TEX(input.texcoord, Surface_BaseMap);
#else
	output.uv = TRANSFORM_TEX(input.texcoord, _BaseMap);
#endif

	// already normalized from normal transform to WS.
	output.normalWS = normalInput.normalWS;
	output.viewDirWS = viewDirWS;
#if defined(REQUIRES_WORLD_SPACE_TANGENT_INTERPOLATOR) || defined(REQUIRES_TANGENT_SPACE_VIEW_DIR_INTERPOLATOR)
	real sign = input.tangentOS.w * GetOddNegativeScale();
	half4 tangentWS = half4(normalInput.tangentWS.xyz, sign);
#endif
#if defined(REQUIRES_WORLD_SPACE_TANGENT_INTERPOLATOR)
	output.tangentWS = tangentWS;
#endif

#if defined(REQUIRES_TANGENT_SPACE_VIEW_DIR_INTERPOLATOR)
	half3 viewDirTS = GetViewDirectionTangentSpace(tangentWS, output.normalWS, viewDirWS);
	output.viewDirTS = viewDirTS;
#endif

	OUTPUT_LIGHTMAP_UV(input.lightmapUV, unity_LightmapST, output.lightmapUV);
	OUTPUT_SH(output.normalWS.xyz, output.vertexSH);

	output.fogFactorAndVertexLight = half4(fogFactor, vertexLight);

#if defined(REQUIRES_WORLD_SPACE_POS_INTERPOLATOR)
	output.positionWS = vertexInput.positionWS;
#endif

#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
	output.shadowCoord = GetShadowCoord(vertexInput);
#endif

	output.positionCS = vertexInput.positionCS;

#if defined(XD_REQUIRES_SCREEN_UV)
	output.screenUV = ComputeScreenPos(output.positionCS / output.positionCS.w).xy;
#endif

#if defined(XD_REQUIRES_VERTEX_COLOR)
	output.vertColor = input.vertColor;
#endif

	return output;
}

void Surface_InitializeInputData(Surface_Varyings input, half3 normalTS, out InputData inputData)
{
    inputData = (InputData)0;

#if defined(REQUIRES_WORLD_SPACE_POS_INTERPOLATOR)
    inputData.positionWS = input.positionWS;
#endif

    half3 viewDirWS = SafeNormalize(input.viewDirWS);
#if defined(_NORMALMAP) || defined(_DETAIL)
    float sgn = input.tangentWS.w;      // should be either +1 or -1
    float3 bitangent = sgn * cross(input.normalWS.xyz, input.tangentWS.xyz);
    inputData.normalWS = TransformTangentToWorld(normalTS, half3x3(input.tangentWS.xyz, bitangent.xyz, input.normalWS.xyz));
#else
    inputData.normalWS = input.normalWS;
#endif

    inputData.normalWS = NormalizeNormalPerPixel(inputData.normalWS);
    inputData.viewDirectionWS = viewDirWS;

#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
    inputData.shadowCoord = input.shadowCoord;
#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
    inputData.shadowCoord = TransformWorldToShadowCoord(inputData.positionWS);
#else
    inputData.shadowCoord = float4(0, 0, 0, 0);
#endif

    inputData.fogCoord = input.fogFactorAndVertexLight.x;
    inputData.vertexLighting = input.fogFactorAndVertexLight.yzw;
    inputData.bakedGI = SAMPLE_GI(input.lightmapUV, input.vertexSH, inputData.normalWS);
    inputData.normalizedScreenSpaceUV = GetNormalizedScreenSpaceUV(input.positionCS);
    inputData.shadowMask = SAMPLE_SHADOWMASK(input.lightmapUV);
}

// Used in Standard (Physically Based) shader
half4 Surface_LitPassFragment(Surface_Varyings input) : SV_Target
{
	UNITY_SETUP_INSTANCE_ID(input);
	UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

#if defined(_PARALLAXMAP)
#if defined(REQUIRES_TANGENT_SPACE_VIEW_DIR_INTERPOLATOR)
	half3 viewDirTS = input.viewDirTS;
#else
	half3 viewDirTS = GetViewDirectionTangentSpace(input.tangentWS, input.normalWS, input.viewDirWS);
#endif
	ApplyPerPixelDisplacement(viewDirTS, input.uv);
#endif

	SurfaceData surfaceData;
	SURFACE_surf_func(input, surfaceData);
	//return input.uv.xyxy;
	//return surfaceData.albedo.rgbr;
	//return surfaceData.emission.rgbr;

	InputData inputData;
	Surface_InitializeInputData(input, surfaceData.normalTS, inputData);

	half4 color = UniversalFragmentPBR(inputData, surfaceData);

	color.rgb = MixFog(color.rgb, inputData.fogCoord);
	color.a = OutputAlpha(color.a, _Surface);

	return color;
}

#endif // STANDARD_SURFACE_URP_RORWARDPASS
