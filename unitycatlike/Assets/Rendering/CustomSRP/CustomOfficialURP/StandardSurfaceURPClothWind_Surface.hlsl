#ifndef STANDARD_SURFACE_URP_CLOTHWIND
#define STANDARD_SURFACE_URP_CLOTHWIND

//#define XD_REQUIRES_SCREEN_UV
//#define _NORMALMAP

#define XD_CUSTOM_MAT_PROP \
half4 _Color; \
sampler2D _MainTex; \
float4 _MainTex_ST; \
half _Glossiness; \
float _Speed; \
float _SwayMaxX; \
float _SwayMaxY; \
float _SwayMaxZ; \
float _WaveSize; \

#include "StandardSurfaceURP_LitInput.hlsl"
#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
#include "StandardSurfaceURP_ForwardInputOutput.hlsl"

void surface_vert (inout Surface_Attributes input) 
{
	float3 positionWS = TransformObjectToWorld(input.positionOS.xyz);
	float wave = 1/_WaveSize;
	float x = sin((positionWS.y + positionWS.z) * wave + (_Time.x * _Speed))  * 5;// x axis movements
	float y = sin((positionWS.x + positionWS.z) * wave + (_Time.x * _Speed))  * 5;// x axis movements
	float z = sin((positionWS.y + positionWS.x) * wave + (_Time.x * _Speed))  * 5;// x axis movements
	// warning TODO
	input.positionOS.x += step(0,1) * x * _SwayMaxX * input.vertColor.x;
	input.positionOS.y += step(0,1) * y * _SwayMaxY * input.vertColor.x;
	input.positionOS.z += step(0,1) * z * _SwayMaxZ * input.vertColor.x;
}

void surface_surf (Surface_Varyings input, inout SurfaceData o) 
{
	o = (SurfaceData)0;
	// Albedo comes from a texture tinted by color
	float4 c = tex2D (_MainTex, input.uv) * _Color;
	o.albedo = c.rgb;
	o.metallic = _Metallic;
	o.smoothness = _Glossiness;
	o.normalTS = SampleNormal(input.uv, TEXTURE2D_ARGS(_BumpMap, sampler_BumpMap), _BumpScale);
	o.occlusion = SampleOcclusion(input.uv);
	o.emission = SampleEmission(input.uv, _EmissionColor.rgb, TEXTURE2D_ARGS(_EmissionMap, sampler_EmissionMap));
	o.alpha = c.a;
}

#define SURFACE_vert_func surface_vert
#define SURFACE_surf_func surface_surf

#endif // STANDARD_SURFACE_URP_CLOTHWIND
