Shader "CustomURP/StandardSurfaceURP"
{
    Properties
    {
		[ToggleOff] _Receive_Shadows("Receive Shadows", Float) = 1.0

		_Color("Color", Color) = (1,1,1,1)
        _MainTex("Albedo", 2D) = "white" {}
     
        _Cutoff("Alpha Cutoff", Range(0.0, 1.0)) = 0.5
 
        _Glossiness("Smoothness", Range(0.0, 1.0)) = 0.5
        [Gamma] _Metallic("Metallic", Range(0.0, 1.0)) = 0.0
        
        _Speed ("MoveSpeed", Range(1,500)) = 30 // speed of the swaying
        _WaveSize ("Wave Size", Range(0.05,1)) = 0.5
        
        _SwayMaxX("Sway Max X", Range(0, 0.1)) = .0025 // how far the swaying goes in X
        _SwayMaxY("Sway Max Y", Range(0, 0.1)) = .0025 // how far the swaying goes in Y
        _SwayMaxZ("Sway Max Z", Range(0, 0.1)) = .0025 // how far the swaying goes in Z


        //[Enum(UV0,0,UV1,1)] _UVSec ("UV Set for secondary textures", Float) = 0
 
        // Blending state
        [HideInInspector] _Mode ("__mode", Float) = 0.0
        [HideInInspector] _SrcBlend ("__src", Float) = 1.0
        [HideInInspector] _DstBlend ("__dst", Float) = 0.0
        [HideInInspector] _ZWrite ("__zw", Float) = 1.0

        [HideInInspector] _Surface("__surface", Float) = 0.0
    }
    SubShader
    {
		Tags{"RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" "UniversalMaterialType" = "Lit" "IgnoreProjector" = "True" "ShaderModel"="3.0"}
        LOD 300

		Pass
        {
            Name "ForwardLit"
            Tags { "LightMode" = "UniversalForward"}

            Blend[_SrcBlend][_DstBlend]
            ZWrite[_ZWrite]
            Cull[_Cull]

            HLSLPROGRAM
            //#pragma only_renderers gles gles3 glcore d3d11
            #pragma target 2.0

            //--------------------------------------
            // GPU Instancing
            #pragma multi_compile_instancing

            // -------------------------------------
            // Material Keywords
            #pragma shader_feature_local _NORMALMAP
            #pragma shader_feature_local_fragment _ALPHATEST_ON
            #pragma shader_feature_local_fragment _ALPHAPREMULTIPLY_ON
            #pragma shader_feature_local_fragment _EMISSION
            #pragma shader_feature_local_fragment _METALLICSPECGLOSSMAP
            #pragma shader_feature_local_fragment _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
            #pragma shader_feature_local_fragment _OCCLUSIONMAP
            #pragma shader_feature_local _PARALLAXMAP
            #pragma shader_feature_local _ _DETAIL_MULX2 _DETAIL_SCALED

            #pragma shader_feature_local_fragment _SPECULARHIGHLIGHTS_OFF
            #pragma shader_feature_local_fragment _ENVIRONMENTREFLECTIONS_OFF
            #pragma shader_feature_local_fragment _SPECULAR_SETUP
            #pragma shader_feature_local _RECEIVE_SHADOWS_OFF

            // -------------------------------------
            // Universal Pipeline keywords
            #pragma multi_compile _ _MAIN_LIGHT_SHADOWS
            #pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
            #pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
            #pragma multi_compile_fragment _ _ADDITIONAL_LIGHT_SHADOWS
            #pragma multi_compile_fragment _ _SHADOWS_SOFT
            #pragma multi_compile _ LIGHTMAP_SHADOW_MIXING
            #pragma multi_compile _ SHADOWS_SHADOWMASK
            #pragma multi_compile_fragment _ _SCREEN_SPACE_OCCLUSION

            // -------------------------------------
            // Unity defined keywords
            #pragma multi_compile _ DIRLIGHTMAP_COMBINED
            #pragma multi_compile _ LIGHTMAP_ON
            #pragma multi_compile_fog

			#define XD_NEED_LINK_BASEMAP
			#define Surface_BaseMap _MainTex
			#define Surface_BaseMap_ST _MainTex_ST
			#define Surface_BaseColor _Color

            #pragma vertex Surface_LitPassVertex
            #pragma fragment Surface_LitPassFragment
			#include "StandardSurfaceURP_Surface.hlsl"
			#include "StandardSurfaceURP_ForwardPass.hlsl"
			ENDHLSL
		}

		Pass
        {
            Name "ShadowCaster"
            Tags{"LightMode" = "ShadowCaster"}

            ZWrite On
            ZTest LEqual
            ColorMask 0
            Cull[_Cull]

            HLSLPROGRAM
            #pragma target 2.0

            // -------------------------------------
            // Material Keywords
            #pragma shader_feature_local_fragment _ALPHATEST_ON
            #pragma shader_feature_local_fragment _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A

            //--------------------------------------
            // GPU Instancing
            #pragma multi_compile_instancing

			#define XD_NEED_LINK_BASEMAP
			#define Surface_BaseMap _MainTex
			#define Surface_BaseMap_ST _MainTex_ST
			#define Surface_BaseColor _Color

			#pragma vertex Surface_ShadowPassVertex
            #pragma fragment Surface_ShadowPassFragment
			#include "StandardSurfaceURP_Surface.hlsl"
            #include "StandardSurfaceURP_ShadowCasterPass.hlsl"
            ENDHLSL
        }

		Pass
        {
            Name "DepthOnly"
            Tags{"LightMode" = "DepthOnly"}

            ZWrite On
            ColorMask 0
            Cull[_Cull]

            HLSLPROGRAM
            #pragma target 2.0

            //--------------------------------------
            // GPU Instancing
            #pragma multi_compile_instancing

			#define XD_NEED_LINK_BASEMAP
			#define Surface_BaseMap _MainTex
			#define Surface_BaseMap_ST _MainTex_ST
			#define Surface_BaseColor _Color

            #pragma vertex Surface_DepthOnlyVertex
            #pragma fragment Surface_DepthOnlyFragment

            // -------------------------------------
            // Material Keywords
            #pragma shader_feature_local_fragment _ALPHATEST_ON
            #pragma shader_feature_local_fragment _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A

			#include "StandardSurfaceURP_Surface.hlsl"
            #include "StandardSurfaceURP_DepthOnlyPass.hlsl"
            ENDHLSL
        }

		Pass
        {
            Name "Meta"
            Tags{"LightMode" = "Meta"}

            Cull Off

            HLSLPROGRAM
            #pragma target 2.0
			#define XD_META_PASS

            #pragma shader_feature_local_fragment _SPECULAR_SETUP
            #pragma shader_feature_local_fragment _EMISSION
            #pragma shader_feature_local_fragment _METALLICSPECGLOSSMAP
            #pragma shader_feature_local_fragment _ALPHATEST_ON
            #pragma shader_feature_local_fragment _ _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
            #pragma shader_feature_local _ _DETAIL_MULX2 _DETAIL_SCALED

            #pragma shader_feature_local_fragment _SPECGLOSSMAP

			#define XD_NEED_LINK_BASEMAP
			#define Surface_BaseMap _MainTex
			#define Surface_BaseMap_ST _MainTex_ST

			#pragma vertex Surface_VertexMeta
            #pragma fragment Surface_FragmentMeta
			#include "StandardSurfaceURP_Surface.hlsl"
			#include "StandardSurfaceURP_MetaPass.hlsl"

            ENDHLSL
        }
    }
}
