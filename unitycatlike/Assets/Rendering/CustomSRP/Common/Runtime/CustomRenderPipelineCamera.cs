﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

[DisallowMultipleComponent, RequireComponent(typeof(Camera))]
public class CustomRenderPipelineCamera : MonoBehaviour
{
    [SerializeField]
    CameraSettings settings = default;
    public CameraSettings Settings => settings ??= new ();

    ProfilingSampler sampler;
    public ProfilingSampler Sampler => sampler ??= new(GetComponent<Camera>().name);

    #if UNITY_EDITOR || DEVELOPMENT_BUILD
    private void OnEnable()
    {
        sampler = null;
    }
    #endif
}
