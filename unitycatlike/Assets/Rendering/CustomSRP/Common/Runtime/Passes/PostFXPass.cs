using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.RenderGraphModule;
using UnityEngine.Rendering;

public class PostFXPass
{
    private PostFXStack postFXStack;

    void Render(RenderGraphContext context)
    {
        postFXStack.Render(context, CameraRenderer.colorAttachmentId);
    }

    public static void Record(RenderGraph graph, PostFXStack postFXStack)
    {
        using RenderGraphBuilder builder = graph.AddRenderPass("Post FX", out PostFXPass pass);
        pass.postFXStack = postFXStack;
        builder.SetRenderFunc<PostFXPass>(((pass, context) => pass.Render(context)));
    }
}
