using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.RenderGraphModule;
using UnityEngine.Rendering;

public class SetupPass
{
    private static readonly ProfilingSampler _sampler = new("Setup Pass");
    private CameraRenderer renderer;

    void Render(RenderGraphContext context)
    {
        renderer.Setup();
    }

    public static void Record(RenderGraph graph, CameraRenderer renderer)
    {
        using RenderGraphBuilder builder = graph.AddRenderPass("Setup Pass", out SetupPass pass);
        pass.renderer = renderer;
        builder.SetRenderFunc<SetupPass>(((pass, context) => pass.Render(context)));
    }
}
