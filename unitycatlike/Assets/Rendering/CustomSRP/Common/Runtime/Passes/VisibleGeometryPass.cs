using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.RenderGraphModule;
using UnityEngine.Rendering;

public class VisibleGeometryPass
{
    private CameraRenderer renderer;
    bool useDynamicBatching;
    bool useGPUInstancing;
    bool useLightsPerObject;
    int renderingLayerMask;

    void Render(RenderGraphContext context)
    {
        renderer.DrawVisibleGeometry(useDynamicBatching, useGPUInstancing, useLightsPerObject, renderingLayerMask);
    }

    public static void Record(RenderGraph graph, CameraRenderer renderer, bool useDynamicBatching, bool useGPUInstancing, bool useLightsPerObject, int renderingLayerMask)
    {
        using RenderGraphBuilder builder = graph.AddRenderPass("Visible Geometry", out VisibleGeometryPass pass);
        pass.renderer = renderer;
        pass.useDynamicBatching = useDynamicBatching;
        pass.useGPUInstancing = useGPUInstancing;
        pass.useLightsPerObject = useLightsPerObject;
        pass.renderingLayerMask = renderingLayerMask;
        builder.SetRenderFunc<VisibleGeometryPass>(((pass, context) => pass.Render(context)));
    }
}
