using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.Experimental.Rendering.RenderGraphModule;
using UnityEngine.Rendering;

public class UnsupportedShaderPass
{
    #if UNITY_EDITOR
    private CameraRenderer renderer;
    
    void Render(RenderGraphContext context)
    {
        renderer.DrawUnsupportedShaders();
    }
    #endif
    
    [Conditional("UNITY_EDITOR")]
    public static void Record(RenderGraph graph, CameraRenderer renderer)
    {
    #if UNITY_EDITOR
        using RenderGraphBuilder builder = graph.AddRenderPass("Unsupported Shaders", out UnsupportedShaderPass pass);
        pass.renderer = renderer;
        builder.SetRenderFunc<UnsupportedShaderPass>(((pass, context) => pass.Render(context)));
    #endif
    }
}
