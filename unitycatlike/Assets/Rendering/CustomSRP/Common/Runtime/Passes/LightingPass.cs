using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.RenderGraphModule;
using UnityEngine.Rendering;

public class LightingPass
{
    private Lighting lighting;
    private CullingResults cullingResults;
    private ShadowSettings shadowSettings;
    
    private bool useLightsPerObject;
    int renderingLayerMask;

    void Render(RenderGraphContext context)
    {
        lighting.Setup(context, cullingResults, shadowSettings, useLightsPerObject, renderingLayerMask);
    }

    public static void Record(RenderGraph graph, Lighting lighting, CullingResults cullingResults, ShadowSettings shadowSettings, bool useLightsPerObject, int renderingLayerMask)
    {
        using RenderGraphBuilder builder = graph.AddRenderPass("Lighting", out LightingPass pass);
        pass.lighting = lighting;
        pass.cullingResults = cullingResults;
        pass.shadowSettings = shadowSettings;
        pass.useLightsPerObject = useLightsPerObject;
        pass.renderingLayerMask = renderingLayerMask;
        builder.SetRenderFunc<LightingPass>(((pass, context) => pass.Render(context)));
    }
}
