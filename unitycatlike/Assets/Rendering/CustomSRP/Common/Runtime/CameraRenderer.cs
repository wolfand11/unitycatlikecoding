﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Experimental.Rendering.RenderGraphModule;

public partial class CameraRenderer
{
    ScriptableRenderContext context;
    public Camera camera;
    //const string bufferName = "=== Render Camera";
    //CommandBuffer buffer = new CommandBuffer { name = bufferName };
    CommandBuffer buffer;
    CullingResults cullingResults;
    static ShaderTagId unlitShaderTagId = new ShaderTagId("SRPDefaultUnlit");
    static ShaderTagId litShaderTagId = new ShaderTagId("CustomLit");

    Lighting lighting = new Lighting();
    PostFXStack postFXStack = new PostFXStack();

    //static int frameBufferId = Shader.PropertyToID("_CameraFrameBuffer");
    public static int colorAttachmentId = Shader.PropertyToID("_CameraColorAttachment");
    public static int depthAttachmentId = Shader.PropertyToID("_CameraDepthAttachment");
    public static int colorTextureId = Shader.PropertyToID("_CameraColorTexture");
    public static int depthTextureId = Shader.PropertyToID("_CameraDepthTexture");
    public static int sourceTextureId = Shader.PropertyToID("_SourceTexture");
    public static int srcBlendId = Shader.PropertyToID("_CameraSrcBlend");
    public static int dstBlendId = Shader.PropertyToID("_CameraDstBlend");
    
    public static CameraSettings defaultCameraSettings = new CameraSettings();
    public static bool copyTextureSupported = SystemInfo.copyTextureSupport > CopyTextureSupport.None;
    
    static readonly Rect fullViewRect = new(0f, 0f, 1f, 1f);

    bool useHDR;
    bool useColorTexture;
    bool useDepthTexture;
    public bool useIntermediateBuffer;

    Material material;
    Texture2D missingTexture;
    public CameraRenderer(Shader shader)
    {
        material = CoreUtils.CreateEngineMaterial(shader);
        missingTexture = new Texture2D(1, 1)
        {
            hideFlags = HideFlags.HideAndDontSave,
            name = "Missing"
        };
        missingTexture.SetPixel(0, 0, Color.white * 0.5f);
        missingTexture.Apply(true, true);
    }
    public void Render(RenderGraph renderGraph, ScriptableRenderContext context, Camera camera, CameraBufferSettings cameraBufferSettings, bool useDynamicBatching, bool useGPUInstancing, bool useLightsPerObject, ShadowSettings shadowSettings, PostFXSettings postFXSettings, int colorLUTResolution)
    {
        this.context = context;
        this.camera = camera;

        ProfilingSampler cameraSampler;
        CameraSettings cameraSettings;

        if (camera.TryGetComponent(out CustomRenderPipelineCamera crpCamera))
        {
            cameraSampler = crpCamera.Sampler;
            cameraSettings = crpCamera.Settings;
        }
        else
        {
            cameraSampler = ProfilingSampler.Get(camera.cameraType);
            cameraSettings = defaultCameraSettings;
        }
        //useDepthTexture = true;
        if(camera.cameraType == CameraType.Reflection)
        {
            useColorTexture = cameraBufferSettings.copyColorReflection;
            useDepthTexture = cameraBufferSettings.copyDepthReflections;
        }
        else
        {
            useColorTexture = cameraBufferSettings.copyColor && cameraSettings.copyColor;
            useDepthTexture = cameraBufferSettings.copyDepth && cameraSettings.copyDepth;
        }
        if(cameraSettings.overridePostFX)
        {
            postFXSettings = cameraSettings.postFXSettings;
        }
        //PrepareBuffer();
        PrepareForSceneWindow();
        if(!Cull(shadowSettings.maxDistance))
        {
            return;
        }
        useHDR = cameraBufferSettings.allowHDR && camera.allowHDR;

        //buffer.BeginSample(SampleName);
        //ExecuteBuffer();
        postFXStack.Setup(camera, postFXSettings, useHDR, colorLUTResolution, cameraSettings.finalBlendMode);
        //buffer.EndSample(SampleName);
        //ExecuteBuffer();

        // 下面的执行顺序会导致如下报错信息
        // Dimensions of color surface does not match dimensions of depth surface
        //Setup();
        //lighting.Setup(context, cullingResults, shadowSettings);

        var renderGraphParameters = new RenderGraphParameters()
        {
            commandBuffer = CommandBufferPool.Get(),
            currentFrameIndex = Time.frameCount,
            executionName = cameraSampler.name,
            scriptableRenderContext = context
        };
        buffer = renderGraphParameters.commandBuffer;
        using (renderGraph.RecordAndExecute(renderGraphParameters))
        {
            using var _ = new RenderGraphProfilingScope(renderGraph, cameraSampler);
            int renderingLayerMask = cameraSettings.maskLights ? cameraSettings.renderingLayerMask : -1;
            LightingPass.Record(renderGraph, lighting, cullingResults, shadowSettings, useLightsPerObject, renderingLayerMask);
            SetupPass.Record(renderGraph, this);
            
            VisibleGeometryPass.Record(renderGraph, this, useDynamicBatching, useGPUInstancing, useLightsPerObject, cameraSettings.renderingLayerMask);
            UnsupportedShaderPass.Record(renderGraph, this);
            if (postFXStack.IsActive)
            {
                PostFXPass.Record(renderGraph, postFXStack);
            }    
            else if (useIntermediateBuffer)
            {
                FinalPass.Record(renderGraph, this, cameraSettings.finalBlendMode);
            }    
            GizmosPass.Record(renderGraph, this);
        }    
        
        Cleanup();
        Submit();
        
        CommandBufferPool.Release(renderGraphParameters.commandBuffer);
    }

    public void Setup()
    {
        context.SetupCameraProperties(camera);
        CameraClearFlags flags = camera.clearFlags;

        useIntermediateBuffer = useColorTexture || useDepthTexture || postFXStack.IsActive;
        if(useIntermediateBuffer)
        {
            if(flags > CameraClearFlags.Color)
            {
                flags = CameraClearFlags.Color;
            }
            buffer.GetTemporaryRT(
                colorAttachmentId, camera.pixelWidth, camera.pixelHeight, 0, FilterMode.Bilinear, 
                useHDR ? RenderTextureFormat.DefaultHDR : RenderTextureFormat.Default
            );
            buffer.GetTemporaryRT(
                depthAttachmentId, camera.pixelWidth, camera.pixelHeight, 32, FilterMode.Point, 
                RenderTextureFormat.Depth
            );

            buffer.SetRenderTarget(
                colorAttachmentId, RenderBufferLoadAction.DontCare, RenderBufferStoreAction.Store,
                depthAttachmentId, RenderBufferLoadAction.DontCare, RenderBufferStoreAction.Store
            );
        }
        buffer.ClearRenderTarget(
            flags <= CameraClearFlags.Depth, 
            flags <= CameraClearFlags.Color, 
            flags == CameraClearFlags.Color ? camera.backgroundColor.linear : Color.clear
        );
        //buffer.BeginSample(SampleName);
        buffer.SetGlobalTexture(colorTextureId, missingTexture);
        buffer.SetGlobalTexture(depthTextureId, missingTexture);
        ExecuteBuffer();
    }

    void Submit()
    {
        //buffer.EndSample(SampleName);
        ExecuteBuffer();
        context.Submit();
    }

    public void ExecuteBuffer()
    {
        context.ExecuteCommandBuffer(buffer);
        buffer.Clear();
    }

    bool Cull(float maxShadowDistance)
    {
        if(camera.TryGetCullingParameters(out ScriptableCullingParameters p))
        {
            p.shadowDistance = Mathf.Min(maxShadowDistance, camera.farClipPlane);
            cullingResults = context.Cull(ref p);
            return true;
        }
        return false;
    }

    public void Draw(RenderTargetIdentifier from, RenderTargetIdentifier to, bool isDepth=false)
    {
        buffer.SetGlobalTexture(sourceTextureId, from);
        buffer.SetRenderTarget(to, RenderBufferLoadAction.DontCare, RenderBufferStoreAction.Store);
        buffer.DrawProcedural(Matrix4x4.identity, material, isDepth ? 1 : 0, MeshTopology.Triangles, 3);
    }

    public void DrawVisibleGeometry(bool useDynamicBatching, bool useGPUInstancing, bool useLightsPerObject, int renderingLayerMask)
    {
        ExecuteBuffer();
        
        PerObjectData lightsPerObjectFlags = useLightsPerObject ? PerObjectData.LightData | PerObjectData.LightIndices : PerObjectData.None;
        var sortingSetting = new SortingSettings(camera) {
            criteria = SortingCriteria.CommonOpaque
        };
        var drawingSettings = new DrawingSettings(unlitShaderTagId, sortingSetting)
        {
            enableDynamicBatching = useDynamicBatching,
            enableInstancing = useGPUInstancing,
            perObjectData = PerObjectData.ReflectionProbes |
                            PerObjectData.Lightmaps | PerObjectData.ShadowMask | 
                            PerObjectData.LightProbe | PerObjectData.OcclusionProbe | 
                            PerObjectData.LightProbeProxyVolume | PerObjectData.OcclusionProbeProxyVolume |
                            lightsPerObjectFlags,
        };
        drawingSettings.SetShaderPassName(1, litShaderTagId);
        var filteringSettings = new FilteringSettings(RenderQueueRange.opaque, renderingLayerMask: (uint)renderingLayerMask);

        context.DrawRenderers(cullingResults, ref drawingSettings, ref filteringSettings);
        context.DrawSkybox(camera);
        CopyAttachments();

        sortingSetting.criteria = SortingCriteria.CommonTransparent;
        drawingSettings.sortingSettings = sortingSetting;
        filteringSettings.renderQueueRange = RenderQueueRange.transparent;
        context.DrawRenderers(cullingResults, ref drawingSettings, ref filteringSettings);
    }

    public void DrawFinal(CameraSettings.FinalBlendMode finalBlendMode)
    {
        buffer.SetGlobalFloat(srcBlendId, (float)finalBlendMode.source);
        buffer.SetGlobalFloat(dstBlendId, (float)finalBlendMode.destination);
        buffer.SetGlobalTexture(sourceTextureId, colorAttachmentId);
        RenderBufferLoadAction loadAction = RenderBufferLoadAction.DontCare;
        if(finalBlendMode.destination != BlendMode.Zero && camera.rect == fullViewRect)
        {
            loadAction = RenderBufferLoadAction.Load;
        }
        buffer.SetRenderTarget(
            BuiltinRenderTextureType.CameraTarget,
            loadAction,
            RenderBufferStoreAction.Store);
        buffer.SetViewport(camera.pixelRect);
        buffer.DrawProcedural(Matrix4x4.identity, material, 0, MeshTopology.Triangles, 3);
        buffer.SetGlobalFloat(srcBlendId, 1f);
        buffer.SetGlobalFloat(dstBlendId, 0f);
    }


    void Cleanup()
    {
        lighting.Cleanup();
        if(useIntermediateBuffer)
        {
            buffer.ReleaseTemporaryRT(colorAttachmentId);
            buffer.ReleaseTemporaryRT(depthAttachmentId);

            if(useColorTexture)
            {
                buffer.ReleaseTemporaryRT(colorTextureId);
            }

            if(useDepthTexture)
            {
                buffer.ReleaseTemporaryRT(depthTextureId);
            }
        }
    }

    void CopyAttachments()
    {
        if(useColorTexture)
        {
            buffer.GetTemporaryRT(colorTextureId, camera.pixelWidth, camera.pixelHeight, 0, FilterMode.Bilinear, useHDR ? RenderTextureFormat.DefaultHDR : RenderTextureFormat.Default);
            if(copyTextureSupported)
            {
                buffer.CopyTexture(colorAttachmentId, colorTextureId);
            }
            else
            {
                Draw(colorAttachmentId, colorTextureId);
            }
        }
        if(useDepthTexture)
        {
            buffer.GetTemporaryRT(depthTextureId, camera.pixelWidth, camera.pixelHeight, 32, FilterMode.Point, RenderTextureFormat.Depth);
            if(copyTextureSupported)
            {
                buffer.CopyTexture(depthAttachmentId, depthTextureId);
            }
            else
            {
                Draw(depthAttachmentId, depthTextureId, true);
            }
        }
        if (useColorTexture || useDepthTexture)
        {
            if(!copyTextureSupported)
            {
                buffer.SetRenderTarget(
                    colorAttachmentId,
                    RenderBufferLoadAction.Load, RenderBufferStoreAction.Store,
                    depthAttachmentId,
                    RenderBufferLoadAction.Load, RenderBufferStoreAction.Store
                );
            }
            ExecuteBuffer();
        }
    }

    public void Dispose()
    {
        CoreUtils.Destroy(material);
        CoreUtils.Destroy(missingTexture);
    }
}
