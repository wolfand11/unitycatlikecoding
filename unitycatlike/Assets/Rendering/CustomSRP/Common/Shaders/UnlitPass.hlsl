﻿#ifndef CUSTOM_UNLIT_PASS_INCLUDED
#define CUSTOM_UNLIT_PASS_INCLUDED

struct Attributes {
	float3 positionOS : POSITION;
	float4 color : COLOR;
#if defined(_FLIPBOOK_BLENDING)
	// 两个UV用于采样同一张atlas上的不同部位
	// baseUV.xy == UV
	// baseUV.zw == UV2
	float4 baseUV : TEXCOORD0;
	// flipbookBlend == AnimBlend
	float flipbookBlend : TEXCOORD1;
#else
	float2 baseUV : TEXCOORD0;
#endif
	UNITY_VERTEX_INPUT_INSTANCE_ID
};

struct Varyings {
	float4 positionCS_SS : SV_POSITION;
#if defined(_VERTEX_COLORS)
	float4 color : VAR_COLOR;
#endif
	float2 rawUV : VAR_RAW_UV;
	float2 baseUV : VAR_BASE_UV;
#if defined(_FLIPBOOK_BLENDING)
	float3 flipbookUVB : VAR_FLIPBOOK;
#endif
	float2 detailUV : VAR_DETAIL_UV;
	UNITY_VERTEX_INPUT_INSTANCE_ID
};

Varyings UnlitPassVertex(Attributes input)
{
	Varyings output;
	UNITY_SETUP_INSTANCE_ID(input);
	UNITY_TRANSFER_INSTANCE_ID(input, output);
	float3 positionWS = TransformObjectToWorld(input.positionOS);
	output.positionCS_SS = TransformWorldToHClip(positionWS);
#if defined(_VERTEX_COLORS)
	output.color = input.color;
#endif
	output.rawUV = input.baseUV.xy;
	output.baseUV.xy = TransformBaseUV(input.baseUV.xy);
#if defined(_FLIPBOOK_BLENDING)
	output.flipbookUVB.xy = TransformBaseUV(input.baseUV.zw);
	output.flipbookUVB.z = input.flipbookBlend;
#endif
	output.detailUV = TransformDetailUV(input.baseUV.xy);
	return output;
}

float4 UnlitPassFragment(Varyings input) : SV_Target
{
	UNITY_SETUP_INSTANCE_ID(input);
	InputConfig config = GetInputConfig(input.positionCS_SS, input.baseUV, input.detailUV);
#if defined(_VERTEX_COLORS)
	config.color = input.color;
#endif
#if defined(_FLIPBOOK_BLENDING)
	config.flipbookUVB = input.flipbookUVB;
	config.flipbookBlending = true;
#endif
#if defined(_NEAR_FADE)
	config.nearFade = true;
#endif
#if defined(_SOFT_PARTICLES)
	config.softParticles = true;
#endif
	
	float4 base = GetBase(config);
#if defined(_CLIPPING)
	clip(base.a - UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _Cutoff));
#endif
#if defined(_DISTORTION)
	//float2 distortion = GetDistortion(config);
	float2 distortion = GetDistortion(config) * base.a;
	//base.rgb = GetBufferColor(config.fragment, distortion).rgb;
	float3 bufferColor = GetBufferColor(config.fragment, distortion).rgb;
	base.rgb = lerp(
		bufferColor, 
		base.rgb, 
		saturate(base.a - GetDistortionBlend(config))
	);

#endif
	//return GetBufferColor(config.fragment, 0.05);
	//return float4(config.fragment.bufferDepth.xxx / 1000.0, 1.0);
	//return float4(base.aaa, 1);
	//return float4(base.rgb, 1);
	return float4(base.rgb, GetFinalAlpha(base.a));
	//return float4(config.baseUV, 0, GetFinalAlpha(base.a));
	//return float4(frac(input.rawUV * 4), 0, 1);
}

#endif