﻿#ifndef CUSTOM_UNLIT_INPUT_INCLUDED
#define CUSTOM_UNLIT_INPUT_INCLUDED

TEXTURE2D(_BaseMap);
SAMPLER(sampler_BaseMap);
TEXTURE2D(_DistortionMap);
TEXTURE2D(_DetailMap);
SAMPLER(sampler_DetailMap);

#define sampler_BaseMap sampler_linear_clamp

#define INPUT_PROP(name) UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, name)

UNITY_INSTANCING_BUFFER_START(UnityPerMaterial)
	UNITY_DEFINE_INSTANCED_PROP(float4, _BaseMap_ST)
	UNITY_DEFINE_INSTANCED_PROP(float4, _BaseColor)
	UNITY_DEFINE_INSTANCED_PROP(float, _NearFadeDistance)
	UNITY_DEFINE_INSTANCED_PROP(float, _NearFadeRange)
	UNITY_DEFINE_INSTANCED_PROP(float, _SoftParticlesDistance)
	UNITY_DEFINE_INSTANCED_PROP(float, _SoftParticlesRange)
	UNITY_DEFINE_INSTANCED_PROP(float, _DistortionStrength)
	UNITY_DEFINE_INSTANCED_PROP(float, _DistortionBlend)
	UNITY_DEFINE_INSTANCED_PROP(float, _Cutoff)
	UNITY_DEFINE_INSTANCED_PROP(float, _ZWrite)
	UNITY_DEFINE_INSTANCED_PROP(float4, _DetailMap_ST)
UNITY_INSTANCING_BUFFER_END(UnityPerMaterial)

struct InputConfig
{
	Fragment fragment;
	float4 color;
	float2 baseUV;
	float2 detailUV;
	float3 flipbookUVB;
	bool flipbookBlending;
	bool nearFade;
	bool softParticles;
};

InputConfig GetInputConfig(float4 positionSS, float2 baseUV, float2 detailUV = 0.0)
{
	InputConfig c;
	c.fragment = GetFragment(positionSS);
	c.color = 1.0;
	c.baseUV = baseUV;
	c.detailUV = detailUV;
	c.flipbookUVB = 0.0;
	c.flipbookBlending = false;
	c.nearFade = false;
	c.softParticles = false;
	return c;
}

float2 TransformBaseUV(float2 baseUV)
{
	float4 baseST = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _BaseMap_ST);
	return baseUV * baseST.xy + baseST.zw;
}

float2 TransformDetailUV(float2 detailUV)
{
	float4 detailST = INPUT_PROP(_DetailMap_ST);
	return detailUV * detailST.xy + detailST.zw;
}

float4 GetBase(InputConfig c)
{
	float4 map = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, c.baseUV);
	if (c.flipbookBlending)
	{
		map = lerp(map, SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, c.flipbookUVB.xy), c.flipbookUVB.z);
	}
	if (c.nearFade)
	{
		float nearAttenuation = (c.fragment.depth - INPUT_PROP(_NearFadeDistance)) / INPUT_PROP(_NearFadeRange);
		map.a *= saturate(nearAttenuation);
	}
	if (c.softParticles)
	{
		float depthDelta = c.fragment.bufferDepth - c.fragment.depth;
		float nearAttenuation = (depthDelta - INPUT_PROP(_SoftParticlesDistance)) / INPUT_PROP(_SoftParticlesRange);
		map.a *= saturate(nearAttenuation);
	}
	float4 color = INPUT_PROP(_BaseColor);
	return map * color * c.color;
}

float3 GetEmission(InputConfig c)
{
	return GetBase(c).rgb;
}

float GetCutoff(InputConfig c)
{
	return UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _Cutoff);
}

float GetFinalAlpha(float alpha)
{
	return INPUT_PROP(_ZWrite) ? 1.0 : alpha;
}

float GetFresnel(InputConfig c)
{
	return 0.0;
}

float2 GetDistortion(InputConfig c)
{
	int tileSize = 1;
	float2 uv1 = frac(c.baseUV * tileSize);
	float4 rawMap = SAMPLE_TEXTURE2D(_DistortionMap, sampler_BaseMap, uv1);
	if (c.flipbookBlending)
	{
		float2 uv2 = frac(c.flipbookUVB.xy * tileSize);
		rawMap = lerp(rawMap, SAMPLE_TEXTURE2D(_DistortionMap, sampler_BaseMap, uv2), c.flipbookUVB.z);
	}
	return DecodeNormal(rawMap, INPUT_PROP(_DistortionStrength)).xy;
}

float GetDistortionBlend(InputConfig c)
{
	return INPUT_PROP(_DistortionBlend);
}
#endif